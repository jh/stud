#!/usr/bin/python3

# Exercise 8.1
import cv2 

video_capture = cv2.VideoCapture(0)
ret = True

while ret:
  ret, gray = video_capture.read()
  if ret == False:
    break
  cv2.imshow('Frame', gray)
  
  if cv2.waitKey(1) & 0xFF == ord('q'):
    break

video_capture.release()
cv2.destroyAllWindows()
