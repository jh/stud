#!/usr/bin/python3

# Exercise 8.2
import cv2
import sys
import time
video_capture = cv2.VideoCapture(0)
video_capture.set(3,1280) # FRAME_WIDTH
video_capture.set(4,720) # FRAME_HEIGHT
video_capture.set(5, 25) # FRAME_FPS
ret = True
no_frames = 100
cnt = 0
start = time.time()
while ret:
    cnt = cnt + 1
    if cnt==no_frames:
        end = time.time()
        seconds = end-start
        print("Time taken = {0} sec".format(seconds))
        fps = no_frames/seconds
        print("Estimated FPS = {0} ".format(fps))
        cnt = 0
        start = time.time()
    # Capture frame-by-frame
    ret, gray = video_capture.read(0)
    if ret == False:
        break
    cv2.imshow("window", gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()
