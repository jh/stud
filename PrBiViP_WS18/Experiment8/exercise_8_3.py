#!/usr/bin/python3

#Exercise 8.3
import cv2
import sys
from multiprocessing.pool import ThreadPool
pencasc_vert = 'pen_vertical_classifier.xml'
pencasc_hor = 'pen_horizontal_classifier.xml'
penCascade_vert = cv2.CascadeClassifier(pencasc_vert)
penCascade_hor = cv2.CascadeClassifier(pencasc_hor)
#video_capture = cv2.VideoCapture('exercise_8_1.mkv')
video_capture = cv2.VideoCapture(0)
ret = True
count = 0

def detect(orientation, pic):
    if orientation == 'vertical':
        detections = penCascade_vert.detectMultiScale(
            pic,
            scaleFactor=1.5,
            minNeighbors=25,
            minSize=(25, 80),
            flags=cv2.CASCADE_SCALE_IMAGE
        )
    else:
        detections = penCascade_hor.detectMultiScale(
            pic,
            scaleFactor=1.8,
            minNeighbors=30,
            minSize=(80, 35),
            flags=cv2.CASCADE_SCALE_IMAGE
        )

    return detections

pool = ThreadPool(processes=4)

while ret:
    count += 1
    # Capture frame-by-frame
    ver_flag = False
    ret, gray = video_capture.read(0)
    if ret == False:
        break

    async_result = pool.starmap(detect, [ ('vertical', gray), ('horizontal', gray) ])

    pens_vert = async_result[0]
    # Draw a rectangle around the objects
    for (x, y, w, h) in pens_vert:
        cv2.rectangle(gray, (x,y), (x+w, y+h), 0, 5)
        #ver_flag = True

    pens_hor = async_result[1]
    for (p, q, r, s) in pens_hor:
        if ver_flag != True:
            cv2.rectangle(gray, (p, q), (p+r, q+s), 100, 5)
    cv2.imshow("window", gray)
    cv2.imwrite("output/frame"+str(count)+".png", gray)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()
