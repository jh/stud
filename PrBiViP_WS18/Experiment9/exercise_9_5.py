#!/usr/bin/python3

# Exercise 9.5

#library imports
import numpy as np
import cv2

def addAlphaChannel(img):
    #color space transform
    img = cv2.cvtColor(img,cv2.COLOR_BGR2RGBA)
    #set the alpha channel to transparent by use of a mask
    #“axis” defines the way of processing rows or columns 
    img[np.all(img == [0, 0, 0, 255], axis=2)] = [0, 0, 0, 0]
    #write the image
    cv2.imwrite('alpha_grabcut.png', img)
    return img


#cv2.imshow('frame', addAlphaChannel(cv2.imread('grabcut_frame.png')))

