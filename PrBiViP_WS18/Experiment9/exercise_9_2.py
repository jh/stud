#!/usr/bin/python3

# Exercise 9.2

#library import and initialization of video capture object
import numpy as np
import cv2

cap = cv2.VideoCapture(0)

#define the codec (fourcc) and create video writer object
fourcc = cv2.VideoWriter_fourcc(*'H264')
out = cv2.VideoWriter('video.mp4', fourcc, 10, (640,480))

while out.isOpened():
    ret, frame = cap.read()
    if ret==True:
        #frame = cv2.flip(frame,0)
        #write and show the (flipped) frame
        out.write(frame)
        cv2.imshow('frame', frame)
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break
    else:
        break

#release everything if job is finished
cap.release()
cv2.releaseVideoWriter(out)
cv2.destroyAllWindows()
