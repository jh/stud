#!/usr/bin/python3

# Exercise 9.4

#library imports
import numpy as np
import cv2

def grabCut(img, x1, y1, x2, y2):
    #read the image 
    frame = img
    #create a mask out of zeros
    mask = np.zeros(frame.shape[:2], dtype=np.uint8)
    #create FG & BG model
    bgdModel = np.zeros(shape=(1,65), dtype=np.float64)
    fgdModel = np.zeros(shape=(1,65), dtype=np.float64)
    #compute rectangle coordinates from input
    rect = (x1, y1, x2, y2)
    #perform OpenCV .grabCut()
    cv2.grabCut(img, mask, rect, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_RECT)
    #convert the mask to binary for further operations
    mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
    #this operation applies the mask onto our frame and creates a new axis
    frame = frame*mask2[:,:,np.newaxis]            
    #write the frame to your directory
    cv2.imwrite('grabcut_frame.png', frame)
    return frame

# cap = cv2.VideoCapture(0)
# ret, img = cap.read()
# cv2.imshow('frame', grabCut(img, 0, 0, 400, 400))
# cap.release()
# cv2.destroyAllWindows()
