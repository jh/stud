#!/usr/bin/python3

# Exercise 9.3

#library imports
import numpy as np
import cv2

#initialization of video capture object; set webcam as input
cap = cv2.VideoCapture(0)

#create background subtractor and kernel, if necessary
fgbg = cv2.bgsegm.createBackgroundSubtractorMOG()
#fgbg = cv2.bgsegm.createBackgroundSubtractorGMG()
#kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3))
#write while-loop, that is executed if the VideoCapture object is opened
#TODO
while(cap.isOpened()):
    #read cap and apply the background subtractor on the current frame
    ret, frame = cap.read()
    fgmask = fgbg.apply(frame)
    #only the version with kernel needs and additional morphology
    #fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
    #show the resulting frame and add an option to quit by pressing a key
    cv2.imshow('frame', fgmask)
    if cv2.waitKey(25) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
