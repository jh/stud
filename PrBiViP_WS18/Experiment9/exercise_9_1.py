#!/usr/bin/python3

# Exercise 9.1

#import libraries
import numpy as np
import cv2

#create VideoCapture object
cap = cv2.VideoCapture(0)
#use pre-trained Haar Cascade XML classifier
object_classifier = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
#read until video is completed/ process every frame
while True:
    #capture a single frame for the current iteration
    ret, img = cap.read()
    #convert frame into gray scale 
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)    
    #detect objects in the frame
    detected_objects = object_classifier.detectMultiScale(gray, 1.1, 5) # 1.1, 4
   
    #to draw rectangle on each detected object in the current frame
    for (x, y, w, h) in detected_objects:
        cv2.rectangle(img, (x,y), (x+w, y+h), 0, 5)
    
    #display the resulting frame
    cv2.imshow('frame', img)
    #press Q on keyboard to exit
    if cv2.waitKey(25) & 0xFF == ord('q'):
        break
    
#release VideoCapture object
cap.release()
#close all windows
cv2.destroyAllWindows()
