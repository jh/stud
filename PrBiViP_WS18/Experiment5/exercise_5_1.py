#!/usr/bin/python3

# Exercise 5.1

import numpy as np
import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import cv2
from pylab import *
from PIL import Image
import sys

import scipy.ndimage as ndim

path = './'

def edgeDetector1D( npImg, Dm, Dn ):
    Fm = np.zeros( npImg.shape )
    Fn = np.zeros( npImg.shape )
    ndim.correlate1d( npImg, Dm, output=Fm, axis=1 )
    ndim.correlate1d( npImg, Dn, output=Fn, axis=0 )
    magnitude = np.sqrt(np.square(Fm)+np.square(Fn))
    phase = np.transpose(np.arctan2(Fm, Fn))
    return Fm, Fn, magnitude, phase

#gray = np.array( Image.open( path + 'chessboard.png').convert('L'), 'uint8')
gray = np.array( Image.open( path + 'exercise_5-2.png').convert('L'), 'uint8')

D1 = np.array([-1, 1, 0])
D2 = np.array([0, -1, 1])
D3 = np.array([-1, 0, 1])

Fm, Fn, magnitude, phase = edgeDetector1D(gray, D1, D1)
Fm2, Fn2, magnitude2, phase2 = edgeDetector1D( gray, D2, D2 )
Fm3, Fn3, magnitude3, phase3 = edgeDetector1D( gray, D3, D3 )

plt.subplot(5,3,1), plt.axis('off')
plt.imshow(gray,'gray'), plt.title('Gradient 1')

plt.subplot(5,3,2), plt.axis('off')
plt.imshow(gray,'gray'), plt.title('Gradient 2')

plt.subplot(5,3,3), plt.axis('off')
plt.imshow(gray,'gray'), plt.title('Gradient 3')

######################################

plt.subplot(5,3,4), plt.axis('off')
plt.imshow(Fm, 'gray')

plt.subplot(5,3,5), plt.axis('off')
plt.imshow(Fm2, 'gray'), plt.title('Fm vertical edges')

plt.subplot(5,3,6), plt.axis('off')
plt.imshow(Fm3, 'gray')

#####################################

plt.subplot(5,3,7), plt.axis('off')
plt.imshow(Fn, 'gray')

plt.subplot(5,3,8), plt.axis('off')
plt.imshow(Fn2, 'gray'), plt.title('Fn horizontal edges')

plt.subplot(5,3,9), plt.axis('off')
plt.imshow(Fn3, 'gray')

#####################################

plt.subplot(5,3,10), plt.axis('off')
plt.imshow(magnitude, 'gray')

plt.subplot(5,3,11), plt.axis('off')
plt.imshow(magnitude2, 'gray'), plt.title('Absolute value')

plt.subplot(5,3,12), plt.axis('off')
plt.imshow(magnitude3, 'gray')

#####################################

plt.subplot(5,3,13), plt.axis('off')
plt.imshow(phase, 'gray')

plt.subplot(5,3,14), plt.axis('off')
plt.imshow(phase2, 'gray'), plt.title('Phase')

plt.subplot(5,3,15), plt.axis('off')
plt.imshow(phase3, 'gray')

#####

plt.show()
