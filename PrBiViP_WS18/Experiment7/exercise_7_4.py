#!/usr/bin/python3

import cv2, time

# Exercise 7.4

FEATURES = 400
MASK = None

img = cv2.imread('Lena.png', 0)
sift = cv2.xfeatures2d.SIFT_create(FEATURES) 
kp, des = sift.detectAndCompute(img, MASK)
print("Detected features:", len(des), ", Shape:", des.shape)
featImg = cv2.drawKeypoints(img, kp, None, (0,0,255), 4)
cv2.imwrite('LenaKP.png', featImg)
