#!/usr/bin/python3

# Exercise 7.3
import numpy as np
import cv2, time
import math
gray = cv2.imread('Lena.png', 0)

scaleLevels = 5
sigma = 1.6
k = math.sqrt(2) 

scaleSpaceGauss = np.zeros((gray.shape[0], gray.shape[1], scaleLevels),dtype=np.uint8)
scaleSpaceDoG = np.zeros((gray.shape[0], gray.shape[1], scaleLevels),dtype=np.uint8)

for i in range(scaleLevels):
    kSigma = sigma*k**i 
    scaleSpaceGauss[:,:,i] = cv2.GaussianBlur(gray, ksize=(0,0),sigmaX=kSigma,sigmaY=kSigma)
    cv2.imwrite('imgGauss_Oct1_' + str(i) + '.png', scaleSpaceGauss[:,:,i])

for i in range(scaleLevels-1):
    scaleSpaceDoG[:,:,i] = scaleSpaceGauss[:,:,i+1] - scaleSpaceGauss[:,:,i]
    cv2.imwrite('imgDoG_Oct1_' + str(i+1) + '.png',scaleSpaceDoG[:,:,i])

nextOctgray = cv2.pyrDown(gray)

scaleSpaceGauss2 = np.zeros((nextOctgray.shape[0], nextOctgray.shape[1],scaleLevels), dtype=np.uint8)
scaleSpaceDoG2 = np.zeros((nextOctgray.shape[0], nextOctgray.shape[1],scaleLevels), dtype=np.uint8)

for i in range(scaleLevels):
    kSigma = sigma*k**i 
    scaleSpaceGauss2[:,:,i] = cv2.GaussianBlur(nextOctgray, ksize=(0,0),sigmaX=kSigma,sigmaY=kSigma)
    cv2.imwrite('imgGauss_Oct2_' + str(i) + '.png', scaleSpaceGauss2[:,:,i])

for i in range(scaleLevels-1):
    scaleSpaceDoG2[:,:,i] = scaleSpaceGauss2[:,:,i+1] - scaleSpaceGauss2[:,:,i]
    cv2.imwrite('imgDoG_Oct2_' + str(i+1) + '.png',scaleSpaceDoG2[:,:,i])
