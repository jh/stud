#!/usr/bin/python3

# Exercise 3.1

from PIL import Image as img
from PIL import ImageDraw as draw
import time

TRUE = 1
FALSE = 0
BLACK = 0
WHITE = 1
CHESSSIZE = [8,8]
FIELDSIZE = [10,15]
WIDTH = CHESSSIZE[0]*FIELDSIZE[0]
HEIGHT = CHESSSIZE[1]*FIELDSIZE[1]

print("Width:", WIDTH, ", Height:", HEIGHT)

# Exercise 3.2

def saveImg(pic, picName, picType):
    assert type(picName) is str
    assert type(picType) is str
    try:
        pic.save(picName, picType)
        result = TRUE
    except:
        result = FALSE
    return result

def createBlankImg(picWidth, picHeight):
    assert type(picWidth) is type(1)
    assert type(picHeight) is int
    assert (picWidth > 0) and (picHeight > 0)
    pic = img.new("1", (picWidth, picHeight))
    return pic

# testing functions
#new_img = createBlankImg(WIDTH, HEIGHT)
#saveImg(new_img, "my.img", "PNG")

# Exercise 3.3

def createChessField(pic):
    assert type(pic.size) is type(()) # is type tuple
    assert (pic.size[0] == WIDTH) and (pic.size[1] == HEIGHT)
    bw = [BLACK, WHITE]
    chessField = pic.copy()
    for i in range(WIDTH):
        for j in range(HEIGHT):
            if (j // FIELDSIZE[1]) % 2 == 0:
                color = bw[( i // FIELDSIZE[0]) % 2]
            else:
                # reversed list
                color = bw[::-1][( i // FIELDSIZE[0]) % 2]

            chessField.putpixel((i,j), color)

    return chessField

# testing funcs
#my_chess = createChessField(createBlankImg(WIDTH, HEIGHT))
#saveImg(my_chess, "chess.png", "PNG")

# Exercise 3.4
#my_chess = createChessField(createBlankImg(WIDTH, HEIGHT))
#saveImg(my_chess, "chessfield.pbm", "PPM")


# Exercise 3.5

def openPic(filePath):
    assert type(filePath) is str
    # try:
    #     pic = img.open(filePath)
    # except:
    #     pic = None
    # return pic
    return img.open(filePath)

def createBlackFrame(pic, frameWidth):
    assert type(frameWidth) is int
    newWidth = 2*frameWidth + pic.width
    newHeight = 2*frameWidth + pic.height
    picWithFrame = createBlankImg(newWidth, newHeight)
    picWithFrame.paste(pic, (frameWidth, frameWidth))
    return picWithFrame

def transposePic(pic):
    rotatedPic = createBlankImg(pic.height, pic.width)
    for x in range(pic.width):
        for y in range(pic.height):
            rotatedPic.putpixel( (y,x), pic.getpixel((x,y)) )
    return rotatedPic

# testing

#framed_img = createBlackFrame(openPic("chessfield.pbm"), 5)
#saveImg(framed_img, "frame.png", "PNG")
#framed_img.show()
#framed_img.show()
#transposed_img = transposePic(framed_img)
#saveImg(transposed_img, "transposed.png", "PNG")
#transposed_img.show()
#transposePic(createChessField(createBlankImg(WIDTH, HEIGHT))).show()

# Exercise 3.6

def createBox(position):
    assert type(position) is type(())
    x = position[0]
    y = position[1]
    assert position[0] >= 0 and position[0] < WIDTH
    assert position[1] >= 0 and position[1] < HEIGHT
    x0 = (x // FIELDSIZE[0]) * FIELDSIZE[0]
    x1 = x0 + FIELDSIZE[0]
    y0 = (y // FIELDSIZE[1]) * FIELDSIZE[1]
    y1 = y0 + FIELDSIZE[1]
    blackBox = (x0, y0, x1, y1)
    return blackBox

def createChessField2(pic):
    chessField2 = pic.copy()
    for i in range(CHESSSIZE[0]):
        for j in range(CHESSSIZE[1]):
            position = ( i * FIELDSIZE[0], j * FIELDSIZE[1] )
            if (i+j) % 2 == 1:
                box = createBox(position)
                chessField2.paste(1, box)

    return chessField2

#chess2_img = createBlankImg(WIDTH, HEIGHT)
#chess2_img = createChessField2(chess2_img)

# Exercise 3.7

def createChessField3(pic):
    assert (pic.size[0] == WIDTH) and (pic.size[1] == HEIGHT)
    drawPic = pic.copy()
    d = draw.Draw(drawPic)
    bw = [BLACK, WHITE]
    for i in range(WIDTH):
        for j in range(HEIGHT):
            if (j // FIELDSIZE[1]) % 2 == 0:
                color = bw[( i // FIELDSIZE[0]) % 2]
            else:
                # reversed list
                color = bw[::-1][( i // FIELDSIZE[0]) % 2]

            d.point((i,j), color)

    del d
    return drawPic

#chess3_img = createBlankImg(WIDTH, HEIGHT)
#chess3_img = createChessField3(chess3_img).show()

# Exercise 3.8

import time

def compare():
    template = createBlankImg(WIDTH, HEIGHT)
    chess1 = template.copy()
    chess2 = template.copy()
    chess3 = template.copy()

    start = time.time()
    chess1 = createChessField(chess1)
    stop = time.time()
    print("createChessField1:", stop-start)

    start = time.time()
    chess2 = createChessField2(chess2)
    stop = time.time()
    print("createChessField2:", stop-start)

    start = time.time()
    chess3 = createChessField3(chess3)
    stop = time.time()
    print("createChessField3:", stop-start)

compare()
