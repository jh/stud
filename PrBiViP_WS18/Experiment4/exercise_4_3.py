#!/usr/bin/python3

# Exercise 4.3

from PIL import Image as img
import numpy as np
import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import matplotlib.image as npimg
import time

def loadImage2Array(filename):
    assert type(filename) is str
    return np.array(img.open(filename), np.uint8)

def saveArray2GrayImg(npArrayImg, filename):
    assert type(filename) is str
    assert type(npArrayImg) is np.ndarray
    npimg.imsave(filename, npArrayImg, cmap='gray')

def flat_luminance( rgb ):
    return int( (rgb[0] + rgb[1] + rgb[2])/3 )

def weighted_luminance( rgb ):
    return int( (0.299*rgb[0] + 0.587*rgb[1] + 0.114*rgb[2])/3 )

def convertRGB_equal( imgRGB ):
    assert type(imgRGB) is np.ndarray
    width, height, depth = imgRGB.shape
    imgGray = np.ndarray(shape=(width, height), dtype=np.uint8)
    for x in range(width):
        for y in range(height):
            imgGray[x,y] = flat_luminance(imgRGB[x,y])

    return imgGray

def convertRGB_weighted1( imgRGB ):
    assert type(imgRGB) is np.ndarray
    width, height, depth = imgRGB.shape
    imgGray = np.ndarray(shape=(width, height), dtype=np.uint8)
    for x in range(width):
        for y in range(height):
            imgGray[x,y] = weighted_luminance(imgRGB[x,y])

    return imgGray

def convertRGB_weighted2( imgRGB ):
    assert type(imgRGB) is np.ndarray
    return np.dot(imgRGB, [0.299, 0.587, 0.114])

def histogramm(imgGray):
    assert type(imgGray) is np.ndarray
    if imgGray.dtype != np.dtype(np.uint8):
        imgGray = convertRGB_weighted2(imgGray)

    values, indices = np.unique(imgGray, return_inverse=True)
    histogramm = np.bincount(indices)
    return histogramm

def plothistogramm(imgGray):
    hist = histogramm(imgGray)
    
    plt.subplot(211)
    plt.plot(hist)
    plt.title("My Histogram")

    # "official" matplotlib hist
    plt.subplot(212)
    plt.hist(imgGray.flatten(), bins=255)
    
    plt.show()

#plothistogramm(loadImage2Array("Lena.png"))

def cmpHists(img1,img2,img3):
    assert type(img1) is str
    assert type(img2) is str
    assert type(img3) is str
    my_img1 = loadImage2Array(img1)
    my_img2 = loadImage2Array(img2)
    my_img3 = loadImage2Array(img3)
    
    plt.subplot(231)
    plt.title(img1)
    plt.imshow(convertRGB_weighted2(my_img1), "gray")

    plt.subplot(232)
    plt.title(img2)
    plt.imshow(convertRGB_weighted2(my_img2), "gray")

    plt.subplot(233)
    plt.title(img3)
    plt.imshow(convertRGB_weighted2(my_img3), "gray")
    
    plt.subplot(234)
    plt.plot( histogramm(my_img1) )

    plt.subplot(235)
    plt.plot( histogramm(my_img2) )

    plt.subplot(236)
    plt.plot( histogramm(my_img3) )

    plt.show()

#cmpHists("exercise_4-2_brightness-0.png", "exercise_4-2_brightness-50.png", "exercise_4-2_brightness-100.png")

import exercise_4_2 as ex42

def reportTime(func, var):
    start = time.time()
    func(var)
    stop = time.time()
    return stop - start

def timeTests():
    my_img_arr = loadImage2Array("Lena.png")
    my_img = img.open("Lena.png")

    print("createGrayScaleEqual:", reportTime(ex42.createGrayScaleEqual, my_img))
    print("createGrayScaleWeighted:", reportTime(ex42.createGrayScaleWeighted, my_img))

    print("convertRGB_equal:", reportTime(convertRGB_equal, my_img_arr))
    print("convertRGB_weighted1:", reportTime(convertRGB_weighted1, my_img_arr))
    print("convertRGB_weighted2:", reportTime(convertRGB_weighted2, my_img_arr))


timeTests()
