#!/usr/bin/python3

# Exercise 4.2

from PIL import Image as img

img_low = img.open("exercise_4-2_brightness-0.png")
img_medium = img.open("exercise_4-2_brightness-50.png")
img_high = img.open("exercise_4-2_brightness-100.png")
img_test = img.open("Testimage.png")

def flat_luminance( rgb ):
    return int( (rgb[0] + rgb[1] + rgb[2])/3 )

def weighted_luminance( rgb ):
    return int( (0.299*rgb[0] + 0.587*rgb[1] + 0.114*rgb[2]) )

def createGrayScaleImages(image):
    flat_gray = img.new("L", (image.width, image.height))
    weighted_gray = img.new("L", (image.width, image.height))
    for x in range(image.width):
        for y in range(image.height):
            old_pixel = image.getpixel((x,y))

            flat_color = flat_luminance(old_pixel)
            flat_gray.putpixel( (x,y), flat_color )

            weighted_color = weighted_luminance(old_pixel)
            weighted_gray.putpixel( (x,y), weighted_color )

    return (flat_gray, weighted_gray)

def createGrayScaleEqual(image):
    flat_gray = img.new("L", (image.width, image.height))
    for x in range(image.width):
        for y in range(image.height):
            old_pixel = image.getpixel((x,y))

            flat_color = flat_luminance(old_pixel)
            flat_gray.putpixel( (x,y), flat_color )

    return flat_gray

def createGrayScaleWeighted(image):
    weighted_gray = img.new("L", (image.width, image.height))
    for x in range(image.width):
        for y in range(image.height):
            old_pixel = image.getpixel((x,y))

            weighted_color = weighted_luminance(old_pixel)
            weighted_gray.putpixel( (x,y), weighted_color )

    return weighted_gray

def render():
    (img_low_flat, img_low_weighted) = createGrayScaleImages(img_low)
    (img_medium_flat, img_medium_weighted) = createGrayScaleImages(img_medium)
    (img_high_flat, img_high_weighted) = createGrayScaleImages(img_high)
    (img_test_flat, img_test_weighted) = createGrayScaleImages(img_test)

    img_low_flat.save("img_low_flat.png", "PNG")
    img_low_weighted.save("img_low_weighted.png", "PNG")
    img_medium_flat.save("img_medium_flat.png", "PNG")
    img_medium_weighted.save("img_medium_weighted.png", "PNG")
    img_high_flat.save("img_high_flat.png", "PNG")
    img_high_weighted.save("img_high_weighted.png", "PNG")
    img_test_flat.save("img_test_flat.png", "PNG")
    img_test_weighted.save("img_test_weighted.png", "PNG")

#render()
