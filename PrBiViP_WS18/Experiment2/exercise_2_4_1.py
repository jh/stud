#!/usr/bin/python3

# Exercise 2.4.1

import keyword
import sys

filename = "python" + sys.version[0] + ".txt"
file = open(filename, 'w')
for kw in keyword.kwlist:
    file.write(kw + '\n')
file.close()

# read and parse keywords from python2.txt
kwlist2 = []
f2 = open('python2.txt', 'r')
readlist = f2.readlines()
for line in readlist:
    kwlist2.append(line.strip('\n'))
f2.close()

# read and parse keywords from python3.txt
kwlist3 = []
f3 = open('python3.txt', 'r')
readlist = f3.readlines()
for line in readlist:
    kwlist3.append(line.strip('\n'))
f3.close()

# returns elements not present in both lists
def diffList(list1, list2):
    diff = set(list2).symmetric_difference(set(list1))
    return list(diff)

diff = diffList(kwlist2, kwlist3)
print(diff)


