# Exercise 2.1.1

# Two as a whole number
Two = 2
print("Is Two of type Integer?", type(Two) is int)

# Two as a String
Two = "2"
print("Is Two of type String?", type(Two) is str)

# Type confusion
Integer = int
print("Of which type is the object Integer?", type(Integer))
