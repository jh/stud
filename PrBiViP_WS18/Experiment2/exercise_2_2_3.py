# Exercise 2.3

import sys

version = sys.version[0]

if version is '2':
    print("Python Version 2")
elif version is '3':
    print("Python Version 3")
else:
    print("Unknown version")
