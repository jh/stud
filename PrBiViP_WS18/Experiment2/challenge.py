#!/usr/bin/python3

import math

raspis = 512
pi_long = 10
pi_wide = 8
pi_tall = 4
rack_wide = 48.26
rack_tall = 186.9

# amount of raspis next to each other
x = math.floor(rack_wide / pi_long)
print("x:", x)
# amount of raspis above each other
y = math.floor(rack_tall / pi_tall)
print("y:", y)

# rack mounts required
print("Rack mounts required:", math.ceil(raspis / x))

# raspis in one stack
raspi_stack = x*y
print("Raspis per Stack:", raspi_stack)

# amount of stacks needed
stacks = raspis / raspi_stack
print("Stacks needed:", math.ceil(stacks), "(", stacks, ")")

