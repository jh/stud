#!/usr/bin/python3
# Exercise 6.1

import numpy as np
import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import cv2
from scipy.ndimage import filters
from mpl_toolkits.mplot3d import Axes3D

path = './'
 
def createTestImg():
    img = np.zeros((800, 800), dtype=np.uint8)
    img[:] = 255
    img[0:400, 0:400] = 0
    return img
  
def addNoise(img, dev):
    nImg = img + np.random.normal(loc=0.0,scale=dev,size=img.shape)
    return nImg
 
def computeDerivatives(img, sigma=3):
    Fm = np.zeros( img.shape )
    Fn = np.zeros( img.shape )
    filters.gaussian_filter( img, (sigma,sigma), (0,1), Fm )
    filters.gaussian_filter( img, (sigma,sigma), (1,0), Fn )    
    return Fm, Fn
  
def windowImg(imgWindow, img, Fm, Fn):
    frameSize = 20
    red = (255,0,0)
    m0, n0, m1, n1 = imgWindow[0], imgWindow[1], imgWindow[2], imgWindow[3]
    
    imgWithFrame = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB) 
    imgWithFrame[m0:m1,n0:n0+frameSize] = red
    imgWithFrame[m0:m1,n1-frameSize:n1] = red
    imgWithFrame[m0:m0+frameSize,n0:n1] = red
    imgWithFrame[m1-frameSize:m1,n0:n1] = red 
		 
    wFm = Fm[m0:m1, n0:n1]
    wFn = Fn[m0:m1, n0:n1]

    return wFm, wFn, imgWithFrame
  
img = createTestImg()
nImg = addNoise(img, 30)
Fm, Fn = computeDerivatives(img)
FmN,FnN = computeDerivatives(nImg)

flat, edge, corner = (600,200,700,300), (350,100,450,200), (350,350,450,450)
wFm1, wFn1, img1 = windowImg(flat, img, Fm, Fn)
wFm2, wFn2, img2 = windowImg(edge, img, Fm, Fn)
wFm3, wFn3, img3 = windowImg(corner, img, Fm, Fn)
wFm4, wFn4, img4 = windowImg(flat, np.uint8(nImg), FmN, FnN)
wFm5, wFn5, img5 = windowImg(edge, np.uint8(nImg), FmN, FnN)
wFm6, wFn6, img6 = windowImg(corner, np.uint8(nImg), FmN, FnN)

def plot_figure1():
    plt.figure(1)
    ###
    plt.subplot(4,3,1), plt.title('noiseless'), plt.axis('off')
    plt.gray(), plt.imshow(img)
    ######
    plt.subplot(4,3,4), plt.title('flat'), plt.axis('off')
    plt.gray(), plt.imshow(img1)
    ###
    plt.subplot(4,3,5), plt.title('Fm'), plt.axis('off')
    plt.gray(), plt.imshow(wFm1)
    ###
    plt.subplot(4,3,6), plt.title('Fn'), plt.axis('off')
    plt.gray(), plt.imshow(wFn1)
    ######
    plt.subplot(4,3,7), plt.title('edge'), plt.axis('off')
    plt.gray(), plt.imshow(img2)
    ###
    plt.subplot(4,3,8), plt.title('Fm'), plt.axis('off')
    plt.gray(), plt.imshow(wFm2)
    ###
    plt.subplot(4,3,9), plt.title('Fn'), plt.axis('off')
    plt.gray(), plt.imshow(wFn2)
    ######
    plt.subplot(4,3,10), plt.title('corner'), plt.axis('off')
    plt.gray(), plt.imshow(img3)
    ###
    plt.subplot(4,3,11), plt.title('Fm'), plt.axis('off')
    plt.gray(), plt.imshow(wFm3)
    ###
    plt.subplot(4,3,12), plt.title('Fn'), plt.axis('off')
    plt.gray(), plt.imshow(wFn3)
    plt.show()
#plot_figure1()

def plot_figure2():
    plt.figure(2)
    plt.subplot(4,3,1), plt.title('noisy'), plt.axis('off')
    plt.gray(), plt.imshow(nImg)
    ######
    plt.subplot(4,3,4), plt.title('noisy flat'), plt.axis('off')
    plt.gray(), plt.imshow(img4)
    ###
    plt.subplot(4,3,5), plt.title('Fm'), plt.axis('off')
    plt.gray(), plt.imshow(wFm4)
    ###
    plt.subplot(4,3,6), plt.title('Fn'), plt.axis('off')
    plt.gray(), plt.imshow(wFn4)
    ######
    plt.subplot(4,3,7), plt.title('noisy edge'), plt.axis('off')
    plt.gray(), plt.imshow(img5)
    ###
    plt.subplot(4,3,8), plt.title('Fm'), plt.axis('off')
    plt.gray(), plt.imshow(wFm5)
    ###
    plt.subplot(4,3,9), plt.title('Fn'), plt.axis('off')
    plt.gray(), plt.imshow(wFn5)
    ######
    plt.subplot(4,3,10), plt.title('noisy corner'), plt.axis('off')
    plt.gray(), plt.imshow(img6)
    ###
    plt.subplot(4,3,11), plt.title('Fm'), plt.axis('off')
    plt.gray(), plt.imshow(wFm6)
    ###
    plt.subplot(4,3,12), plt.title('Fn'), plt.axis('off')
    plt.gray(), plt.imshow(wFn6)
    plt.show()
#plot_figure2()


# Exercise 6.2

def plot_figure3():
    plt.figure(3)
    plt.subplot(321), plt.title('flat')
    plt.ylabel('Fn')
    plt.axis([-10, 10, -10, 10])
    plt.grid(True)
    plt.plot(wFm1[:], wFn1[:], 'rx')

    plt.subplot(322), plt.title('noisy flat')
    plt.axis([-10, 10, -10, 10]), plt.grid(True)
    plt.plot(wFm4[:], wFn4[:], 'rx')


    plt.subplot(323), plt.title('edge')
    plt.axis([-40, 40, -40, 40]), plt.grid(True)
    plt.plot(wFm2[:], wFn2[:], 'rx')

    plt.subplot(324), plt.title('noisy edge')
    plt.axis([-40, 40, -40, 40]), plt.grid(True)
    plt.plot(wFm5[:], wFn5[:], 'rx')

    plt.subplot(325), plt.title('corner')
    plt.axis([-40, 40, -40, 40]), plt.grid(True)
    plt.plot(wFm3[:], wFn3[:], 'rx')

    plt.subplot(326), plt.title('noisy corner')
    plt.axis([-40, 40, -40, 40]), plt.grid(True)
    plt.plot(wFm6[:], wFn6[:], 'rx')

    plt.show()
#plot_figure3()

# Exercise 6.3
def computeHarrisCorners(Fm, Fn, k=0.04, sigma=3):
    Mmm = filters.gaussian_filter( Fm*Fm, sigma=sigma )
    Mmn = filters.gaussian_filter( Fm*Fn, sigma=sigma )
    Mnm = filters.gaussian_filter( Fn*Fm, sigma=sigma )
    Mnn = filters.gaussian_filter( Fn*Fn, sigma=sigma )

    M = np.array( [ [Mmm, Mmn], [Mnm, Mnn] ] )
    detM = Mmm*Mnn - Mnm*Mmn
    traM = np.trace(M)
    #print("Mmm: ", Mmm.shape, ", M:", M.shape, ", detM:", detM.shape, ", traceM:", traM.shape)
    R = detM - k*(traM**2)

    return Mmm, Mmn, Mnm, Mnn, detM, traM, R

def plot_figure4():
    plt.figure(4)
    Mmm, Mmn, Mnm, Mnn, detM, traM, R = computeHarrisCorners(Fm, Fn)
    plt.subplot(221), plt.title('image'), plt.axis('off'), plt.gray()
    plt.imshow(img)

    plt.subplot(222), plt.title('corner reponse R'), plt.axis('off'), plt.gray()
    plt.imshow(R)

    plt.subplot(223), plt.title('det(M)'), plt.axis('off'), plt.gray()
    plt.imshow(detM)

    plt.subplot(224), plt.title('trace(M)'), plt.axis('off'), plt.gray()
    plt.imshow(traM)
    plt.show()
#plot_figure4()

def plot_figure5():
    plt.figure(5)
    Mmm, Mmn, Mnm, Mnn, detM, traM, R = computeHarrisCorners(FmN, FnN)
    plt.subplot(221), plt.title('noisy image'), plt.axis('off'), plt.gray()
    plt.imshow(nImg)

    plt.subplot(222), plt.title('corner reponse R'), plt.axis('off'), plt.gray()
    plt.imshow(R)

    plt.subplot(223), plt.title('det(M)'), plt.axis('off'), plt.gray()
    plt.imshow(detM)

    plt.subplot(224), plt.title('trace(M)'), plt.axis('off'), plt.gray()
    plt.imshow(traM)
    plt.show()
#plot_figure5()


def plot_figure67():
    Mmm, Mmn, Mnm, Mnn, detM, traM, R = computeHarrisCorners(Fm, Fn)
    fig = plt.figure(6)
    ax = fig.gca(projection='3d')
    x = np.arange(R.shape[1])
    y = np.arange(R.shape[0])
    X, Y = np.meshgrid(x, y)
    Z = R
    ax.plot_surface(X, Y, Z, rstride=20, cstride=20, alpha=0.3)
    cset = ax.contour(X, Y, Z, zdir='z')
    cset = ax.contour(X, Y, Z, zdir='x')
    cset = ax.contour(X, Y, Z, zdir='y')
    ax.set_xlabel('m')
    ax.set_ylabel('n')
    ax.set_zlabel('R(m,n)')

    Mmm, Mmn, Mnm, Mnn, detM, traM, R = computeHarrisCorners(FmN, FnN)
    fig = plt.figure(7)
    ax = fig.gca(projection='3d')
    x = np.arange(R.shape[1])
    y = np.arange(R.shape[0])
    X, Y = np.meshgrid(x, y)
    Z = R
    ax.plot_surface(X, Y, Z, rstride=20, cstride=20, alpha=0.3)
    cset = ax.contour(X, Y, Z, zdir='z')
    cset = ax.contour(X, Y, Z, zdir='x')
    cset = ax.contour(X, Y, Z, zdir='y')
    ax.set_xlabel('m')
    ax.set_ylabel('n')
    ax.set_zlabel('R(m,n)')
    plt.show()
#plot_figure67()

def detectCorners( R, threshold=0.5 ):
    threshold = threshold*R.max()
#    cornersCandidates = [R[x,y] for x in range(R.shape[0]) for y in range(R.shape[1]) if R[x,y] > threshold]
    cornerPos = [[y,x] for x in range(R.shape[0]) for y in range(R.shape[1]) if R[x,y] > threshold]
    return np.array(cornerPos)

def plot_figure8():
    Mmm, Mmn, Mnm, Mnn, detM, traM, R = computeHarrisCorners(Fm, Fn)
    cornerPos = detectCorners(R)
    plt.figure(8)
    plt.subplot(221), plt.title('corners in image'), plt.axis('off')
    plt.gray(), plt.imshow(img), plt.plot(cornerPos[:,0], cornerPos[:,1], 'ro')

    plt.subplot(223)
    plt.text(0.2, 0.8, str( len(cornerPos) ) + ' corners found', fontsize=14)
    plt.axis('off')

    Mmm, Mmn, Mnm, Mnn, detM, traM, R = computeHarrisCorners(FmN, FnN)
    cornerPos = detectCorners(R)
    plt.subplot(222), plt.title('corners in noisy image'), plt.axis('off')
    plt.gray(), plt.imshow(nImg), plt.plot(cornerPos[:,0], cornerPos[:,1], 'ro')
    
    plt.subplot(224)
    plt.text(0.2, 0.8, str( len(cornerPos) ) + ' corners found', fontsize=14)
    plt.axis('off')
    plt.show()
#plot_figure8()

# Exercise 6.4

def rotateImg(img, deg=45, scale=1.0):
    h, w = img.shape[:2]
    M = cv2.getRotationMatrix2D( (int(w/2),int(h/2)), deg, scale )
    return cv2.warpAffine(img, M, (h,w))

def plot_figure9():
    plt.figure(9)
    img = rotateImg(createTestImg())
    nImg = addNoise(img, 3)
    Fm, Fn = computeDerivatives(img)
    FmN,FnN = computeDerivatives(nImg)

    Mmm, Mmn, Mnm, Mnn, detM, traM, R = computeHarrisCorners(FmN, FnN)
    plt.subplot(221), plt.title('noisy image'), plt.axis('off'), plt.gray()
    plt.imshow(nImg)

    plt.subplot(222), plt.title('corner reponse R'), plt.axis('off'), plt.gray()
    plt.imshow(R)

    plt.subplot(223), plt.title('det(M)'), plt.axis('off'), plt.gray()
    plt.imshow(detM)

    plt.subplot(224), plt.title('trace(M)'), plt.axis('off'), plt.gray()
    plt.imshow(traM)
    plt.show()
#plot_figure9()

# Exercise 6.5

def importAndConvertImg( imgPath, imgSize=800 ):
    img = cv2.imread( imgPath )
    h, w = img.shape[:2]
    if h != imgSize:
      scaleFactor = int(imgSize/h)
      img = cv2.resize(img, (imgSize, imgSize), interpolation=cv2.INTER_LINEAR)
    return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

def plot_figure10():
    img = importAndConvertImg('chessfoto.png')
    Fm, Fn = computeDerivatives(img)

    plt.figure(9)
    Mmm, Mmn, Mnm, Mnn, detM, traM, R = computeHarrisCorners(Fm, Fn)
    plt.subplot(321), plt.title('image'), plt.axis('off'), plt.gray()
    plt.imshow(img)

    plt.subplot(322), plt.title('corner reponse R'), plt.axis('off'), plt.gray()
    plt.imshow(R)

    plt.subplot(323), plt.title('det(M)'), plt.axis('off'), plt.gray()
    plt.imshow(detM)

    plt.subplot(324), plt.title('trace(M)'), plt.axis('off'), plt.gray()
    plt.imshow(traM)

    cornerPos = detectCorners(R, threshold=0.5)
    plt.subplot(325), plt.title('corners in image'), plt.axis('off')
    plt.gray(), plt.imshow(img), plt.plot(cornerPos[:,0], cornerPos[:,1], 'ro')
    
    plt.subplot(326)
    plt.text(0.2, 0.8, str( len(cornerPos) ) + ' corners found', fontsize=14)
    plt.axis('off')

    plt.show()
plot_figure10()

