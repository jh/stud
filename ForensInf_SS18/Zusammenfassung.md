# Zusammenfassung Foresische Informatik SS 18

## Klassische Forensik

**Forensisch**: gerichtlichen oder kriminologischen Zwecken dienend, im Dienste der Rechtspflege stehend, gerichtlich

Klassische Forensik beschäftigt sich mit physischen Spuren, digitale Forensik mit digitalen Spuren.

*Rechtliche Frage*: Hat Person X den Mord an Person Y begangen?
-> *Wissenschaftliche Frage*: Finden sich genetische Sequenzen von X am Tatwerkzeug mit dem Y getötet wurde?
(Abstraktion von Schuld)

*Spur*: hinterlassenes Zeichen, "alle materiellen Veränderung der Umwelt, die im Zusammenhang mit der Begehung der Tat entstanden sind"

*Ereignis*: Feststellung des Kontakts auf Basis einer Spur; Ereignisse müssen individuell hergeleitet und begründet werden (Grundlage: Assoziation)

*Assoziation*: Vorgang der Feststellung eines Kontakt zwischen zwei Objekten (Ergebnis der Assoziation: Ereignis)

*Rekonstruktion*: Ereignisse in einen räumlichen/zeitlichen Zusammenhang bringen

## Dateisystemeanalyse Methodik

physische Ebene > Partitionsebene > Dateisystemebene > Dateiebene / Anwendungsebene (Dateiformate etc.)

Referenzmodell nach Carrier:

1. Dateisystemdaten (Informationen über das Dateisystem selbst, z. B. Layout, Sektorengröße, Superblock, ...)
2. Inhaltsdaten (Dateiinhalte)
3. Metadaten (Daten über Dateien, z. B. Zeitstempel, Zugriffsrechte, ... im inode)
4. Dateinamensdaten
5. Anwendungsdaten ("Der Rest", z. B. Dateisystem-Journale)

**Essential Data**: those that are needed to save and retrieve files (z. B. Verweis auf Datenblöcke)

**Non-Essential Data**: are there for convenience and not needed for basic functionality (z. B. Zeitstempel oder Zugriffsrechte)

Essentielle Daten haben höheren Beweiswert und Vertrauenswürdigkeit als nicht-essentielle Daten

## Digitale Spuren

**Digital evidence**: "any data stored or transmitted using a computer that support or refute a theory of how an offense occured or that address critical elements of the offense such as intent or alibi"

* Wann sind digitale Spuren original (vom Tatort) / authentisch (vom Täter)?
* Inwiefern sind digitale Spuren personenbezogen?
* Volatile Bindung zwischen Spur und Träger bei digitalen Spuren

CSI-Modell für Spuren: **c**laim, **s**upport, **i**nformation

## Dokumentation und Vorgehensmodelle

Wichtig ist, *dass* man ein Modell hat, nicht genau welches (Vielzahl an Vorgehensmodellen)

Aufgabentrennung zwischen *Polizeiermittler* (treibt Ermittlungsverfahren voran, bildet Hypothesen und prüft Hypothesen anhand von Spuren), Ersteinschritter bzw. *Spurensicherung* (versucht Spuren möglichst unverändert ins Labor zu schaffen) und *forensischen Wissenschaftlern* (untersucht Spuren im Labor).

Wichtig: *richtige* Kommunikation zwischen den Parteien (auf Zielpublikum achten) und wissenschaftliche Arbeitsweise (versuchen sich selbst zu widerlegen und glaubwürdig bleiben)

**Basic Mindset**:

* Nichts verändern (Dokumentation bei wissentlicher Veränderung)
* Alles immer und überall nachvollziehbar machen

Fragen der Juristen (Sicherungsphase):

1. Waren Ihre technischen Geräte während der Datensicherung hinreichend geschützt?
2. Wie wurde sichergestellt, dass auch tatsächlich die richtigen Daten gesichert wurden?
3. Inwiefern ist sichergestellt, dass die Daten unverändert (im Original) gesichert wurden?

Fragen der Juristen (Analysephase):

1. Welche relevanten Daten konnten auf welchen Asservaten gefunden werden? Wo wurden sie jeweils gefunden und wie wurden sie gesichert?
2. Inwiefern waren die Daten für den Verdächtigen zugreifbar?
3. Kann festgestellt werden, wann die Daten auf dem Datenträger abgespeichert wurden und wann auf diese zuletzt zugegriffen bzw. diese verändert wurden?
4. Wurden gelöschte Daten wie Fotos, Textnachrichten oder Videos korrekt gesichert?
5. Sind die im Netzwerk übertragenen Daten korrekt dargestellt?

Weitere Frage zur Analyse:

1. Wie sind die digitalen Spuren auf dem Spurenträger entstanden?
2. Waren die Daten wie erwartet?
3. Welche alternativen Hypothesen für die Entstehung der Daten gibt es?
4. Wieviel Fachkenntnis ist nötig, um eine bestimmte Handlung/Manipulation durchzuführen?

### Dokumentation

Dokumentation der Datensicherung vor Ort durch Fotoaufnahmen

Mantra der forensischen Informatik: Dokumentieren, Dokumentieren, Dokumentieren.

### Vorgehensmodelle

*Computer Security Incident* is a "violation or imminent threat of violation of computer security policies, acceptable use policies, or standard security practices"

**Incident Response**: Reaktion einer Organisation auf einen Sicherheitsvorfall (Orgnisationen müssen in wohl definierter Weise auf Zwischenfälle jeglicher Art reagieren)

**Investigative Process**: Ziel: gerichtsverwertbare Sicherung digitaler Spuren

## Digitale Ermittlungen

Die Wahrheit herausfinden:

* Was ist passiert? (*Straftat*)
* Wo? (*Tatort*)
* Wann? (*Tatzeit*)
* Wer? (*Täter*)
* Warum? (*Tatmotiv*)
* Wie? (*Tathergang*)
* Womit? (*Tatwaffe*)

Beachten der **Erfahrungsfalle** - jeder Fall ist neu und einzigartig!

Ermittlungsverfahren dient der Klärung eines Verdachts

Zielt auf "hohen Grad an Wahrscheinlichkeit" bzw. "Überzeugung von Wahrheit" ab

Selektion bei der Sicherung vs. Priorisierung bei der Analyse

## Timelining und Logdateianalyse

**Quellen von Zeitinformationen:**

* Dateisystemmetadaten (Frage nach Korrektheit der Uhr)
* System-Logdateien (Syslog, Windows Event Log, etc.)
* Anwendungs-Logdateien (Browserverlauf, Debug-Dateien etc., manchmal als SQL Datenbank)
* Usability-Dienste (Siri, Cortana, Zeitgeist, ...)
* Cache-Dateien (oft implizite oder explizite Zeitinformationen vorhanden)

*Interne Inkonsistenz*: Unregelmäßigkeit / Manipulation innerhalb der Logdatei(en) einer Anwendung (z. B. Verletzung der Monotonie-Bedingung)

*Externe Inkonsistenz*: Unregelmäßigkeit beim Vergleich von Logdateien verschiedener Anwendungen

## Festplattentechnik

### Firmware

Festplatte ist eigener kleiner Computer (mit Prozessor, ROM, Flash etc.)

Gefahren beim Auslesen:

* Infiziertes Boot ROM (ROM ist nicht schreibbar, muss vom Hersteller kommen, "Hardware Trojaner")
* Manipulierte Firmware auf EEPROM / Flash: Firmware kann neu geflasht werden (z.B. für legitime Firmware Upgrades), Gefahren analog zu infiziertem Betriebssystem ("rootkit")

Ausschluss von Manipulation an der Firmware der Festplatte sehr schwierig,
da moderne Festplatten eine "Service Area" mit speziellen Bootcode für die Festplatte enthalten.
Dieser Bootcode ist vollkommen proprietär und intransparent und kann nur von dem Festplattencontroller selbst gelesen werden,
da dieser Abschnitt in "negativen Sektoren" liegt.

vereinfachende (implizite) Annahme: "Festplatte sagt die Wahrheit"

Firmware kann aktuell nur mit Herstellerhilfe verifiziert werden.

Einfache Abhilfe: signierte Firmware-Updates

### Laufwerk vs. Partition

Laut Carrier:

* Laufwerk (Volume): Menge von addressierbaren Sektoren
* Partition: Menge von aufeinanderfolgenden Sektoren in einem Laufwerk

Partitionstabelle organisiert das Layout des Laufwerks (wichtige Einträge: Start- und Endsektor jeder Partition)

### Festplattengeometrie

C - Cylinder - Zylinder
H - Head - Schreib-Lese-Kopf
S - Sector - Sektor (von außen nach innen auf der Scheibe durchnummeriert)

Eindeutige Adressierung durch CHS möglich

Moderne Festplatten: logische Blockadressierung (LBA, logical block addressing) -> physische Adresse (*phyisical address*) eines Sektor auf der Festplatte

**Logische Laufwerksadresse** (*logical volume address*) ist Adresse des Sektors relativ zum Beginn des Laufwerks (logische Adresse gleich physikalische Adresse)

**Logische Partitionsadresse** (*logical partiton volume address*) ist Adresse des Sektores relativ zum Beginn der Partition

Umrechung CHS - LBA:

LBA | C H S
----|---
0   | 0,0,1
1   | 0,0,2
2   | 0,0,3
... | ...
n   | 0,1,1

### Festplattentechnologie

```
+-------------------------+-----+-----+
| user addressable sector | HPA | DCO |
+-------------------------+-----+-----+
```

**HPA**: host protected area (spezieller ATA Bereich am Ende der Festplatte)

**DCO**: device protection overlay (seit SATA-6 spezieller Bereich hinter HPA, z.B. reserviert für fehlerhafte Blöcke)

### Flashspeichertechnologie

**Page**: kleinste Einheit, die geschrieben werden kann (üblich 2-16 KiB)

**Block**: kleinste Einheit, die gelöscht werden kann (üblich 4-8 MiB)

Wear Leveling sorgt für eine gleichmäßige Abnutzung aller Flashzellen.
Durch den TRIM Befehl weiß die SSD, welche Blöcke nicht mehr gebraucht werden.


## Forensischer Zugriff auf Datenträger

Regeln des forensischen Zugriffs:

### 1. "So tief wie nötig, so hoch wie möglich"
Auf jeder Abstraktionsstufe gehen Daten verloren -> mas sollte Daten "so tief wie nötig" aber "so hoch wie möglich" sichern (tief genug, um alle relevanten Spuren zu finden - hoch genug, um nicht zu viele Daten zu haben)

Daten auf höherer Ebene sind in niedrigeren Ebenen eingebettet - niedriegere Ebenen enthalten Metadaten über diese Daten

### 2. "Weg durch die Hierarchiestufen dokumentieren"

Dead Acquisition: Sicherung der Daten *ohne* die Hilfe des Betriebssystems auf dem Rechner selbst (schwierig, da System nicht vertrauenswürdig)

Live Acquisition: Sicherung *mit* Hilfe des Betriebssystems

### 3. "Nichts verändern!"

So wenig wie möglich verändern, und das dokumentieren, was man wissentlich verändert.

siehe z.B. "Hardware Write Blocker" für Festplatten

Problem: asynchrone Garbage Collection in SSDs (TRIM)

**Dokumentation der Integrität**: Nutzung kryptographischer Hashfunktionen zur Dokumentation und Wahrung der Integrität

**Strikt und partiell essentielle Daten**:

> Essential file system data are those that are needed to save and retrieve files.
> Non-essential file system data are there for convenience and not needed for the basic functionality.
-- Carrier

> While strictly essential corresponds to Carrier's definition, partially essential refers to application / OS specific interpretations.
-- Freiling, Schuhr, Gruhn

Vertrauenshierarchie für (Meta)Daten: strictly essential >= partially essential >= non-essential

## NTFS

*New Technology File System*

Entwurfsmuster: "Alles ist eine Datei!" (auch Dateisystemdaten sind in Daten abgelegt, allerdings vor dem Benutzer versteckt, da Hidden-Flag gesetzt)

Keine offizielle Spezifikation, v.a. Reverse Engineering -> NTFS kann sich (durch Microsoft) spontan ändern, oft unklar was ein "valides NTFS" ist.

### MFT

Zentrale Datenstruktur in NTFS: Master File Table (MFT)

Im Bootsektor steht der Anfang der MFT. MFT selbst ist Datei mit MFT-Einträgen, die theoretisch beliebig wachsen kann.

MFT-Eintrag: 1 KB, ersten 42 Byte sind Kopf (fest definiert), danach folgt beliebige Sammlung von Attributen. Unbenutzter Speicher am Ende des MFT-Eintrags (aufgrund fester Größe) -> Slack-Space / Fragmentierung, Signatur am Anfang des Eintrags: "FILE" oder "BAAD"

Adressierung von MFT-Einträgen via Reference Address

---|---
0  |
1  |
2  |
.. |
78 | 0 (16bit Sequenznummer)


**Reference Address** (64 Bit):

File Number | Sequenznummer
------------|--------------
(48 Bit)    | (16 Bit)
78          | 0

Sequenznummer (teil der Reference Address) erlaubt Ruckschlüsse auf Aktualität der gefundenen Dateien.

**MFT-Attribute**:

Attributkopf (16 Bit): type identifier, Größe des Attributs, Name des Attributs, Bits ob Inhalt komprimiert / verschlüsselt / resident ist.

Attributinhalt: Inhalt völlig frei wählbar, nicht-residente Attribute werden in Clustern gespeichert (Attributkopf enthält Liste von *Cluster Runs*, sehr effiziente Darstellung).

Type $DATA und resident => Daten werden direkt im MFT-Eintrag gespeichert (für kleine Dateien)

Standardattribute:

* $FILE_NAME (type identifier 48): Dateiname und Zeitstempel
* $STANDARD_INFORMATION (type indentifier 16): Besitzer, Zugriffsinformationen, Zeitstempel (MAC-Zeiten (modified, accessed, created) in 100 Nanosekunden seit 1. Januar 1601)
* $DATA (type identifier 128): Dateiinhalt

### Metadateien

Dateisystemdaten werden in verschiedenen Dateien abgelegt -> Metadateien (die ersten 16 MFT-Einträge sind für solche Metadateien reserviert)

Name von Metadateien beginnt mit $ und mindestens einem Großbuchstaben (werden standardmäßig versteckt)

* $MFT ist wichtigiste Metadatei (Startpunkt steht im Bootsektor, wird benötigt um alle anderen Dateien zu finden)
* $MFTMirr: Kopie der MFT, MFT-Eintrag 1
* $Boot: Volume Boot Record, einzige Datei mit statischem Layout, MFT-Eintrag 7
* $Volume: Laufwerksinformationen (Laufwerksname, NTFS-Versionnummer, ...), MFT-Eintrag 3
* $AttrDef: enthält im $DATA-Attribut Namen und type identifier aller Attribute -> Umdefinierung möglich, MFT-Eintrag 4 (Henne-Ei-Problem: Wie soll man $AttrDef lesen, ohne type identifier zu kennen? => Beim ersten Lesen werden die Standardwerte verwendet)
* $Bitmap: zeigt Belegtstatus der Cluster an, MFT-Eintrag 6
* $BadClus: speichert beschädigte Cluster, MFT-Eintrag 8


## FAT

Original Dateisystem von MS/DOS

Drei verschiedene Versionen: FAT12, FAT16, FAT32 (, exFAT)

Basiskonzept: Jede Datei / Verzeichnis besitzt einen Verzeichniseintrag (directory entry)

Verzeichniseintrag:

Dateiname | Metadaten (z.B. Größe) | Referenz auf erstes Cluster der Datei
----------|------------------------|---
file.dat  | 4000 Byte              | 34

Verweise auf folgende Cluster (Dateisystemblöcke) werde in der FAT gespeichert.

Beispiel für File Allocation Table:

Cluster Nummer | Nächster Block
---------------|---
...            |
33             |
34             | 35
35             | 37
36             |
37             | EOF
...            |

=> file.dat hat Cluster 34, 35 und 37

Layout einer FAT Partition:
```
+--------------+---------+--------------+
| Reservierter | FAT-    | Datenbereich |
| Bereich      | Bereich |              |
+--------------+---------+--------------+
```

FAT12/16:

* Reservierter Bereich == Boot Sektor (Größe: 1 Sektor)
* FAT-Bereich enthält FAT genau ein Mal (aber: trotzdem Slack-Space am Ende)
* Wurzelverzeichnis liegt im ersten Sektor des Datenbereichs

FAT32:

* Reservierter Bereich ist größer als Boot Sektor
* FAT32 enthält n-Kopien der FAT im FAT-Bereich (evtl. Slack-Space am Ende dieses Bereiches)
* Wurzelverzeichnis kann irgendwo im Datenbereich stehen (Verweis steht im Boot Sektor)
* Backup-Boot-Sektor in Sektor 6 (-> fester Offset zwischen Primärem und Backup-Boot-Sektor kann zum Carven genutzt werden)

File System Slack:

* Hinter dem Ende des letzten Clusters (= Gruppe mehrerer Sektoren) können sich noch (wenige) unbenutzte Sektoren finden
* Zwischen dem Ende des letzten validen FAT-Eintrages und dem Beginn des Datenbereichs können Daten versteckt werden

Anzahl von Sektoren pro Cluster steht im Boot Sektor

FAT hat einen Eintrag pro Cluster im Datenbereich

* 0x0: Cluster unbelegt
* 0xFF: EOF
* 0xFF7 (FAT12) bzw. 0xFFF7 (FAT16) bzw. 0x0FFF FFF7 (FAT32): Cluster beschädigt
* alle anderen Werte: Cluster belegt

Inhaltsdaten: beschreiben Datei- und Verzeichnisdaten, organisiert in Dateisystemblöcken (Cluster)

Cluster beginnen bei Adresse 2, erstes Cluster liegt **nicht** am Anfang des Dateisystems => Logische Laufwerksadresse != logische Dateisystemadresse

Metadaten (Dateinamen, Attribute, Größe, Logische Dateisystemadresse des ersten Clusters, Zugriffszeiten) werden in Verzeichniseintrag (directory entry, 32 Byte) gespeichert.

Bei Löschung wird das erste Byte des Verzeichniseintrag auf 0xE5 gesetzt (=> erster Buchstabe des Dateinamens geht verloren) und die dazugehörigen Clustereinträge in der FAT werden auf 0x0 gesetzt, die Cluster selbst werden jedoch nicht genullt.

## Ext (UFS/Ext2/Ext3/Ext4)

ExtX (*extended*) zielt wie UFS (*unix file system*) auf **Geschwindigkeit** (Datenblöcke un einer Datei werden sehr nah beieinander gespeichert, um Wege des Lesekopfes kurz zu halten) und **Zuverlässigkeit** (wichtige Datenstrukturen sind mehrfach auf dem Datenträger gespeichert) ab

Kategorisierung der Zusatzfunktionen in Compatible Features, Incompatible Features und Read-only Features

Standardlayout:

```
+----------------------+--------------+--------------+-----+
| reservierter Bereich | Blockgruppe1 | Blockgruppe2 | ... |
+----------------------+--------------+--------------+-----+
```

Reservierter Bereich:

* Bootcode (ersten 1024 Byte)
* Superblock (enthält Magic Value, Metadaten und Konfiguration zum Dateisystem, z. B. Größe eines Blocks (in Sektoren), Anzahl der Blöcke pro Blockgruppe, Anzahl der Blockgruppen, Anfang der Blockgruppen)
* Slack Space

Blockgruppe ist Menge von Datenblöcken auf dem Datenträger.
*Jede* Blockgruppe enthält einen Gruppedeskriptor und die Dateisystemblöcke.

**Gruppendeskriptor**: Backup des FS-Superblock, Gruppendeskriptor Tabelle (enthält Verweise auf alle anderen Blockgruppen), Block Bitmap (verwaltet Belegstatus der Blöcke in der Blockgruppe), Inode Bitmap, Inodes


**inodes** stellen Dateien oder Verzeichnisse dar, enthalten Metadaten, haben eindeutige Nummber

* inode 1: sammelt typischerweise beschädigte Blöcke
* inode 2: `/` Wurzelverzeichnis
* inode 8: enthält typischerweise das Journal
* inode 11: oftmals `lost+found` (belegte Indexknoten, auf die kein Verzeichniseintrag zeigt)

Inode Inhalt:

* Dateigröße
* Besitzer (UID, GID, Zeitstempel)
* Zeitstempel (in Unix Time): Letzter Zugriff, letzte Änderung des Inhalts der Datei, letzte Änderung der Metadaten der Datei, Löschung der Datei (ersten Beiden können mittels `touch` manipuliert werden)
* Mode (RWX, SUID, SGID, ...)
* Verweiszähler (Anzahl der Verzeichniseinträge, die auf diesen inode zeigen)

Verzeichnis mit Verzeichniseinträgen
Verzeichniseintrag referenziert inode
Inode enthält Metadaten zur Datei sowie Verweise auf Datenblöcke
(Dateiname ist nicht Teil der Metadaten)

Dateinamen (inklusive Inode-Nummer) sind als verkettete Liste im Verzeichnis gespeichert -> bei Löschung wird Eintrag "ausgereiht".

Beim Löschen der Datei wird (zunächst) nur die Referenz gelöscht -> Link Count in den Metadaten wird dekrementiert.

Journal führt Updates in Form von Transaktionen durch.
Es speichert:

* Welche Updates auf Blöcken durchgeführt werden sollen
* Ob Update fertig wurde
* Fortlaufende Nummer des Updates


## Strafprozessordnung (StPO) Einführung

Gastvortrag Christoph Safferling

### Ziel und Funktion eines Strafverfahrens:

* Wahrheit
* Gerechtigkeit
* Rechtsfrieden
* Durchsetzung des materiellen Strafrechts
* Verdachtsklärung

### Prozessmaximen (allgemeine, übergeordnete Verfahrensgrundsätze):

* Offizialprinzip: Ermittlung wird nicht von Privatpersonen sondern von Amtsstellen durchgeführt
* Inquisitionsprinzip: Richter dominiert das Hauptverfahren -> kein Parteiprozess
* Untersuchungsgrundatz und Amtermittlungsprinzip: Gericht muss zu Lasten **und** zu Gunsten des Beschuldigten untersuchen
* Rechsstaatlichkeit und Menschenwürde, Subjektstellung des Angeklagten (Rechtsweggarantie, Unschuldsvermutung, Grundatz des rechlichen Gehörs, Unabhängigkeit und Unparteilichkeit des Gerichts, Recht auf Verteidiger, Schweigerecht, Beschleunigungsgebot und Konzentrationsmaxime)
* Grundatz der Mündlichkeit
* Anwesenheitsgrundsatz
* Öffentlichkeitsgrundsatz

### Zwangsmaßnahmen:

* Überwachung der Telekommunikation, Quellen-TKÜ
* Großer Lauschangriff (Abhören von Wohnungen)
* Auskunft über Telekommunikationsverbindungen
* "IMSI-Catcher"
* Einsatz "Verdeckter Ermittler" (Beamte des Polizeidienstes)
* Untersuchungshaft
* Festnahmerecht
* Identitätsfestellung (Anhalten, Festhalten, Verbringen zur Dienststelle, Durchsuchung von Person oder mitgeführter Sachen)

### Beweismittel:

* Zeuge
* Urkunde
* Sachverständige
* Augenschein
* **nicht** der Angeklagte selbst

### Sachverständige

Wird benötigt, wenn das Gericht nicht über eigene Sachkunde verfügt.
Auswahl des Sachverständigen durch Richter, aber nach StPO auch Staatsanwaltschaft oder Polizei.
Verteidigung "soll" bei der Auswahl des Gutachters gehört werden (nicht verpflichtend).

* Gutachtensauftrag -> klare und eindeutige Auftragsbeschreibung
* Pflicht des SV zur Erstattung des Gutachtens (aber Gutachtensverweigerungsrecht oder Ablehnung möglich, z.B. bei Voreingenommenheit)
* Möglichkeit der Vereidigung des SV (selten)


## Spurentheorie

Defizite:

* Mangel an grundlegender Theorie
* Mangelnde Orientierung an etablierten Methoden und Prinzipien der Wissenschaft

### Rekonstruktion

Computer als Zustandsautomat mit Ereignissen an den Zustandsübergängen

-> Ziel der forensischen Informatik: Wie ist der Computer in einen bestimmten Zustand gekommen?

Problem: Frage ist nicht immer eindeutig beantwortbar (mehrere Pfade führen zu einem Zustand; komplexer Zustandsraum)


### Allgemeines Rekonstruktionsproblem (GRP)
Für ein gegebenes System und einen gegebenen Zustand,
finde alle Pfade (Aktionen), die zu diesem Zustand führen.

### Spezifisches Rekonstruktionsproblem (SRP)

Für ein gegebenes System, eine gegebene Aktion und einen gegebenen (End-) Zustand, entscheide welcher der folgenden Fälle zutrifft:

* die Aktion hat statt gefunden
* die Aktion hat definitiv nicht statt gefunden
* die Aktion hat möglicherweise statt gefunden

### Spuren

**Spurenmenge** (evidence E) einer Aktion: Menge aller Teilmengen von Zuweisungen die von der Aktion ausgeführt werden.

**Charakteristische Spuren** (CE) einer Aktion ist die Menge aller Spuren einer Aktion, die nicht durch andere Aktionen erzeugt werden können oder im Initialzustand enthalten waren.

=> CE: Aktion ist ausgeführt worden

**Kontraspuren** (counter evidence XE): Menge von Teilmengen von Zuweisungen, die von der Aktion _nicht_ ausgeführt werden, aber nur von Variablen, denen die Aktion einen Wert zuweist.

=> CXE: Aktion ist nicht ausgeführt worden

CE und CXE schließen sich gegenseitig aus

Interferenz zwischen Zuweisungen verschiedener Aktionen
