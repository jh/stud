---
author: "Jack Henschel"
title: "Forensischer Bericht zur Asservatennummer 35/17/2015"
header: "Forensischer Bericht Asservatennr. 35/17/2015"
date: 2018-05-07
lang: de-DE
papersize: a4
documentclass: article
fontsize: 12pt
toc: true
titlepage: true
toc-own-page: true
---

## Prolog

### Beweismittelkette

Das Asservat `exercise_1.tgz` (Asservatennummer 35/17/2015) wurde am 17.04.2018 vom Portal 'StudOn' über eine gesicherte Verbindung heruntergeladen.

Die Staatsanwaltschaft hat folgende SHA-256-Prüfsumme übermittelt, welche mit der Prüfsumme des vorliegenden Asservats übereinstimmt:
```
1f23fcf72f931e14a2762b3014b97f51e5031c045129d044287457a996b0c4cc  exercise_1.img
```

Dieses Asservat wird während der gesamten Untersuchung auf einem Rechner mit Festplattenvollverschlüsselung LUKS mit dem Verschlüsselungsverfahren AES-XTS-Plain64 sowie 512 Bit Schlüsselgröße.
Am Ende der Untersuchung wird die Hashsumme erneut geprüft.

### Arbeitsumgebung

Bei dem Rechner auf dem die Untersuchungen durchgeführt werden handelt es sich um ein speziell angefertigtes x86-System, bestehend aus einem Intel i5-2500K Prozessor, 8GB DDR3 Arbeitsspeicher, einer MSI Z77A-GD55 Hauptplatine sowie einer 120GB A-DATA SSD als nichtflüchtigen Datenspeicher.

Auf dem Rechner selbst läuft ein Debian Stretch GNU/Linux System (9.4), worauf die Tools "The Sleuth Kit" in Version 4.4.0 und "TestDisk" in Version 7.0 installiert sind.

### Auftrag

Der Untersuchungsauftrag besteht aus der Prüfung des Besitzes illegaler Nashornographie gemäß Paragraph 184i des Strafgesetzbuches.

Bei der Hausuntersuchung vom 25.10.2016 wurde in der Wohnung von Herrn S. eine externe USB-Festplatte der Marke Seetor (Asservatennummer 35/17/2015, Baujahr 2007) beschlagnahmt.

Es soll ermittelt werden, ob auf dem Datenträger Bilddateien rhinographischer Natur sind und falls ja, in wie weit Herr Jürgen S. wissentlich im Besitz dieser ist beziehungsweise war.

\newpage

## Zusammenfassung

Zunächst wurde bei der Untersuchung die Integrität des Datenträgerabbilds überprüft.
Danach wurde das Datenträgerabbild systematisch auf potentiell rhinographische Bilddateien sowie Spuren bezüglich Modfikationen untersucht.

Ein Vorwort zu Metadaten: Metadaten beschreiben den Inhalt anderer Daten, geben also bespielsweise zusätzliche Informationen zu Bilddateien wie Bildtyp, Auflösung oder Datum.
Diese können jedoch mit speziellen Programmen, die bereits nach einer Internetrecherche von nur wenigen Minuten zu finden sind, von jedem Laien hinzugefügt, entfernt oder modifiziert werden, sofern ihm diese Funktionalität bekannt ist.

Bei der Untersuchung kam zum Vorschein, dass eine der auf dem Datenträger befindlichen Partitionen gelöscht worden sein muss.
Trotz dieser Löschung konnte darauf ein potentiell rhinographisches Foto sichergestellt werden (siehe Anhang/f0000001.jpg).
Das Bild wurde laut Metadaten am 22. April 2008 um 13:49 erstellt. Da sich die Datei auf einem nicht benutzten Abschnitt des Abbildes befand, ist es vorstellbar, dass Herr Jürgen S. nichts von der Existenz dieser Datei wusste, da er angab das Medium vor drei Jahren gebraucht erworben zu haben.

Auf dem benutzten Abschnitt der Festplatte wurden zwei weitere Dateien mit potentiell rhinographischen Inhalten gefunden.
Bei der Ersten handelt es sich um eine Grafik, die laut EXIF-Metadaten mit dem Programm Adobe Photoshop CS3  am 20. Februar 2009 um 14:17 erstellt wurde (siehe Anhang/nashorn.jpg).
Bei der Zweiten handelt es sich um eine Fotografie, die laut EXIF-Metadaten mit einer Canon EOS-1D Mark III am 9. März 2011 um 16:19 aufgenommen wurde.
Diese Datei wurde im Windows Betriebssystem gelöscht, da die Referenz auf die Datei fehlte.
Sie ließ sich aber dennoch wiederherstellen.
Da beide dieser Dateien direkt im Hauptverzeichnis des externen Datenträgers liegen, ist davon auszugehen, das der Besitzer des Mediums bei der letzten Benutzung des Datenträgers von ihrer Existenz gewusst hat.
Jedoch ist grundsätzlich möglich, dass eine dritte Person den Datenträger benutzt hat und diese Dateien darauf kopiert hat.
Laut Dateisystem-Zeitstempel wurden die Dateien auf dem Datenträger zuletzt am 23. September 2015 um 16:49 modifiert, was ein Jahr vor dem Zeitpunkt der Beschlagnahmung des Datenträgers liegt (25.10.2016).

\newpage

## Technischer Teil

Nachdem die Checksumme des Datenträgerabbilds überprüft wurde, muss zunächst der Partitionstyp des 20MB großen Abbilds überprüft werden.

Der Einsatz des Analyseprogramms `mmstat`, welches Teil des "Sleuth Kit" ist, zeigt, dass es sich um ein klassisches DOS/MBR-Partitionsschema handelt.
```
$ mmstat exercise_1.img
dos
```

Unter Einsatz des `mmls` Kommandos wird die Partitionstabelle des Datenträgerabbilds ausgelesen.
```
$ mmls exercise_1.img
DOS Partition Table
Offset Sector: 0
Units are in 512-byte sectors

      Slot      Start        End          Length       Description
000:  Meta      0000000000   0000000000   0000000001   Primary Table (#0)
001:  -------   0000000000   0000003455   0000003456   Unallocated
002:  000:000   0000003456   0000040959   0000037504   Linux (0x83)
```
Da die Partitionstabelle korrekt ausgelesen werden konnte, handelt es sich in der Tat um ein DOS-Partitionsschema.
In diesem Fall besteht es aus drei Bereichen:
der erste Bereich, welcher nur eine Größe von einem Sektor (entspricht 512 Byte) aufweist, enthält den Master Boot Record (MBR).
Danach folgt ein nicht verwendeter Bereich von 3455 Sektoren (1.768.960 Byte).
Der letzte Bereich beginnt direkt nach dem zweiten Bereich und geht bis zum Ende des Abbilds (20MB). Er hat eine Größe von 19.202.048 Byte und ist vom Typ "Linux Ext".

Um die einzelnen Partitionen besser untersuchen zu können, werden diese nun aus dem Datenträgerabbild extrahiert und die zugehörigen Hashsummen generiert, um die Integrität der Daten zu gewährleisten.

```
for i in {0..2}; do
    mmcat exercise_1.img $i > part$i
    sha256sum part$i
done

037cb5a6a61002be8dda5cc041bcfb654735275c4e535ca22fd4c5b6436ccc1a  part0
a05c5509c0226581e78acafb72136a251e0260862c8f5cfb460b92df8491d183  part1
3b468ac788cf588145c42e3b4c26ae909f489b7e9882589d487a171e0d01c38e  part2
```

### Datenträgerabschnitt 1

Zunächst betrachten wir erneut die erste Partition, den Master Boot Record (siehe \mbox{Anhang/MBR}).

Die ersten 446 Byte (`0x0` - `0x1be`) eines klassischen, generischen MBR-Sektors stellen das Startprogramm für den Computer dar.
In diesem Fall sind diese jedoch bis auf die letzten vier Bytes leer:
```
da 18 1c 9d
```

Die Interpretation dieser vier Bytes als ASCII-Code ergibt jedoch keinen Sinn.

Das fehlende Startprogramm in diesem MBR-Sektor deutet daraufhin, dass sich auf dem Datenträger kein Betriebssystem befand, sondern dass dieser als reiner Datenträger genutzt wurde.

Der erste Partitionseintrag (`0x1BE` - `Ox1CD`) im MBR-Sektor zeigt auf die zuvor gefundene valide Linux Partition.

```
00 3f 07 02 83 06 0a 1d 80 0d 00 00 80 92 00 00
```

Anhand des ersten Bytes des Partitionseintrags ist ersichtlich, dass diese Partition als inaktiv markiert ist (`Ox00`).

Laut dem vierten Byte des Eintrags handelt es sich um ein Linux Ext Dateisystem (`0x83`), welches im LBA (logical block addressing) Sektor 3456 beginnt (Byte `0x1c6` - `0x1c9`) und eine Größe von 37504 Sektoren hat (Byte `0x1a` - `0x1cd`).


### Datenträgerabschnitt 2

Die zweite Partition, die im Partitionsschema als nicht verwendet angegeben ist, erhält bei der Betratchung der Rohdaten (mittels eines Hexdump Werkzeugs) doch relevante Daten (siehe Anhang/Hexdump Part1).

Bereits aus den Metadaten der Datei(en) ist ersichtlich, dass dieser Teil der Festplatte möglicherweise fall-relevante Daten enthält.
Mithilfe des File-Carvers `photorec` konnte aus diesem Bereich der Festplatte das Bild `f0000001.jpg` extrahiert werden, wobei es sich um potentiell rhinographisches Material handelt (siehe Anhang/f0000001.jpg).

SHA-256-Prüfsumme der gefundenen Datei:
```
69fbe3497d2a9809bc20b262c8fc891a2d0cf08deea89830b86f52eafb5b793a  f0000001.jpg
```

```
$ file f0000001.jpg
f0000001.jpg: JPEG image data, JFIF standard 1.01, resolution (DPI),
density 72x72, segment length 16, Exif Standard: [TIFF image data,
little-endian, direntries=6, xresolution=86, yresolution=94, resolutionunit=2,
datetime=2008:04:22 13:49:48], baseline, precision 8, 308x390, frames 3
```

### Datenträgerabschnitt 3

Nun wird der letzte Bereich des Festplattenabbildes, bei dem es sich um eine valide Partition handelt, untersucht.
Entgegen dem Eintrag im Partitionsschema handelt es sich hierbei nicht um eine Ext2/3/4 Partition, sondern um eine NTFS-Parition, wie das Tool `fsstat` zeigt. Dies kann jedoch auch vorkommen, wenn das Programm mit dem die Partition angelegt wurde nicht korrekt programmiert wurde.

```
$ fsstat part2
FILE SYSTEM INFORMATION
--------------------------------------------
File System Type: NTFS
Volume Serial Number: 019288CE1A1B565C
OEM Name: NTFS
Version: Windows XP

METADATA INFORMATION
--------------------------------------------
First Cluster of MFT: 4
First Cluster of MFT Mirror: 2343
Size of MFT Entries: 1024 bytes
Size of Index Records: 4096 bytes
Range: 0 - 66
Root Directory: 5

CONTENT INFORMATION
--------------------------------------------
Sector Size: 512
Cluster Size: 4096
Total Cluster Range: 0 - 4686
Total Sector Range: 0 - 37502

$AttrDef Attribute Values:
$STANDARD_INFORMATION (16)   Size: 48-72   Flags: Resident
$ATTRIBUTE_LIST (32)   Size: No Limit   Flags: Non-resident
$FILE_NAME (48)   Size: 68-578   Flags: Resident,Index
$OBJECT_ID (64)   Size: 0-256   Flags: Resident
$SECURITY_DESCRIPTOR (80)   Size: No Limit   Flags: Non-resident
$VOLUME_NAME (96)   Size: 2-256   Flags: Resident
$VOLUME_INFORMATION (112)   Size: 12-12   Flags: Resident
$DATA (128)   Size: No Limit   Flags:
$INDEX_ROOT (144)   Size: No Limit   Flags: Resident
$INDEX_ALLOCATION (160)   Size: No Limit   Flags: Non-resident
$BITMAP (176)   Size: No Limit   Flags: Non-resident
$REPARSE_POINT (192)   Size: 0-16384   Flags: Non-resident
$EA_INFORMATION (208)   Size: 8-8   Flags: Resident
$EA (224)   Size: 0-65536   Flags:
$LOGGED_UTILITY_STREAM (256)   Size: 0-65536   Flags: Non-resident
```

Die Partition wurde mit Microsofts Windows XP erstellt.

```
$ fls -r part2
r/r 4-128-1:	$AttrDef
r/r 8-128-2:	$BadClus
r/r 8-128-1:	$BadClus:$Bad
r/r 6-128-1:	$Bitmap
r/r 7-128-1:	$Boot
d/d 11-144-2:	$Extend
+ r/r 25-144-2:	$ObjId:$O
+ r/r 24-144-3:	$Quota:$O
+ r/r 24-144-2:	$Quota:$Q
+ r/r 26-144-2:	$Reparse:$R
r/r 2-128-1:	$LogFile
r/r 0-128-1:	$MFT
r/r 1-128-1:	$MFTMirr
r/r 9-128-2:	$Secure:$SDS
r/r 9-144-3:	$Secure:$SDH
r/r 9-144-4:	$Secure:$SII
r/r 10-128-1:	$UpCase
r/r 10-128-2:	$UpCase:$Info
r/r 3-128-3:	$Volume
r/r 65-128-2:	nashorn.jpg
r/- * 0:	nasohnehorn.jpg
-/r * 64-128-2:	nasohnehorn.jpg
d/d 66:	$OrphanFiles
+ -/r * 16:	OrphanFile-16
+ -/r * 17:	OrphanFile-17
+ -/r * 18:	OrphanFile-18
+ -/r * 19:	OrphanFile-19
+ -/r * 20:	OrphanFile-20
+ -/r * 21:	OrphanFile-21
+ -/r * 22:	OrphanFile-22
+ -/r * 23:	OrphanFile-23
```

Die `fls` Ausgabe zeigt neben den Verwaltungsdateien des Dateisystem (erkenntlich an dem Präfix `$`) an, dass sich auf der Partition zwei Dateien befinden, deren Name auf rhinographische Inhalte hindeutet (`nashorn.jpg` und `nasohnehorn.jpg`), sowie acht Dateien, die gelöscht wurden (`OprhanFile-{16..23}`). Da bei allen gelöschten Daten jedoch das NTFS `$DATA` Attribut fehlt, lassen sich diese Dateien nicht mehr wiederherstellen.

Lässt man sich die Datei `nashorn.jpg` ausgeben, musst erneut festgestellt werden, dass es sich hierbei um potentiell rhinographisches Material handelt (siehe Anhang/nashorn.jpg).

SHA-256-Prüfsumme der gefundenen Datei:
```
e93de62354519bc7ae130ab0c0c2033c12e3bd3ff915f1aec28795e3e0a608f6  nashorn.jpg
```

Aus den EXIF-Metadaten des Bildes ist zu entnehmen, dass die Grafik in dem Bild mit der Software Adobe Photoshop CS3 unter Windows am 20. Februar 2009 um 14:17 erstellt wurde. Es ist jedoch mit speziellen Tools, die nach wenigen Minuten Internetrecherche aufzufinden sind, möglich diese Metadaten der Datei hinzuzufügen, zu entfernen sowie zu modifizieren. Da es sich bei diesem Bild jedoch tatsächlich um eine Grafik, und keine Fotografie, handelt, ist es wahrscheinlich, dass die Datei tatsächlich mit Adobe Photoshop CS3 erstellt wurde.

```
$ file nashorn.jpg
nashorn.jpg: JPEG image data, JFIF standard 1.01, resolution (DPI), density 72x72,
segment length 16, Exif Standard: [TIFF image data, big-endian, direntries=10,
description=Rhino Warning Sign With Glossy Effect, orientation=upper-left,
xresolution=172, yresolution=180, resolutionunit=2, software=Adobe Photoshop CS3
Windows, datetime=2009:02:20 14:17:44], baseline, precision 8, 346x346, frames 3
```

Auch bei der Datei `nasohnehorn.jpg` handelt es sich um potentiell rhinographisches Material (siehe Anhang/nasohnehorn.jpg), wobei bei dieser Datei der Verweis zwischen File Name Entry und der Datei fehlt (zu erkennen an dem `r/-` bzw. `-/r` in der fls-Ausgabe).
Dies deutet darauf hin, dass die Datei unter Windows gelöscht wurde.

SHA-256-Prüfsumme der gefundenen Datei:
```
afc573d8dfff285d9d3e039f2ee5cdcc8da2da2b25247b4d463928d1d40ac4cc  nasohnehorn.jpg
```

Laut EXIF-Metadaten wurde die Aufnahme am 9. März 2011 um 16:19 mit einer Kamera der Marke Canon gemacht (Modell EOS-1D Mark III), wobei es wiederum grundsätzlich möglich ist diese Einträge zu ändern.

```
nasohnehorn.jpg: JPEG image data, JFIF standard 1.01, resolution (DPI),
density 72x72, segment length 16, Exif Standard: [TIFF image data,
little-endian, direntries=10, manufacturer=Canon, model=Canon EOS-1D Mark III,
orientation=upper-left, xresolution=162, yresolution=170, resolutionunit=2,
datetime=2011:03:09 16:29:43, GPS-Data], baseline, precision 8, 283x424, frames 3
```

Somit wurden auf dem Datenträger insgesamt drei JPEG Bilddateien potentiell rhinographischer Natur sichergestellt, jedoch befinden sich möglicherweise noch mehr Inhalte auf dem Datenträger.
Sofern das Gericht weitere Beweismittel für den Prozess braucht, können die Ermittlungen forgesetzt werden.
Bei den Bildern `nashorn.jpg` und `nasohnehorn.jpg` besteht Grund zur Annahme, dass der Eigentümer des Datenträgers von ihrer Existenz wusste, da diese direkt im Hauptverzeichnis ("root-directory") der externen Festplatte liegen.
Wobei es grundsätzlich auch möglich ist, dass die Dateien von einer dritten Person auf den Datenträger kopiert wurde, ohne dass Herr S. in der zwischenzeit seinen Datenträger verwendet hat.
Dies wäre auch einem Laien möglich, da es sich bei der Partitionstyp um NTFS handelt, welche von jedem Windowssystem gelesen und beschrieben werden kann.

Die Datei, die auf in dem nicht partitionierten Teil des Datenträgers sichergestellt wurde, muss hingegen dort abgespeichert worden sein, bevor die Partition gelöscht wurde.
Doch das Löschen einer Partition unter einem Betriebssystem wie Microsoft Windows ist nach ein paar Minuten Internetrecherche wiederum jedem Laien möglich.
Da Herr Jürgen S. ausgesagt hat, dass er den Datenträger vor drei Jahren gebraucht im Internet erworben hat, besteht durchaus die Möglichkeit, dass sich die Datei bereits zu dem Zeitpunkt, als Herr S. den Datenträger erwarb, auf dem Datenträger befand. In den EXIF-Daten des Bildes liegt zwar ein Zeitstempel vor (`2008:04:22`), jedoch bezieht sich dieser nur auf das Aufnahme des Fotos und nicht auf den Zeitpunkt zu dem die Datei abgespeichert wurde. Somit lässt sich keine konkrete Aussage dazu tätigen, wann das Bild auf dem Datenträger abegspeichert wurde.

### Abschluss

Abschließend werden die Prüfsummen der einzelenen Partitionen sowie des gesamten Festplattenabbildes erneut überprüft.

```
1f23fcf72f931e14a2762b3014b97f51e5031c045129d044287457a996b0c4cc  exercise_1.img
037cb5a6a61002be8dda5cc041bcfb654735275c4e535ca22fd4c5b6436ccc1a  part0
a05c5509c0226581e78acafb72136a251e0260862c8f5cfb460b92df8491d183  part1
3b468ac788cf588145c42e3b4c26ae909f489b7e9882589d487a171e0d01c38e  part2
```

Diese stimmen nach wie vor mit den ursprünglichen Prüfsummen überein, es sind also während der Untersuchungen an den Daten keine versehentlichen oder absichtlichen Veränderungen aufgetreten.

\newpage

## Anhang

### MBR

```
$ hd -v part0
00000000  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000010  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000020  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000030  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000040  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000050  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000060  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000070  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000080  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000090  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000000a0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000000b0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000000c0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000000d0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000000e0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000000f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000100  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000110  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000120  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000130  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000140  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000150  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000160  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000170  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000180  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000190  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000001a0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000001b0  00 00 00 00 00 00 00 00  da 18 1c 9d 00 00 00 3f  |...............?|
000001c0  07 02 83 06 0a 1d 80 0d  00 00 80 92 00 00 00 00  |................|
000001d0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000001e0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000001f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa  |..............U.|
00000200
```

### Hexdump Part1

```
$ hd -v part1 | head -n 216
000001b0  00 00 00 00 00 00 00 00  da 18 1c 9d 00 00 00 3f  |...............?|
000001c0  07 02 83 06 0a 1d 80 0d  00 00 80 92 00 00 00 00  |................|
000001d0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000001e0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
000001f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa  |..............U.|
00000200  ff d8 ff e0 00 10 4a 46  49 46 00 01 01 01 00 48  |......JFIF.....H|
00000210  00 48 00 00 ff e1 00 d0  45 78 69 66 00 00 49 49  |.H......Exif..II|
00000220  2a 00 08 00 00 00 06 00  1a 01 05 00 01 00 00 00  |*...............|
00000230  56 00 00 00 1b 01 05 00  01 00 00 00 5e 00 00 00  |V...........^...|
00000240  28 01 03 00 01 00 00 00  02 00 00 00 32 01 02 00  |(...........2...|
00000250  14 00 00 00 66 00 00 00  13 02 03 00 01 00 00 00  |....f...........|
00000260  01 00 00 00 69 87 04 00  01 00 00 00 7a 00 00 00  |....i.......z...|
00000270  00 00 00 00 48 00 00 00  01 00 00 00 48 00 00 00  |....H.......H...|
00000280  01 00 00 00 32 30 30 38  3a 30 34 3a 32 32 20 31  |....2008:04:22 1|
00000290  33 3a 34 39 3a 34 38 00  06 00 00 90 07 00 04 00  |3:49:48.........|
000002a0  00 00 30 32 32 30 01 91  07 00 04 00 00 00 01 02  |..0220..........|
000002b0  03 00 00 a0 07 00 04 00  00 00 30 31 30 30 01 a0  |..........0100..|
000002c0  03 00 01 00 00 00 ff ff  00 00 02 a0 03 00 01 00  |................|
000002d0  00 00 e5 07 00 00 03 a0  03 00 01 00 00 00 04 0a  |................|
000002e0  00 00 00 00 00 00 ff ed  02 d2 50 68 6f 74 6f 73  |..........Photos|
000002f0  68 6f 70 20 33 2e 30 00  38 42 49 4d 04 04 00 00  |hop 3.0.8BIM....|
00000300  00 00 02 b5 1c 02 00 00  02 00 02 1c 02 05 00 14  |................|
00000310  57 68 69 74 65 20 52 68  69 6e 6f 20 49 73 6f 6c  |White Rhino Isol|
00000320  61 74 65 64 1c 02 19 00  05 72 68 69 6e 6f 1c 02  |ated.....rhino..|
00000330  19 00 05 77 68 69 74 65  1c 02 19 00 06 61 66 72  |...white.....afr|
00000340  69 63 61 1c 02 19 00 06  61 6e 69 6d 61 6c 1c 02  |ica.....animal..|
00000350  19 00 07 61 6e 69 6d 61  6c 73 1c 02 19 00 03 62  |...animals.....b|
00000360  69 67 1c 02 19 00 09 64  61 6e 67 65 72 6f 75 73  |ig.....dangerous|
00000370  1c 02 19 00 0a 65 6e 64  61 6e 67 65 72 65 64 1c  |.....endangered.|
00000380  02 19 00 0a 65 78 70 65  64 69 74 69 6f 6e 1c 02  |....expedition..|
00000390  19 00 09 68 65 72 62 69  76 6f 72 65 1c 02 19 00  |...herbivore....|
000003a0  04 68 6f 72 6e 1c 02 19  00 04 68 75 67 65 1c 02  |.horn.....huge..|
000003b0  19 00 05 6c 61 72 67 65  1c 02 19 00 06 6d 61 6d  |...large.....mam|
000003c0  6d 61 6c 1c 02 19 00 06  6e 61 74 75 72 65 1c 02  |mal.....nature..|
000003d0  19 00 0a 72 68 69 6e 6f  63 65 72 6f 73 1c 02 19  |...rhinoceros...|
000003e0  00 06 73 61 66 61 72 69  1c 02 19 00 06 74 72 61  |..safari.....tra|
000003f0  76 65 6c 1c 02 19 00 04  77 69 6c 64 1c 02 19 00  |vel.....wild....|
00000400  08 77 69 6c 64 6c 69 66  65 1c 02 19 00 04 65 61  |.wildlife.....ea|
00000410  72 73 1c 02 19 00 08 69  73 6f 6c 61 74 65 64 1c  |rs.....isolated.|
00000420  02 19 00 05 77 68 69 74  65 1c 02 19 00 05 72 68  |....white.....rh|
00000430  69 6e 6f 1c 02 19 00 05  77 68 69 74 65 1c 02 19  |ino.....white...|
00000440  00 06 61 66 72 69 63 61  1c 02 19 00 06 61 6e 69  |..africa.....ani|
00000450  6d 61 6c 1c 02 19 00 07  61 6e 69 6d 61 6c 73 1c  |mal.....animals.|
00000460  02 19 00 03 62 69 67 1c  02 19 00 09 64 61 6e 67  |....big.....dang|
00000470  65 72 6f 75 73 1c 02 19  00 0a 65 6e 64 61 6e 67  |erous.....endang|
00000480  65 72 65 64 1c 02 19 00  0a 65 78 70 65 64 69 74  |ered.....expedit|
00000490  69 6f 6e 1c 02 19 00 09  68 65 72 62 69 76 6f 72  |ion.....herbivor|
000004a0  65 1c 02 19 00 04 68 6f  72 6e 1c 02 19 00 04 68  |e.....horn.....h|
000004b0  75 67 65 1c 02 19 00 05  6c 61 72 67 65 1c 02 19  |uge.....large...|
000004c0  00 06 6d 61 6d 6d 61 6c  1c 02 19 00 06 6e 61 74  |..mammal.....nat|
000004d0  75 72 65 1c 02 19 00 0a  72 68 69 6e 6f 63 65 72  |ure.....rhinocer|
000004e0  6f 73 1c 02 19 00 06 73  61 66 61 72 69 1c 02 19  |os.....safari...|
000004f0  00 06 74 72 61 76 65 6c  1c 02 19 00 04 77 69 6c  |..travel.....wil|
00000500  64 1c 02 19 00 08 77 69  6c 64 6c 69 66 65 1c 02  |d.....wildlife..|
00000510  19 00 04 65 61 72 73 1c  02 19 00 08 69 73 6f 6c  |...ears.....isol|
00000520  61 74 65 64 1c 02 65 00  0c 53 6f 75 74 68 20 41  |ated..e..South A|
00000530  66 72 69 63 61 1c 02 6e  00 17 44 75 6e 63 61 6e  |frica..n..Duncan|
00000540  20 4e 6f 61 6b 65 73 20  2d 20 46 6f 74 6f 6c 69  | Noakes - Fotoli|
00000550  61 1c 02 73 00 07 37 32  38 35 37 34 36 1c 02 74  |a..s..7285746..t|
00000560  00 17 44 75 6e 63 61 6e  20 4e 6f 61 6b 65 73 20  |..Duncan Noakes |
00000570  2d 20 46 6f 74 6f 6c 69  61 1c 02 78 00 3b 57 68  |- Fotolia..x.;Wh|
00000580  69 74 65 20 52 68 69 6e  6f 20 77 69 74 68 20 6c  |ite Rhino with l|
00000590  61 72 67 65 20 68 6f 72  6e 73 20 69 73 6f 6c 61  |arge horns isola|
000005a0  74 65 64 20 6f 6e 20 61  20 77 68 69 74 65 20 62  |ted on a white b|
000005b0  61 63 6b 67 72 6f 75 6e  64 00 ff e1 12 f0 68 74  |ackground.....ht|
000005c0  74 70 3a 2f 2f 6e 73 2e  61 64 6f 62 65 2e 63 6f  |tp://ns.adobe.co|
000005d0  6d 2f 78 61 70 2f 31 2e  30 2f 00 3c 3f 78 70 61  |m/xap/1.0/.<?xpa|
000005e0  63 6b 65 74 20 62 65 67  69 6e 3d 27 ef bb bf 27  |cket begin='...'|
000005f0  20 69 64 3d 27 57 35 4d  30 4d 70 43 65 68 69 48  | id='W5M0MpCehiH|
00000600  7a 72 65 53 7a 4e 54 63  7a 6b 63 39 64 27 3f 3e  |zreSzNTczkc9d'?>|
00000610  0a 3c 78 3a 78 6d 70 6d  65 74 61 20 78 6d 6c 6e  |.<x:xmpmeta xmln|
00000620  73 3a 78 3d 27 61 64 6f  62 65 3a 6e 73 3a 6d 65  |s:x='adobe:ns:me|
00000630  74 61 2f 27 20 78 3a 78  6d 70 74 6b 3d 27 49 6d  |ta/' x:xmptk='Im|
00000640  61 67 65 3a 3a 45 78 69  66 54 6f 6f 6c 20 37 2e  |age::ExifTool 7.|
00000650  33 30 27 3e 0a 3c 72 64  66 3a 52 44 46 20 78 6d  |30'>.<rdf:RDF xm|
00000660  6c 6e 73 3a 72 64 66 3d  27 68 74 74 70 3a 2f 2f  |lns:rdf='http://|
00000670  77 77 77 2e 77 33 2e 6f  72 67 2f 31 39 39 39 2f  |www.w3.org/1999/|
00000680  30 32 2f 32 32 2d 72 64  66 2d 73 79 6e 74 61 78  |02/22-rdf-syntax|
00000690  2d 6e 73 23 27 3e 0a 0a  20 3c 72 64 66 3a 44 65  |-ns#'>.. <rdf:De|
000006a0  73 63 72 69 70 74 69 6f  6e 20 72 64 66 3a 61 62  |scription rdf:ab|
000006b0  6f 75 74 3d 27 27 0a 20  20 78 6d 6c 6e 73 3a 64  |out=''.  xmlns:d|
000006c0  63 3d 27 68 74 74 70 3a  2f 2f 70 75 72 6c 2e 6f  |c='http://purl.o|
000006d0  72 67 2f 64 63 2f 65 6c  65 6d 65 6e 74 73 2f 31  |rg/dc/elements/1|
000006e0  2e 31 2f 27 3e 0a 20 20  3c 64 63 3a 64 65 73 63  |.1/'>.  <dc:desc|
000006f0  72 69 70 74 69 6f 6e 3e  0a 20 20 20 3c 72 64 66  |ription>.   <rdf|
00000700  3a 41 6c 74 3e 0a 20 20  20 20 3c 72 64 66 3a 6c  |:Alt>.    <rdf:l|
00000710  69 20 78 6d 6c 3a 6c 61  6e 67 3d 27 78 2d 64 65  |i xml:lang='x-de|
00000720  66 61 75 6c 74 27 3e 57  68 69 74 65 20 52 68 69  |fault'>White Rhi|
00000730  6e 6f 20 77 69 74 68 20  6c 61 72 67 65 20 68 6f  |no with large ho|
00000740  72 6e 73 20 69 73 6f 6c  61 74 65 64 20 6f 6e 20  |rns isolated on |
00000750  61 20 77 68 69 74 65 20  62 61 63 6b 67 72 6f 75  |a white backgrou|
00000760  6e 64 3c 2f 72 64 66 3a  6c 69 3e 0a 20 20 20 3c  |nd</rdf:li>.   <|
00000770  2f 72 64 66 3a 41 6c 74  3e 0a 20 20 3c 2f 64 63  |/rdf:Alt>.  </dc|
00000780  3a 64 65 73 63 72 69 70  74 69 6f 6e 3e 0a 20 20  |:description>.  |
00000790  3c 64 63 3a 73 75 62 6a  65 63 74 3e 0a 20 20 20  |<dc:subject>.   |
000007a0  3c 72 64 66 3a 42 61 67  3e 0a 20 20 20 20 3c 72  |<rdf:Bag>.    <r|
000007b0  64 66 3a 6c 69 3e 72 68  69 6e 6f 3c 2f 72 64 66  |df:li>rhino</rdf|
000007c0  3a 6c 69 3e 0a 20 20 20  20 3c 72 64 66 3a 6c 69  |:li>.    <rdf:li|
000007d0  3e 77 68 69 74 65 3c 2f  72 64 66 3a 6c 69 3e 0a  |>white</rdf:li>.|
000007e0  20 20 20 20 3c 72 64 66  3a 6c 69 3e 61 66 72 69  |    <rdf:li>afri|
000007f0  63 61 3c 2f 72 64 66 3a  6c 69 3e 0a 20 20 20 20  |ca</rdf:li>.    |
00000800  3c 72 64 66 3a 6c 69 3e  61 6e 69 6d 61 6c 3c 2f  |<rdf:li>animal</|
00000810  72 64 66 3a 6c 69 3e 0a  20 20 20 20 3c 72 64 66  |rdf:li>.    <rdf|
00000820  3a 6c 69 3e 61 6e 69 6d  61 6c 73 3c 2f 72 64 66  |:li>animals</rdf|
00000830  3a 6c 69 3e 0a 20 20 20  20 3c 72 64 66 3a 6c 69  |:li>.    <rdf:li|
00000840  3e 62 69 67 3c 2f 72 64  66 3a 6c 69 3e 0a 20 20  |>big</rdf:li>.  |
00000850  20 20 3c 72 64 66 3a 6c  69 3e 64 61 6e 67 65 72  |  <rdf:li>danger|
00000860  6f 75 73 3c 2f 72 64 66  3a 6c 69 3e 0a 20 20 20  |ous</rdf:li>.   |
00000870  20 3c 72 64 66 3a 6c 69  3e 65 6e 64 61 6e 67 65  | <rdf:li>endange|
00000880  72 65 64 3c 2f 72 64 66  3a 6c 69 3e 0a 20 20 20  |red</rdf:li>.   |
00000890  20 3c 72 64 66 3a 6c 69  3e 65 78 70 65 64 69 74  | <rdf:li>expedit|
000008a0  69 6f 6e 3c 2f 72 64 66  3a 6c 69 3e 0a 20 20 20  |ion</rdf:li>.   |
000008b0  20 3c 72 64 66 3a 6c 69  3e 68 65 72 62 69 76 6f  | <rdf:li>herbivo|
000008c0  72 65 3c 2f 72 64 66 3a  6c 69 3e 0a 20 20 20 20  |re</rdf:li>.    |
000008d0  3c 72 64 66 3a 6c 69 3e  68 6f 72 6e 3c 2f 72 64  |<rdf:li>horn</rd|
000008e0  66 3a 6c 69 3e 0a 20 20  20 20 3c 72 64 66 3a 6c  |f:li>.    <rdf:l|
000008f0  69 3e 68 75 67 65 3c 2f  72 64 66 3a 6c 69 3e 0a  |i>huge</rdf:li>.|
00000900  20 20 20 20 3c 72 64 66  3a 6c 69 3e 6c 61 72 67  |    <rdf:li>larg|
00000910  65 3c 2f 72 64 66 3a 6c  69 3e 0a 20 20 20 20 3c  |e</rdf:li>.    <|
00000920  72 64 66 3a 6c 69 3e 6d  61 6d 6d 61 6c 3c 2f 72  |rdf:li>mammal</r|
00000930  64 66 3a 6c 69 3e 0a 20  20 20 20 3c 72 64 66 3a  |df:li>.    <rdf:|
00000940  6c 69 3e 6e 61 74 75 72  65 3c 2f 72 64 66 3a 6c  |li>nature</rdf:l|
00000950  69 3e 0a 20 20 20 20 3c  72 64 66 3a 6c 69 3e 72  |i>.    <rdf:li>r|
00000960  68 69 6e 6f 63 65 72 6f  73 3c 2f 72 64 66 3a 6c  |hinoceros</rdf:l|
00000970  69 3e 0a 20 20 20 20 3c  72 64 66 3a 6c 69 3e 73  |i>.    <rdf:li>s|
00000980  61 66 61 72 69 3c 2f 72  64 66 3a 6c 69 3e 0a 20  |afari</rdf:li>. |
00000990  20 20 20 3c 72 64 66 3a  6c 69 3e 74 72 61 76 65  |   <rdf:li>trave|
000009a0  6c 3c 2f 72 64 66 3a 6c  69 3e 0a 20 20 20 20 3c  |l</rdf:li>.    <|
000009b0  72 64 66 3a 6c 69 3e 77  69 6c 64 3c 2f 72 64 66  |rdf:li>wild</rdf|
000009c0  3a 6c 69 3e 0a 20 20 20  20 3c 72 64 66 3a 6c 69  |:li>.    <rdf:li|
000009d0  3e 77 69 6c 64 6c 69 66  65 3c 2f 72 64 66 3a 6c  |>wildlife</rdf:l|
000009e0  69 3e 0a 20 20 20 20 3c  72 64 66 3a 6c 69 3e 65  |i>.    <rdf:li>e|
000009f0  61 72 73 3c 2f 72 64 66  3a 6c 69 3e 0a 20 20 20  |ars</rdf:li>.   |
00000a00  20 3c 72 64 66 3a 6c 69  3e 69 73 6f 6c 61 74 65  | <rdf:li>isolate|
00000a10  64 3c 2f 72 64 66 3a 6c  69 3e 0a 20 20 20 20 3c  |d</rdf:li>.    <|
00000a20  72 64 66 3a 6c 69 3e 77  68 69 74 65 3c 2f 72 64  |rdf:li>white</rd|
00000a30  66 3a 6c 69 3e 0a 20 20  20 20 3c 72 64 66 3a 6c  |f:li>.    <rdf:l|
00000a40  69 3e 72 68 69 6e 6f 3c  2f 72 64 66 3a 6c 69 3e  |i>rhino</rdf:li>|
00000a50  0a 20 20 20 20 3c 72 64  66 3a 6c 69 3e 77 68 69  |.    <rdf:li>whi|
00000a60  74 65 3c 2f 72 64 66 3a  6c 69 3e 0a 20 20 20 20  |te</rdf:li>.    |
00000a70  3c 72 64 66 3a 6c 69 3e  61 66 72 69 63 61 3c 2f  |<rdf:li>africa</|
00000a80  72 64 66 3a 6c 69 3e 0a  20 20 20 20 3c 72 64 66  |rdf:li>.    <rdf|
00000a90  3a 6c 69 3e 61 6e 69 6d  61 6c 3c 2f 72 64 66 3a  |:li>animal</rdf:|
00000aa0  6c 69 3e 0a 20 20 20 20  3c 72 64 66 3a 6c 69 3e  |li>.    <rdf:li>|
00000ab0  61 6e 69 6d 61 6c 73 3c  2f 72 64 66 3a 6c 69 3e  |animals</rdf:li>|
00000ac0  0a 20 20 20 20 3c 72 64  66 3a 6c 69 3e 62 69 67  |.    <rdf:li>big|
00000ad0  3c 2f 72 64 66 3a 6c 69  3e 0a 20 20 20 20 3c 72  |</rdf:li>.    <r|
00000ae0  64 66 3a 6c 69 3e 64 61  6e 67 65 72 6f 75 73 3c  |df:li>dangerous<|
00000af0  2f 72 64 66 3a 6c 69 3e  0a 20 20 20 20 3c 72 64  |/rdf:li>.    <rd|
00000b00  66 3a 6c 69 3e 65 6e 64  61 6e 67 65 72 65 64 3c  |f:li>endangered<|
00000b10  2f 72 64 66 3a 6c 69 3e  0a 20 20 20 20 3c 72 64  |/rdf:li>.    <rd|
00000b20  66 3a 6c 69 3e 65 78 70  65 64 69 74 69 6f 6e 3c  |f:li>expedition<|
00000b30  2f 72 64 66 3a 6c 69 3e  0a 20 20 20 20 3c 72 64  |/rdf:li>.    <rd|
00000b40  66 3a 6c 69 3e 68 65 72  62 69 76 6f 72 65 3c 2f  |f:li>herbivore</|
00000b50  72 64 66 3a 6c 69 3e 0a  20 20 20 20 3c 72 64 66  |rdf:li>.    <rdf|
00000b60  3a 6c 69 3e 68 6f 72 6e  3c 2f 72 64 66 3a 6c 69  |:li>horn</rdf:li|
00000b70  3e 0a 20 20 20 20 3c 72  64 66 3a 6c 69 3e 68 75  |>.    <rdf:li>hu|
00000b80  67 65 3c 2f 72 64 66 3a  6c 69 3e 0a 20 20 20 20  |ge</rdf:li>.    |
00000b90  3c 72 64 66 3a 6c 69 3e  6c 61 72 67 65 3c 2f 72  |<rdf:li>large</r|
00000ba0  64 66 3a 6c 69 3e 0a 20  20 20 20 3c 72 64 66 3a  |df:li>.    <rdf:|
00000bb0  6c 69 3e 6d 61 6d 6d 61  6c 3c 2f 72 64 66 3a 6c  |li>mammal</rdf:l|
00000bc0  69 3e 0a 20 20 20 20 3c  72 64 66 3a 6c 69 3e 6e  |i>.    <rdf:li>n|
00000bd0  61 74 75 72 65 3c 2f 72  64 66 3a 6c 69 3e 0a 20  |ature</rdf:li>. |
00000be0  20 20 20 3c 72 64 66 3a  6c 69 3e 72 68 69 6e 6f  |   <rdf:li>rhino|
00000bf0  63 65 72 6f 73 3c 2f 72  64 66 3a 6c 69 3e 0a 20  |ceros</rdf:li>. |
00000c00  20 20 20 3c 72 64 66 3a  6c 69 3e 73 61 66 61 72  |   <rdf:li>safar|
00000c10  69 3c 2f 72 64 66 3a 6c  69 3e 0a 20 20 20 20 3c  |i</rdf:li>.    <|
00000c20  72 64 66 3a 6c 69 3e 74  72 61 76 65 6c 3c 2f 72  |rdf:li>travel</r|
00000c30  64 66 3a 6c 69 3e 0a 20  20 20 20 3c 72 64 66 3a  |df:li>.    <rdf:|
00000c40  6c 69 3e 77 69 6c 64 3c  2f 72 64 66 3a 6c 69 3e  |li>wild</rdf:li>|
00000c50  0a 20 20 20 20 3c 72 64  66 3a 6c 69 3e 77 69 6c  |.    <rdf:li>wil|
00000c60  64 6c 69 66 65 3c 2f 72  64 66 3a 6c 69 3e 0a 20  |dlife</rdf:li>. |
00000c70  20 20 20 3c 72 64 66 3a  6c 69 3e 65 61 72 73 3c  |   <rdf:li>ears<|
00000c80  2f 72 64 66 3a 6c 69 3e  0a 20 20 20 20 3c 72 64  |/rdf:li>.    <rd|
00000c90  66 3a 6c 69 3e 69 73 6f  6c 61 74 65 64 3c 2f 72  |f:li>isolated</r|
00000ca0  64 66 3a 6c 69 3e 0a 20  20 20 20 3c 72 64 66 3a  |df:li>.    <rdf:|
00000cb0  6c 69 3e 77 68 69 74 65  3c 2f 72 64 66 3a 6c 69  |li>white</rdf:li|
00000cc0  3e 0a 20 20 20 3c 2f 72  64 66 3a 42 61 67 3e 0a  |>.   </rdf:Bag>.|
00000cd0  20 20 3c 2f 64 63 3a 73  75 62 6a 65 63 74 3e 0a  |  </dc:subject>.|
00000ce0  20 20 3c 64 63 3a 74 69  74 6c 65 3e 0a 20 20 20  |  <dc:title>.   |
00000cf0  3c 72 64 66 3a 41 6c 74  3e 0a 20 20 20 20 3c 72  |<rdf:Alt>.    <r|
00000d00  64 66 3a 6c 69 20 78 6d  6c 3a 6c 61 6e 67 3d 27  |df:li xml:lang='|
00000d10  78 2d 64 65 66 61 75 6c  74 27 3e 57 68 69 74 65  |x-default'>White|
00000d20  20 52 68 69 6e 6f 20 49  73 6f 6c 61 74 65 64 3c  | Rhino Isolated<|
00000d30  2f 72 64 66 3a 6c 69 3e  0a 20 20 20 3c 2f 72 64  |/rdf:li>.   </rd|
00000d40  66 3a 41 6c 74 3e 0a 20  20 3c 2f 64 63 3a 74 69  |f:Alt>.  </dc:ti|
00000d50  74 6c 65 3e 0a 20 3c 2f  72 64 66 3a 44 65 73 63  |tle>. </rdf:Desc|
00000d60  72 69 70 74 69 6f 6e 3e  0a 0a 20 3c 72 64 66 3a  |ription>.. <rdf:|
00000d70  44 65 73 63 72 69 70 74  69 6f 6e 20 72 64 66 3a  |Description rdf:|
00000d80  61 62 6f 75 74 3d 27 27  0a 20 20 78 6d 6c 6e 73  |about=''.  xmlns|
00000d90  3a 74 69 66 66 3d 27 68  74 74 70 3a 2f 2f 6e 73  |:tiff='http://ns|
00000da0  2e 61 64 6f 62 65 2e 63  6f 6d 2f 74 69 66 66 2f  |.adobe.com/tiff/|
00000db0  31 2e 30 2f 27 3e 0a 20  20 3c 74 69 66 66 3a 42  |1.0/'>.  <tiff:B|
00000dc0  69 74 73 50 65 72 53 61  6d 70 6c 65 3e 0a 20 20  |itsPerSample>.  |
00000dd0  20 3c 72 64 66 3a 53 65  71 3e 0a 20 20 20 20 3c  | <rdf:Seq>.    <|
00000de0  72 64 66 3a 6c 69 3e 38  3c 2f 72 64 66 3a 6c 69  |rdf:li>8</rdf:li|
00000df0  3e 0a 20 20 20 3c 2f 72  64 66 3a 53 65 71 3e 0a  |>.   </rdf:Seq>.|
00000e00  20 20 3c 2f 74 69 66 66  3a 42 69 74 73 50 65 72  |  </tiff:BitsPer|
00000e10  53 61 6d 70 6c 65 3e 0a  20 20 3c 74 69 66 66 3a  |Sample>.  <tiff:|
00000e20  49 6d 61 67 65 4c 65 6e  67 74 68 3e 32 35 36 34  |ImageLength>2564|
00000e30  3c 2f 74 69 66 66 3a 49  6d 61 67 65 4c 65 6e 67  |</tiff:ImageLeng|
00000e40  74 68 3e 0a 20 20 3c 74  69 66 66 3a 49 6d 61 67  |th>.  <tiff:Imag|
00000e50  65 57 69 64 74 68 3e 32  30 32 31 3c 2f 74 69 66  |eWidth>2021</tif|
00000e60  66 3a 49 6d 61 67 65 57  69 64 74 68 3e 0a 20 20  |f:ImageWidth>.  |
00000e70  3c 74 69 66 66 3a 52 65  73 6f 6c 75 74 69 6f 6e  |<tiff:Resolution|
00000e80  55 6e 69 74 3e 32 3c 2f  74 69 66 66 3a 52 65 73  |Unit>2</tiff:Res|
00000e90  6f 6c 75 74 69 6f 6e 55  6e 69 74 3e 0a 20 20 3c  |olutionUnit>.  <|
00000ea0  74 69 66 66 3a 58 52 65  73 6f 6c 75 74 69 6f 6e  |tiff:XResolution|
00000eb0  3e 37 32 2f 31 3c 2f 74  69 66 66 3a 58 52 65 73  |>72/1</tiff:XRes|
00000ec0  6f 6c 75 74 69 6f 6e 3e  0a 20 20 3c 74 69 66 66  |olution>.  <tiff|
00000ed0  3a 59 52 65 73 6f 6c 75  74 69 6f 6e 3e 37 32 2f  |:YResolution>72/|
00000ee0  31 3c 2f 74 69 66 66 3a  59 52 65 73 6f 6c 75 74  |1</tiff:YResolut|
00000ef0  69 6f 6e 3e 0a 20 3c 2f  72 64 66 3a 44 65 73 63  |ion>. </rdf:Desc|
00000f00  72 69 70 74 69 6f 6e 3e  0a 3c 2f 72 64 66 3a 52  |ription>.</rdf:R|
00000f10  44 46 3e 0a 3c 2f 78 3a  78 6d 70 6d 65 74 61 3e  |DF>.</x:xmpmeta>|
00000f20  0a 20 20 20 20 20 20 20  20 20 20 20 20 20 20 20  |.               |
```

\newpage

### f0000001.jpg

![f0000001.jpg](./f0000001.jpg)

```
69fbe3497d2a9809bc20b262c8fc891a2d0cf08deea89830b86f52eafb5b793a  f0000001.jpg
```

\newpage

### nashorn.jpg

![nashorn.jpg](./nashorn.jpg)

```
e93de62354519bc7ae130ab0c0c2033c12e3bd3ff915f1aec28795e3e0a608f6  nashorn.jpg
```

\newpage

### nasohnehorn.jpg

![nasohnehorn.jpg](./nasohnehorn.jpg)

```
afc573d8dfff285d9d3e039f2ee5cdcc8da2da2b25247b4d463928d1d40ac4cc  nasohnehorn.jpg
```
