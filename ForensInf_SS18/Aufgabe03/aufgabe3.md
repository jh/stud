
---
author: "Jack Henschel"
title: "Forensischer Bericht"
header: "Forensischer Bericht"
date: 2018-07-17
lang: de-DE
papersize: a4
documentclass: article
fontsize: 12pt
toc: true
titlepage: true
toc-own-page: true
---

## Prolog

### Beweismittelkette

Die Asservate A und B wurden vom Gutachter am 12.06.2018 über eine verschlüsselte Verbindung aus dem Netz der Kriminalinspektion 5 (Cybercrime und digitale Spuren) heruntergeladen.

Hierbei handelt es sich um zwei Festplattenabbilder, welche bei der Hausdurchsuchung beschlagnahmt wurden.
Unmittelbar nach Erhalt der Asservate wurden die SHA-256-Checksummen der Dateien gebildet.

```
a01b1963c1c6b1636242b72ab3423e310186d554c079e288abd3ea5a9f22b55f  ex_A.dd
ea2fb3488e11f6426a27bcee054c2629c55334f565d0ed1a2346c0d32f2c3d80  ex_B.dd
```

Diese stimmen mit den von der Staatsanwaltschaft übermittelten Prüfsummen überein.

Der Originaldatenträger wird weiterhin in der Asservatenkammer des Polizeipräsidiums verwahrt. Am Ende der Untersuchungen des Gutachters wird die Checksumme erneut überprüft, um die Integrität des Abbilds zu gewährleisten.

### Arbeitsumgebung

Bei dem Rechner auf dem die Untersuchungen durchgeführt werden handelt es sich um ein speziell angefertigtes x86-System, bestehend aus einem Intel i5-2500K Prozessor, 8GB DDR3 Arbeitsspeicher, einer MSI Z77A-GD55 Hauptplatine sowie einer 120GB A-DATA SSD als nichtflüchtigen Datenspeicher.

Auf dem Rechner selbst läuft ein Debian Stretch GNU/Linux System (9.4), worauf die Tools "The Sleuth Kit" in Version 4.4.0, "ClamAV" in Version 0.99.4 und "TestDisk" in Version 7.0 installiert sind.
Des weiteren wird das Hexdump-Programm `hd` aus der bsdmainutils Sammlung 9.0.12, `sqlite3` Version 3.16.2, `grep` Version 2.27 sowie `file` Version 5.30 verwendet.

\newpage

### Auftrag

Im Zuge der Ermittlungen zu einem Angriff, Kompromittierung und Ausspähung von Daten auf einem Webserver der Universität Erlangen wurde die zwei Computer von John und Frank Doe beschlagnahmt, da ihre IP-Adresse als Ursprung des Angriff ermittelt wurde.

Eine Zuordnung der beiden Rechner durch die Brüder war bei der Beschlagnahmung jedoch nicht möglich.
Beide Brüder haben in einer Befragung ausgesagt am Tatdatum, dem 8.10.2015, zwischen 23:00 und 24:00 Uhr auf besagten Webserver zugegriffen zu haben.

Die Kriminalinspektion 5, Schwerpunkt Cybercrime und digitale Spuren, hat den Webserver bereits analysiert. Durch den Gutachter soll nun geklärt werden, ob die beiden Rechner den Brüdern jeweils eindeutig zugeordnet werden können und ob von einem der Rechner der Angriff durchgeführt wurde.

Die Staatsanwaltschaft erwartet den Untersuchungsbericht bis 3. Juli 2018.

\newpage

## Zusammenfassung

Das Asservat A ist wahrscheinlich John Doe zuzuordnen, da es auf System nur einen Benutzer namens `John` gibt.
Es wurden keine weiteren Hinweise auf den oder die Benutzer des Systems gefunden.

Bei dem installierten Webbrowser (Mozilla Firefox) wurden die Einstellung zur Speicherung von Cookies verändert.
Cookies sind kleine Textabschnitte, die eine Website auf dem Rechner des Nutzers abspeichern kann, um später wieder darauf zuzugreifen.
Des Weiteren wurde der User-Agent Header des Browsers abgeschaltet.
Ein Header ist ein einfaches Textfeld, welches der Browser automatisch mit jeder Anfrage an den Server mitsendet, damit der Server seine Anfrage auf den Client anpassen kann.
Der User-Agent Header beschreibt dabei insbesondere den verwendeten Browser, die Versionsnummer sowie das Betriebssystem.
Dies sind zwei eindeutige Spuren auf den Versuch der Verschleierung der Identität des Webbrowsers im Netz.
Zwar zeigt der Browserverlauf ausschießlich legitime Zugriffe auf den betreffenden Webserver, jedoch ist das Fehlen des Angriffs auf die Seite im Browserverlauf leicht durch die Verwendung des Privaten-Modus beziehungsweise Incognito-Modus zu erklären.
In diesem Modus legt der Webbrowser keinen Webseitenverlauf an und verwirft alle Cookies nach dem Schließen des Webbrowsers.
Diese Funktionalität ist vielen Nutzern bekannt.
Außerdem passen die Zeitdifferenzen zwischen den Webseitenaufrufen, die im Browserverlauf gespeichert sind, sehr genau zu denen, die der Webserver vor dem Angriff registriert hat.
Anhand dieser drei Indizien (IP-Adresse, User-Agent Header, Zeitdifferenzen) wurde der Angriff mit großer Wahrscheinlichkeit von diesem System (Asservat A) durchgeführt.

Das Asservat B ist ziemlich sicher Frank Doe zuzuordnen, da es auf dem System nur einen Nutzer mit dem Namen `frank` und vollem Namen `Frank Doe` gibt.
Außerdem trägt der Computer selbst den Namen `frankomputer`.
Auch auf diesem Asservat finden sich nur legitime Aufrufe des betreffenden Webservers im Browserverlauf, was erneut durch die Verwendung des Privaten-Modus zu erklären ist.
Die Einstellungen dieses Browsers wurden ebenso modifiziert wie die auf Asservat A (Änderung der Cookie-Präferenzen sowie Abschaltung des User-Agent Headers).
Auch hier wurde also versucht die Identität des Webbrowsers zu maskieren.
Außerdem gibt es beim Asservat B definitive Anzeichen, dass der Nutzer sich der Nutzer tatsächlich zur Tatzeit eingeloggt hat und aktiv war.
Zusätzlich finden sich auf dem Asservat B Spuren auf die Modifikation der Computeruhr und damit verbunden Inkonsistenz von Zeitstempel in diversen Logdateien.
Möglicherweise wurde dies im Nachhinein getan.
Die Zeitdifferenzen der im Browserverlauf gespeicherten Webseitenzugriffe passen jedoch eher zu der Zugriffssequenz, die auf dem Webserver gespeichert wurde, bei welcher kein Angriff registriert wurde.

\newpage

## Technischer Teil

### Asservat A

Beim Festplattenabbild `ex_A.dd` (Asservat A) handelt es sich um ein MS/DOS-Partitionsschema.
Darin befindet sich nur der Master-Boot-Record (MBR, Boot-Sektor) sowie eine FAT16-Partition von 1,5 GB Größe.

```
$ mmstat ex_A.dd
dos
```

```
$ mmls ex_A.dd
DOS Partition Table
Offset Sector: 0
Units are in 512-byte sectors

      Slot      Start        End          Length       Description
000:  Meta      0000000000   0000000000   0000000001   Primary Table (#0)
001:  -------   0000000000   0000000062   0000000063   Unallocated
002:  000:000   0000000063   0003140927   0003140865   DOS FAT16 (0x06)
003:  -------   0003140928   0003145727   0000004800   Unallocated
```

Mit folgendem Befehl wurden die einzelnen Festplattenabschnitte (mithilfe des `mmcat` Tools aus der Sleuthkit Sammlung) herausgetrennt.

```
$ for i in {0..3}; do
    mmcat ex_A.dd $i > A_part$i;
    sha256sum A_part$i;
done

278a17da13ecd9396c62d29edf9f1efa95b3b881da5da3d2a65ade78c39cbe4e  A_part0
9b7e118fa399ce03195995cd87bac6f030993c5e344ca4a808923da0d9f91310  A_part1
13d27272e3c7d3f7bb6bf72e7e7b7a6f333d8bdbc65655038969a6c8b5c493ac  A_part2
54f980b5b3be8ce80cb6490c527e38d681deade50f239f2cb7d23cf9d0108c37  A_part3
```

Die Datenträgerabschnitte `A_part0` (MBR), `A_part1` (unallozierter Bereich) und `A_part3` (unallozierter Bereich) enthalten keine relevanten Daten.

Der Abschnitt `A_part2` ist die einzige valide Partition auf dem Asservat.
Das Sleuthkit-Tool `fsstat` bestätigt den Eintrag aus dem Partitionsschema, dass es sich um ein FAT16 Dateisystem handelt (siehe [Anhang/fsstat A_part2](#fsstat-a_part2)).

Ein Blick auf die Dateien in der Partition offenbart, dass es sich um ein Windows-System handeln muss.
Die Untersuchung der Datei `C:\Windows\System32\eula.txt` (Inode 12430914) zeigt, dass das System ein Microsoft Windows XP mit Service Pack 3 (SP3) ist.

```
Microsoft Windows XP Professional und
Microsoft Windows XP Tablet PC Edition
Service Pack 3
```

Da Microsoft das Betriebssystem Windows XP mit Service Pack 3 nur bis Anfang 2014 mit Sicherheitsupdates unterstützt hat, handelt es sich hierbei um ein unsicheres Betriebssystem im Jahr 2015.
Aus diesem Grund wurde die Partition mit dem Open-Source Tool ClamAV auf Malware geprüft, hierbei wurden jedoch keine potentiell gefährlichen Dateien gefunden.

```
$ sudo clamscan -ri /mnt/A_part2

----------- SCAN SUMMARY -----------
Known viruses: 6547512
Engine version: 0.99.4
Scanned directories: 671
Scanned files: 7756
Infected files: 0
Data scanned: 1388.59 MB
Data read: 964.32 MB (ratio 1.44:1)
Time: 220.725 sec (3 m 40 s)
```

Auf dem System sind nur Standard-Windows-Programme sowie der Webbrowser Mozilla Firefox (Version 41.0.1, siehe `C:\Programme\Mozilla Firefox\application.ini`) installiert.
Außerdem ist nur ein Benutzer (mit dem Namen "John", siehe `C:\Dokumente und Einstellungen\`) vorhanden, der nicht standardmäßig von Windows angelegt wird.
Im gesamten System wurden keine weiteren Referenzen auf die beiden Beschuldigten gefunden (Suche nach den Strings "John Doe" und "Frank Doe" in diversen Kombinationen).

Im privaten Verzeichnis des Nutzers John wurde ein Firefox Profil (`Dokumente und Einstellungen/John/Anwendungsdaten/Mozilla/Firefox/Profiles/6fv2gz33.default/`) gefunden, welches folgenden Browserverlauf enthält (extrahiert aus `places.sqlite`).

```sql
SELECT moz_places.id,datetime(moz_historyvisits.visit_date/1000000,'unixepoch'), moz_places.url
FROM moz_places, moz_historyvisits
WHERE moz_places.id = moz_historyvisits.place_id;
```

ID | Datum           | URL
---|-----------------|-----
 9|2015-10-08 14:59:33|https://www.mozilla.org/de/firefox/41.0.1/firstrun/
10|2015-10-08 17:48:30|http://131.188.31.137/vuln/
11|2015-10-08 17:48:35|http://131.188.31.137/vuln/?&page=1
12|2015-10-08 17:48:45|http://131.188.31.137/vuln/?page=2
13|2015-10-08 17:48:50|http://131.188.31.137/vuln/?page=3
14|2015-10-08 17:49:01|http://131.188.31.137/vuln/?page=4
15|2015-10-08 17:49:12|http://131.188.31.137/vuln/?page=5

Daraus ist ersichtlich, dass der Benuzter die besagte Website tatsächlich aufgerufen hat.
Jedoch handelt es sich hierbei ausschließlich um legitime Aufrufe (ohne directory-traversal Angriff), die am 8.10.2015 gegen 17:50 Uhr geschahen.

Die Differenzen der Zeitstempel aus dem Browserverlauf können mit denen aus den Webserver-Logdateien, die von der Systemadministrator übermittelt hat, korreliert werden.

Seite                      | Zeitdifferenz Browserverlauf | Zeitdifferenz Webserver-Log
---------------------------|------------------------------|----------------------------
/vuln/                     | -                            | -
/vuln/?&page=1             | 5s                           | 5s
/vuln/?page=2              | 10s                          | 9s
/vuln/?page=3              | 5s                           | 5s
/vuln/?page=4              | 9s                           | 11s
/vuln/?page=5              | 11s                          | 10s
/vuln/?page=/etc/shadow%00 | 37s                          | -

Wie anhand der Tabelle zu erkennen ist, passen die Zugriffsmuster des Asservats A sehr genau zur der zweiten Zugriffssequenz, die auf dem Webserver protokolliert wurde.
Bei eben jener wurde auch der Angriff durchgeführt.
Die Abweichungen von nur einer Sekunden lassen sich durch Transportverzögerungen erklären, da es sich bei Internetverbindungen um Paketvermittelte (und nicht Leitungsvermittelte) Netze handelt, welche weniger hohe Garantien bezüglich der Dienstgüte geben.

Besonderes Augenmerk muss auf die `prefs.js` Konfigurationdatei des Browserprofils gelegt werden (siehe [Anhang/prefs.js A_part2](#pref.js-a_part2)).
In dieser Datei legt Firefox Einstellungen ("Präferenzen") ab, die vom Nutzer gesetzt wurden und vom Standardverhalten abweichen.
Fallrelevant ist in dieser Datei, dass der Konfigurationsschlüssel `general.useragent.override` auf `""` (leerer String) gesetzt ist.
Normalerweise übersendet jeder HTTP-Client einen User-Agent-Header, der die Anwendung identifiziert.
Aus den von der Staatsanwaltschaft bereitgestellten Konfigurations- und Protokolldateien des Webserver ist ersichtlich, dass der Angreifer eben diesen User-Agent-Header abgeschalten hat, da der entsprechende Eintrag in den Logdateien leer ist.

```
user_pref("general.useragent.override", "");
```

Somit ist es durchaus möglich, dass dieser Browser für den Angriff verwendet wurde.
Die Erstellung sowie Bearbeitung von Konfigurationsschlüsseln ist im Firefox Browser durch den Aufruf der Spezial-URL `about:config` möglich.
Das diese Spezial-URL tatsächlich benutzt wurde ist erkennbar an einer abgespeicherten Sitzung unter `sessionstore-backups\previous.js` (siehe [Anhang/previous.js A_part2](#previous.js-a_part2)).
Darin ist auch zu sehen, dass der Nutzer versucht hat das Standard-Cookie-Verhalten des Browser zu ändern (Suche nach `network.cookie` Schlüssel in `formdata.id.textbox`).

Die für den Angriff fehlenden Einträge im Browserverlauf lassen sich beispielsweise durch Verwendung des Incognito- oder Privaten-Modus des Webbrowser (`Ctrl-Shift-P` für Firefox) erklären.
In diesem Modus speichert der Browser keinen Websiteverlauf und verwirft alle Cookies.
Diese Argumentation wird zusätzlich durch das Fehlen eines Referrer-Headers im Webserver-Log unterstützt.
Auch erklärt diese, wieso vor der Angreifer die Website zunächst mit schnellen Klicks durchsurft (erkennbar an der kurzen Verweildauer auf den einzelnen Seiten), dann aber 37 Sekunden vergehen, bevor der Webserver den Angriff registriert.

Für den Internet Explorer, der andere auf dem System installierte Webbrowser, wurden Protokolldaten unter `C:\Dokumente und Einstellungen\John\Lokale Einstellungen\Verlauf\History.IE5` gefunden.
Diese zeigen dass mit dem Internet Explorer lediglich ein anderer Browser (Firefox) von Mozilla heruntergeladen wurde.

Datum      | URL
-----------|----------------------------------------------------------------------------
2015-10-08 | http://www.msn.com/de-de?ocid=iefvrt
2015-10-08 | http://www.bing.com/search?srch=106&FORM=AS6&q=firefox
2015-10-08 | https://www.mozilla.org/de/firefox/new
2015-10-08 | https://download-installer.cdn.mozilla.net/pub/ firefox/releases/41.0.1/win32/de/ Firefox%20Setup%20Stub%2041.0.1.exe

Abschließend lässt sich sagen, dass der Angriff auf den Webserver mit hoher Wahrscheinlichkeit von diesem Rechner durchgeführt wurde.
Es stimmen drei Indizien überein:

* die IP-Adresse des Rechners
* der fehlenden User-Agent-Header
* das charakteristische Zugriffsmuster vor dem Angriff

\newpage

### Asservat B

Beim Festplattenabbild `ex_B.dd` (Asservat B) handelt es sich ebenfalls um ein MS/DOS-Partitionsschema.
Diesmal befindet sich jedoch darauf jedoch (neben dem MBR) eine Linux Ext4 Partition von 6 GB Größe.

```
$ mmstat ex_B.dd
dos
```

```
$ mmls ex_B.dd
DOS Partition Table
Offset Sector: 0
Units are in 512-byte sectors

      Slot      Start        End          Length       Description
000:  Meta      0000000000   0000000000   0000000001   Primary Table (#0)
001:  -------   0000000000   0000002047   0000002048   Unallocated
002:  000:000   0000002048   0012580863   0012578816   Linux (0x83)
003:  -------   0012580864   0012582911   0000002048   Unallocated
```

Die einzelnen Festplattenabschnitte wurden extrahiert und mit einem kryptographischen Hash gesichert.

```
$ for i in {0..3}; do
    mmcat ex_B.dd $i > B_part$i;
    sha256sum B_part$i;
done

52d237d00caa17d29497b5635c7ede140f7c43be4622ea7fbaed0b3685dc4144  B_part0
4f2bd29a8aa16481f53973b5581fe9c369b202030d2a042b85b1b28db27f732b  B_part1
7578246b38730978eb81da8ff7aae1c54634089bf840ced411d4fe334795bf83  B_part2
30e14955ebf1352266dc2ff8067e68104607e750abb9d3b36582b8af909fcb58  B_part3
```

Bei der Überprüfung wurde festgestellt, dass die Datenträgerabschnitte `B_part0` (MBR), `B_part1` (unallozierter Bereich) und `B_part3` (unallozierter Bereich) keine fallrelevanten Daten enthalten.

Beim Betriebssystem auf der Ext4 Partition handelt es sich um ein Ubuntu Linux 14.04.1 LTS (`/etc/issue`).
Der Computer trägt intern den Namen `frankomputer` (siehe `/etc/hostname`), außerdem gibt es abgesehen von Systemnutzern (die automatisch durch das Betriebssystem angelegt werden) nur einen Nutzer `frank` mit Namen `Frank Doe` in der Datei `/etc/passwd`:

```
frank:x:1000:1000:Frank Doe,,,:/home/frank:/bin/bash
```

Abgesehen von dieser Information wurde auf dem System keine weitere Referenz auf die Beschuldigten gefunden.

Der einzige auf dem System installierte Browser ist Mozilla Firefox 41.0.1, wobei HTTP-Anfragen grundsätzlich auch mit anderen installierten Tools wie Python, Perl oder `wget` gemacht werden können.
Hierzu muss der Benutzer jedoch über Erfahrung mit Kommandozeilen-Werkzeugen verfügen, oder zumindest von dieser Möglichkeit Kenntnis haben und die Suchmaschine seines Vertrauens dazu befragen.
Es finden sich jedoch keine gespeicherten Shell-Kommandos in `/root/.bash_history` oder `/home/frank/.bash_history`.

Im Homeverzeichnis des Nutzers `frank` befindet sich ein Firefox Profil (`/home/frank/.mozilla/firefox/uhdp5pwj.default`) mit folgendem Browserverlauf (aus `places.sqlite`).

```sql
SELECT moz_places.id,datetime(moz_historyvisits.visit_date/1000000,'unixepoch'), moz_places.url
FROM moz_places, moz_historyvisits
WHERE moz_places.id = moz_historyvisits.place_id;
```

ID | Datum            | URL
---|------------------|-----
14|2015-10-08 21:42:11|http://131.188.31.137/vuln/
15|2015-10-08 21:42:20|http://131.188.31.137/vuln/?&page=1
16|2015-10-08 21:42:26|http://131.188.31.137/vuln/?page=2
17|2015-10-08 21:42:36|http://131.188.31.137/vuln/?page=3
18|2015-10-08 21:42:41|http://131.188.31.137/vuln/?page=4
19|2015-10-08 21:42:51|http://131.188.31.137/vuln/?page=5

Auch hier sind im Browserverlauf wieder nur legitime Zugriffe auf den Webserver gespeichert.
Hierbei können die Zeitdifferenzen der Zeitstempel aus dem Browserverlauf erneut mit denen vom Webserver korreliert werden.

Seite          | Zeitdifferenz Browserverlauf | Zeitdifferenz Webserver-Log
---------------|------------------------------|----------------------------
/vuln/         | -                            | -
/vuln/?&page=1 | 9s                           | 10s
/vuln/?page=2  | 6s                           | 6s
/vuln/?page=3  | 6s                           | 10s
/vuln/?page=4  | 5s                           | 5s
/vuln/?page=5  | 10s                          | 9s

Das Zugriffsverhalten auf diesem Asservat passt gut zur ersten Zugriffssequenzen, die auf dem Webserver registriert wurde.
Bei dieser wurde jedoch kein Angriff durchgeführt.

Genau wie bei Asservat A wurde der User-Agent-Header des Browsers durch den Nutzer abgeschalten, wie in der Datei `prefs.js` zu erkennen ist (siehe [Anhang/prefs.js B_part2](#pref.js-b_part2)).
Unter der Spezial-URL `about:config` können eigene Konfigurationsschlüssel gesetzt werden.
Der Nutzer hat diese URL auch verwendet, wie in einer abgespeicherten Sitzung vom 8.10.2015 zu erkennen ist (siehe [Anhang/sessionstore.bak B_part2](#sessionstore.bak-b_part2)), um das Standard-Cookie-Verhalten  des Browsers zu ändern.

```
user_pref("general.useragent.override", "");
```

Auch hier lassen sich die für den Angriff fehlenden Einträge wieder durch Verwendung des Privaten-Modus erklären.

Hinzu kommt, dass der Zeitgeist Dienst (ein Hintergrundprozess welcher diverse Nutzeraktionen protokolliert) am Tattag einige Browserstarts gespeichert hat (siehe [Anhang/Zeitgeist](#zeitgeist)).

Firefox Browser wurde am 8.10.2015 um 17:33 Uhr, 17:42 Uhr und 21:41 Uhr gestartet.
Zwischen 19:45 Uhr und 20:30 Uhr wurde etliche Male eine Kommandozeile gestartet.
Auffällig ist die interne Inkonsistenz des Protokolls, da der Eintrag mit der ID 16 zeitlich vor dem Eintrag mit ID 15 liegt.

Diese Inkonsistenz sowie das Aufrufen der Kommandozeile wird bestätigt durch die Einträge in der zentralen Authentifikationsdatei `/var/log/auth.log`.
Diese zeigt (siehe [Anhang/auth.log](#auth.log)), dass der Nutzer `frank` mit dem `sudo`-Befehl (welches dem Nutzer administrative Rechte gibt) mehrmals die aktuelle Uhrzeit des Systems ausgelesen (`hwclock`) und sogar geändert hat (`date --set`).

Zusätzlich enthält die `auth.log` auch noch folgenden Eintrag, der zeigt, dass sich der Benutzer `frank` zu einem späteren Zeitpunkt (gegen 23:40 Uhr) noch einmal eingeloggt hat.

```
Oct  8 23:40:41 frankomputer lightdm: pam_unix(lightdm-autologin:session): session opened for user frank by (uid=0)
Oct  8 23:40:41 frankomputer systemd-logind[499]: New session c1 of user frank.
```

Dazu passen die Zeitstempel der Dateien, in denen Firefox den Browserverlauf (`places.sqlite`), die Nutzerpräferenzen (`prefs.js`) und die aktuelle Sitzung (`sessionstore.js`) speichert (zuletzt geändert bzw. modifiziert am 08.10.2015 um 23:42 Uhr).

```
  File: places.sqlite
  Size: 10485760  	Blocks: 20480      IO Block: 4096   regular file
Device: 700h/1792d	Inode: 6243        Links: 1
Access: (0644/-rw-r--r--)  Uid: ( 1000/    frank)   Gid: ( 1000/    frank)
Access: 2018-06-16 19:21:24.071208127 +0200
Modify: 2015-10-08 23:42:51.203507327 +0200
Change: 2015-10-08 23:42:51.203507327 +0200
 Birth: -

  File: prefs.js
  Size: 5280      	Blocks: 16         IO Block: 4096   regular file
Device: 700h/1792d	Inode: 6230        Links: 1
Access: (0600/-rw-------)  Uid: ( 1000/    frank)   Gid: ( 1000/    frank)
Access: 2018-06-16 19:25:40.389718085 +0200
Modify: 2015-10-08 23:42:59.339507628 +0200
Change: 2015-10-08 23:42:59.347507629 +0200
 Birth: -

  File: sessionstore.js
  Size: 1513      	Blocks: 8          IO Block: 4096   regular file
Device: 700h/1792d	Inode: 6245        Links: 1
Access: (0600/-rw-------)  Uid: ( 1000/    frank)   Gid: ( 1000/    frank)
Access: 2018-06-16 19:25:40.333717534 +0200
Modify: 2015-10-08 23:42:59.091507619 +0200
Change: 2015-10-08 23:42:59.091507619 +0200
 Birth: -
```

Eine mögliche Hypothese lautet:
Nutzer `frank` hat sich um 23:40 eingeloggt.
Der User-Agent-Header war zu diesem Zeitpunkt schon deakiviert (siehe Webserverprotokolle).
Der Nutzer startet den Browser und öffnet den Privaten-Modus (kein Browserverlauf).
Der Webserver registriert um 23:40 den Angriff.
Der Nutzer schließt den Browser wieder, wobei die Dateien `places.sqlite`, `prefs.js` und `sessionstore.js` durch Firefox gespeichert werden (erkennbar an Zeitstempeln).

### Abschluss

Abschließend werden die Prüfsummen der einzelnenen Partitionen sowie der gesamten Festplattenabbilder erneut überprüft.

```
$ sha256sum *_part* ex_*.dd
278a17da13ecd9396c62d29edf9f1efa95b3b881da5da3d2a65ade78c39cbe4e  A_part0
9b7e118fa399ce03195995cd87bac6f030993c5e344ca4a808923da0d9f91310  A_part1
13d27272e3c7d3f7bb6bf72e7e7b7a6f333d8bdbc65655038969a6c8b5c493ac  A_part2
54f980b5b3be8ce80cb6490c527e38d681deade50f239f2cb7d23cf9d0108c37  A_part3
52d237d00caa17d29497b5635c7ede140f7c43be4622ea7fbaed0b3685dc4144  B_part0
4f2bd29a8aa16481f53973b5581fe9c369b202030d2a042b85b1b28db27f732b  B_part1
7578246b38730978eb81da8ff7aae1c54634089bf840ced411d4fe334795bf83  B_part2
30e14955ebf1352266dc2ff8067e68104607e750abb9d3b36582b8af909fcb58  B_part3
a01b1963c1c6b1636242b72ab3423e310186d554c079e288abd3ea5a9f22b55f  ex_A.dd
ea2fb3488e11f6426a27bcee054c2629c55334f565d0ed1a2346c0d32f2c3d80  ex_B.dd
```

Diese stimmen nach wie vor mit den ursprünglichen Prüfsummen überein, es sind also während der Untersuchungen an den Daten keine versehentlichen oder absichtlichen Veränderungen aufgetreten.

\newpage

## Anhang

### fsstat A_part2

```
$ fsstat A_part2
FILE SYSTEM INFORMATION
--------------------------------------------
File System Type: FAT16

OEM Name: MSDOS5.0
Volume ID: 0xd4c9fa66
Volume Label (Boot Sector): NO NAME
Volume Label (Root Directory):
File System Type Label: FAT16

Sectors before file system: 63

File System Layout (in sectors)
Total Range: 0 - 3140864
* Reserved: 0 - 7
** Boot Sector: 0
* FAT 0: 8 - 199
* FAT 1: 200 - 391
* Data Area: 392 - 3140864
** Root Directory: 392 - 423
** Cluster Area: 424 - 3140839
** Non-clustered: 3140840 - 3140864

METADATA INFORMATION
--------------------------------------------
Range: 2 - 50247574
Root Directory: 2

CONTENT INFORMATION
--------------------------------------------
Sector Size: 512
Cluster Size: 32768
Total Cluster Range: 2 - 49070
```

### prefs.js A_part2

`C:\Dokumente und Einstellungen\John\Anwendungsdaten\Mozilla\Firefox\Profiles\6fv2gz33.default\prefs.js`:

```
# Mozilla User Preferences

/* Do not edit this file.
 *
 * If you make changes to this file while the application is running,
 * the changes will be overwritten when the application exits.
 *
 * To make a manual change to preferences, you can visit the URL about:config
 */

user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 0);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 0);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.cache.disk.capacity", 143360);
user_pref("browser.cache.disk.filesystem_reported", 1);
user_pref("browser.cache.disk.smart_size.first_run", false);
user_pref("browser.cache.frecency_experiment", 1);
user_pref("browser.download.importedFromSqlite", true);
user_pref("browser.migration.version", 30);
user_pref("browser.newtabpage.enhanced", true);
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.places.smartBookmarksVersion", 7);
user_pref("browser.rights.3.shown", true);
user_pref("browser.search.countryCode", "DE");
user_pref("browser.search.region", "DE");
user_pref("browser.sessionstore.upgradeBackup.latestBuildID", "20150929144111");
user_pref("browser.shell.mostRecentDateSetAsDefault", "1444326491");
user_pref("browser.slowStartup.averageTime", 3402);
user_pref("browser.slowStartup.samples", 3);
user_pref("browser.startup.homepage_override.buildID", "20150929144111");
user_pref("browser.startup.homepage_override.mstone", "41.0.1");
user_pref("browser.toolbarbuttons.introduced.pocket-button", true);
user_pref("datareporting.healthreport.nextDataSubmissionTime", "1444402772043");
user_pref("datareporting.healthreport.service.firstRun", true);
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1444326182964");
user_pref("datareporting.policy.firstRunTime", "1444316372042");
user_pref("datareporting.sessions.current.activeTicks", 22);
user_pref("datareporting.sessions.current.clean", true);
user_pref("datareporting.sessions.current.firstPaint", 941);
user_pref("datareporting.sessions.current.main", 290);
user_pref("datareporting.sessions.current.sessionRestored", 1161);
user_pref("datareporting.sessions.current.startTime", "1444326490584");
user_pref("datareporting.sessions.current.totalTime", 109);
user_pref("datareporting.sessions.currentIndex", 2);
user_pref("datareporting.sessions.previous.0", "{\"s\":1444316366080,\"a\":9,\"t\":51,\"c\":true,\"m\":240,\"fp\":483,\"sr\":7013}");
user_pref("datareporting.sessions.previous.1", "{\"s\":1444326170600,\"a\":10,\"t\":50,\"c\":true,\"m\":321,\"fp\":2263,\"sr\":2363}");
user_pref("dom.apps.reset-permissions", true);
user_pref("dom.mozApps.used", true);
user_pref("experiments.activeExperiment", false);
user_pref("extensions.blocklist.pingCountVersion", 0);
user_pref("extensions.bootstrappedAddons", "{}");
user_pref("extensions.databaseSchema", 17);
user_pref("extensions.enabledAddons", "%7B972ce4c6-7e08-4474-a285-3208198ce6fd%7D:41.0.1");
user_pref("extensions.getAddons.databaseSchema", 5);
user_pref("extensions.lastAppVersion", "41.0.1");
user_pref("extensions.lastPlatformVersion", "41.0.1");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.shownSelectionUI", true);
user_pref("extensions.xpiState", "{\"app-global\":{\"{972ce4c6-7e08-4474-a285-3208198ce6fd}\":{\"d\":\"C:\\\\Programme\\\\Mozilla Firefox\\\\browser\\\\extensions\\\\{972ce4c6-7e08-4474-a285-3208198ce6fd}\",\"e\":true,\"v\":\"41.0.1\",\"st\":1444316362000,\"mt\":1443569298000}}}");
user_pref("gecko.buildID", "20150929144111");
user_pref("gecko.mstone", "41.0.1");
user_pref("general.useragent.override", "");
user_pref("gfx.driver-init.appVersion", "41.0.1");
user_pref("gfx.driver-init.deviceID", "0x0000");
user_pref("gfx.driver-init.feature-d2d", false);
user_pref("gfx.driver-init.feature-d3d11", false);
user_pref("gfx.driver-init.status", 2);
user_pref("media.gmp-gmpopenh264.lastUpdate", 1444326553);
user_pref("media.gmp-gmpopenh264.version", "1.4");
user_pref("media.gmp-manager.buildID", "20150929144111");
user_pref("media.gmp-manager.lastCheck", 1444326552);
user_pref("media.hardware-video-decoding.failed", true);
user_pref("network.cookie.cookieBehavior", 3);
user_pref("network.cookie.prefsMigrated", true);
user_pref("network.predictor.cleaned-up", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("pdfjs.previousHandler.alwaysAskBeforeHandling", true);
user_pref("pdfjs.previousHandler.preferredAction", 4);
user_pref("places.history.expiration.transient_current_max_pages", 4938);
user_pref("plugin.disable_full_page_plugin_for_types", "application/pdf");
user_pref("plugin.importedState", true);
user_pref("privacy.sanitize.migrateFx3Prefs", true);
user_pref("sanity-test.device-id", "0x0000");
user_pref("sanity-test.driver-version", "");
user_pref("sanity-test.running", false);
user_pref("sanity-test.version", "41.0.1");
user_pref("signon.importedFromSqlite", true);
user_pref("toolkit.startup.last_success", 1444326490);
user_pref("toolkit.telemetry.cachedClientID", "8900fd37-4553-4b67-8d80-6fdf9a1c3e58");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
```


### previous.js A_part2

`C:\Dokumente und Einstellungen\John\Anwendungsdaten\Mozilla\Firefox\Profiles\6fv2gz33.default\sessionstore-backups\previous.js` aus Asservat A:

```
{
  "windows": [
    {
      "tabs": [
        {
          "entries": [
            {
              "url": "about:home",
              "title": "Mozilla Firefox-Startseite",
              "ID": 2,
              "docshellID": 5,
              "docIdentifier": 2,
              "persist": true
            },
            {
              "url": "about:config",
              "ID": 4,
              "docshellID": 5,
              "owner_b64": "SmIS26zLEdO3ZQBgsLbOywAAAAAAAAAAwAAAAAAAAEY=",
              "docIdentifier": 4,
              "persist": true
            }
          ],
          "lastAccessed": 1444326221152,
          "hidden": false,
          "attributes": {},
          "index": 2,
          "formdata": {
            "id": {
              "textbox": "network.cookie."
            },
            "url": "about:config"
          },
          "image": null
        }
      ],
      "selected": 1,
      "_closedTabs": [],
      "width": "610",
      "height": "450",
      "screenX": "4",
      "screenY": "4",
      "sizemode": "maximized",
      "title": "about:config",
      "_shouldRestore": true,
      "closedAt": 1444326221153
    }
  ],
  "selectedWindow": 0,
  "_closedWindows": [],
  "session": {
    "lastUpdate": 1444326221215,
    "startTime": 1444326171273,
    "recentCrashes": 0
  },
  "global": {}
}
```

### fsstat B_part2

```
FILE SYSTEM INFORMATION
--------------------------------------------
File System Type: Ext4
Volume Name:
Volume ID: c0abc469196ea49fad46bf1029943999

Last Written at: 2015-10-08 19:00:08 (CEST)
Last Checked at: 2015-10-08 18:48:17 (CEST)

Last Mounted at: 2015-10-08 18:59:59 (CEST)
Unmounted properly
Last mounted on: /

Source OS: Linux
Dynamic Structure
Compat Features: Journal, Ext Attributes, Resize Inode, Dir Index
InCompat Features: Filetype, Extents, Flexible Block Groups,
Read Only Compat Features: Sparse Super, Large File, Huge File, Extra Inode Size

Journal ID: 00
Journal Inode: 8

METADATA INFORMATION
--------------------------------------------
Inode Range: 1 - 393217
Root Directory: 2
Free Inodes: 214896
Inode Size: 256

CONTENT INFORMATION
--------------------------------------------
Block Groups Per Flex Group: 16
Block Range: 0 - 1572351
Block Size: 4096
Free Blocks: 653642
```

### prefs.js B_part2

`/home/frank/.mozilla/firefox/uhdp5pwj.default/prefs.js`:

```
# Mozilla User Preferences

/* Do not edit this file.
 *
 * If you make changes to this file while the application is running,
 * the changes will be overwritten when the application exits.
 *
 * To make a manual change to preferences, you can visit the URL about:config
 */

user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 0);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.cache.disk.capacity", 358400);
user_pref("browser.cache.disk.smart_size.first_run", false);
user_pref("browser.cache.disk.smart_size.use_old_max", false);
user_pref("browser.cache.disk.smart_size_cached_value", 296960);
user_pref("browser.cache.frecency_experiment", 1);
user_pref("browser.download.importedFromSqlite", true);
user_pref("browser.migration.version", 22);
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.places.smartBookmarksVersion", 7);
user_pref("browser.sessionstore.upgradeBackup.latestBuildID", "20140715214335");
user_pref("browser.slowStartup.averageTime", 4879);
user_pref("browser.slowStartup.samples", 3);
user_pref("browser.startup.homepage_override.buildID", "20140715214335");
user_pref("browser.startup.homepage_override.mstone", "31.0");
user_pref("datareporting.healthreport.nextDataSubmissionTime", "1444411908254");
user_pref("datareporting.healthreport.service.firstRun", true);
user_pref("datareporting.policy.firstRunTime", "1444325508254");
user_pref("datareporting.sessions.current.activeTicks", 13);
user_pref("datareporting.sessions.current.clean", true);
user_pref("datareporting.sessions.current.firstPaint", 5827);
user_pref("datareporting.sessions.current.main", 1356);
user_pref("datareporting.sessions.current.sessionRestored", 5549);
user_pref("datareporting.sessions.current.startTime", "1444340511533");
user_pref("datareporting.sessions.current.totalTime", 67);
user_pref("datareporting.sessions.currentIndex", 2);
user_pref("datareporting.sessions.previous.0", "{\"s\":1444325505643,\"a\":10,\"t\":96,\"c\":true,\"m\":1185,\"fp\":7400,\"sr\":7127}");
user_pref("datareporting.sessions.previous.1", "{\"s\":1444326162827,\"a\":15,\"t\":75,\"c\":true,\"m\":63,\"fp\":1992,\"sr\":2219}");
user_pref("distribution.canonical.bookmarksProcessed", true);
user_pref("extensions.blocklist.pingCountVersion", 0);
user_pref("extensions.bootstrappedAddons", "{\"webapps-team@lists.launchpad.net\":{\"version\":\"3.0.2\",\"type\":\"extension\",\"descriptor\":\"/usr/
share/mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/webapps-team@lists.launchpad.net\"}}");
user_pref("extensions.databaseSchema", 16);
user_pref("extensions.enabledAddons", "ubufox%40ubuntu.com:2.9,%7B2e1445b0-2682-11e1-bfc2-0800200c9a66%7D:2013.09.20.beta,online-accounts%40lists.laun
chpad.net:0.5,%7B972ce4c6-7e08-4474-a285-3208198ce6fd%7D:31.0");
user_pref("extensions.installCache", "[{\"name\":\"app-system-local\",\"addons\":{\"online-accounts@lists.launchpad.net\":{\"descriptor\":\"/usr/lib/m
ozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/online-accounts@lists.launchpad.net\",\"mtime\":1406067053000,\"rdfTime\":1390969638000}}},{\
"name\":\"app-system-share\",\"addons\":{\"ubufox@ubuntu.com\":{\"descriptor\":\"/usr/share/mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/
ubufox@ubuntu.com\",\"mtime\":1406067052000,\"rdfTime\":1401829706000},\"webapps-team@lists.launchpad.net\":{\"descriptor\":\"/usr/share/mozilla/exten
sions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/webapps-team@lists.launchpad.net\",\"mtime\":1406067053000,\"rdfTime\":1397665356000},\"{2e1445b0-2682-11e1-bfc2-0800200c9a66}\":{\"descriptor\":\"/usr/share/mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/{2e1445b0-2682-11e1-bfc2-0800200c9a66}\",\"mtime\":1406067052000,\"rdfTime\":1379719396000}}},{\"name\":\"app-global\",\"addons\":{\"{972ce4c6-7e08-4474-a285-3208198ce6fd}\":{\"descriptor\":\"/usr/lib/firefox/browser/extensions/{972ce4c6-7e08-4474-a285-3208198ce6fd}\",\"mtime\":1406066844000,\"rdfTime\":1405462714000},\"langpack-en-ZA@firefox.mozilla.org\":{\"descriptor\":\"/usr/lib/firefox/browser/extensions/langpack-en-ZA@firefox.mozilla.org.xpi\",\"mtime\":1443730127000},\"langpack-en-GB@firefox.mozilla.org\":{\"descriptor\":\"/usr/lib/firefox/browser/extensions/langpack-en-GB@firefox.mozilla.org.xpi\",\"mtime\":1443730126000}}}]");
user_pref("extensions.lastAppVersion", "31.0");
user_pref("extensions.lastPlatformVersion", "31.0");
user_pref("extensions.pendingOperations", false);
user_pref("gecko.buildID", "20140715214335");
user_pref("gecko.mstone", "31.0");
user_pref("general.useragent.override", "");
user_pref("network.cookie.cookieBehavior", 3);
user_pref("network.cookie.prefsMigrated", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("pdfjs.previousHandler.alwaysAskBeforeHandling", true);
user_pref("pdfjs.previousHandler.preferredAction", 4);
user_pref("places.history.expiration.transient_current_max_pages", 29254);
user_pref("plugin.disable_full_page_plugin_for_types", "application/pdf");
user_pref("plugin.importedState", true);
user_pref("privacy.sanitize.migrateFx3Prefs", true);
user_pref("toolkit.startup.last_success", 1444340512);
user_pref("toolkit.telemetry.previousBuildID", "20140715214335");
```

### auth.log

Auszüge der `/var/log/auth.log` vom Asservat B:


```
Oct  8 19:33:35 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date
Oct  8 19:33:35 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:33:35 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:34:31 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/sbin/hwclock --show
Oct  8 19:34:31 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:34:32 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:34:34 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/sbin/hwclock --show
Oct  8 19:34:34 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:34:35 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:35:11 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date --set Do 08 Okt 2015 19:30:32 CEST
Oct  8 19:35:11 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:35:11 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:35:20 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date --set=Do 08 Okt 2015 19:30:32 CEST
Oct  8 19:35:20 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:35:20 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:35:37 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date -s Do 08 Okt 2015 19:30:32 CEST
Oct  8 19:35:37 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:35:37 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:35:49 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/sbin/hwclock --show
Oct  8 19:35:49 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:35:50 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:35:56 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date
Oct  8 19:35:56 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:35:56 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:36:02 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date -s Do 8. Okt 2015 19:30:32 CEST
Oct  8 19:36:02 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:36:02 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:36:11 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date -s Do 8. Okt 19:30:32 CEST 2015
Oct  8 19:36:11 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:36:11 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:36:25 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date
Oct  8 19:36:25 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:36:25 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:36:36 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date -s Do 8. Okt 19:36:25 CEST 2015
Oct  8 19:36:36 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:36:36 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:37:00 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date --set=Do 8. Okt 19:36:25 CEST 2015
Oct  8 19:37:00 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:37:00 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:37:09 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date --help
Oct  8 19:37:09 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:37:09 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:41:39 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date --set=Do 8. Okt 19:36:25 CEST 2015
Oct  8 19:41:39 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:41:39 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 19:41:44 frankomputer sudo:    frank : TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date
Oct  8 19:41:44 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 19:41:44 frankomputer sudo: pam_unix(sudo:session): session closed for user root
```

```
Oct  8 21:47:22 frankomputer sudo:    frank : TTY=pts/8 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date +%H%M --set=2130
Oct  8 21:47:22 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 21:30:00 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 21:30:10 frankomputer sudo:    frank : TTY=pts/8 ; PWD=/home/frank ; USER=root ; COMMAND=/sbin/hwclock -w
```

```
Oct  8 22:33:33 frankomputer sudo: pam_unix(sudo:session): session opened for user root by frank(uid=0)
Oct  8 21:30:00 frankomputer sudo: pam_unix(sudo:session): session closed for user root
Oct  8 21:30:03 frankomputer sudo:    frank : timestamp too far in the future: Oct  8 22:33:33 2015 ; TTY=pts/0 ; PWD=/home/frank ; USER=root ; COMMAND=/bin/date
```

### Zeitgeist

Aus `/home/frank/.local/share/zeitgeist/activity.sqlite` auf Asservat B:

```sql
SELECT id,datetime((timestamp/1000), "unixepoch"),subj_uri,subj_text,actor_uri FROM event_view;
```

ID | Datum           | Pfad                          | Anwendung     | Starter
---|-----------------|-------------------------------|---------------|-------------------------------
1|2015-10-08 17:30:03|application:// rhythmbox.desktop||application:// zeitgeist-datahub.desktop
2|2015-10-08 17:30:03|application:// gcalctool.desktop||application:// zeitgeist-datahub.desktop
3|2015-10-08 17:30:03|application:// gedit.desktop||application:// zeitgeist-datahub.desktop
4|2015-10-08 17:30:03|application:// totem.desktop||application:// zeitgeist-datahub.desktop
5|2015-10-08 17:30:03|application:// thunderbird.desktop||application:// zeitgeist-datahub.desktop
6|2015-10-08 17:30:03|application:// yelp.desktop||application:// zeitgeist-datahub.desktop
7|2015-10-08 17:31:45|application:// firefox.desktop|Firefox Web Browser|application:// compiz.desktop
8|2015-10-08 17:33:27|application:// gnome-terminal.desktop|Terminal|application:// compiz.desktop
9|2015-10-08 17:42:38|application:// gnome-terminal.desktop|Terminal|application:// compiz.desktop
10|2015-10-08 17:42:42|application:// firefox.desktop|Firefox Web Browser|application:// compiz.desktop
11|2015-10-08 19:46:30|application:// gnome-terminal.desktop|Terminal|application:// compiz.desktop
12|2015-10-08 19:30:22|application:// gnome-terminal.desktop|Terminal|application:// compiz.desktop
13|2015-10-08 20:31:50|application:// gnome-terminal.desktop|Terminal|application:// compiz.desktop
14|2015-10-08 20:32:37|application:// gnome-terminal.desktop|Terminal|application:// compiz.desktop
15|2015-10-08 20:32:53|application:// gnome-terminal.desktop|Terminal|application:// compiz.desktop
16|2015-10-08 19:30:43|application:// gnome-terminal.desktop|Terminal|application:// compiz.desktop
17|2015-10-08 21:41:51|application:// firefox.desktop|Firefox Web Browser|application:// compiz.desktop

### sessionstore.bak B_part2

`/home/frank/.mozilla/firefox/uhdp5pwj.default/sessionstore.bak` auf Asservat B:

```json
{
  "windows": [
    {
      "tabs": [
        {
          "entries": [
            {
              "url": "about:startpage",
              "title": "Ubuntu Start Page",
              "ID": 2,
              "docshellID": 5,
              "docIdentifier": 2
            },
            {
              "url": "about:config",
              "ID": 4,
              "docshellID": 5,
              "owner_b64": "SmIS26zLEdO3ZQBgsLbOywAAAAAAAAAAwAAAAAAAAEY=",
              "docIdentifier": 4
            }
          ],
          "lastAccessed": 1444326237976,
          "hidden": false,
          "attributes": {},
          "image": null,
          "index": 2,
          "formdata": {
            "id": {
              "textbox": "network.cookie"
            },
            "url": "about:config"
          }
        }
      ],
      "selected": 1,
      "_closedTabs": [],
      "width": "610",
      "height": "450",
      "screenX": "4",
      "screenY": "4",
      "sizemode": "maximized",
      "title": "about:config",
      "_shouldRestore": true,
      "closedAt": 1444326237979
    }
  ],
  "selectedWindow": 0,
  "_closedWindows": [],
  "session": {
    "lastUpdate": 1444326238083,
    "startTime": 1444326163701,
    "recentCrashes": 0
  },
  "scratchpads": [],
  "global": {}
}
```
