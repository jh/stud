#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "plist.h"

#define DELIM "\n\t "

/* #define SANE_SHELL */

/* array to temprorarily collect PIDs of dead processes */
static pid_t *zombies = NULL;
/* sie of above array */
static int num_zombies = 0;

/* maximum line length supported by system */
static long MAX_LINE_LENGTH;
static long LINE_BUFFER_SIZE;

/* callback function for walkList */
/* prints all alive processes */
static int alive( pid_t p, const char *s ) {
    int status = 0xFFFFFFFF;

    /* retrieve status of process */
    if( waitpid( p, &status, WNOHANG ) == -1 ) {
        perror("waitpid"); // ?
        return 0;
    }

    if( ! WIFEXITED(status) ) {
        /* process did not exit (is still alive) */
        printf( "[%d] %s\n", p, s );
    }

    /* continue walking list */
    return 0;
}

/* callback function for walkList */
/* collects and prints all dead processes */
static int dead( pid_t p, const char *s ) {
    int status = 0xFFFFFFFF;

    /* retrieve status of process */
    if( waitpid( p, &status, WNOHANG ) == -1 ) {
        // ? waitpid: No child processes
        perror("waitpid");
        return -1;
    }

    if( WIFEXITED(status) ) {
        /* process terminated */
        printf( "Exit status [%s] = %d\n", s, WEXITSTATUS(status) );

        /* append pid to the end of the zombie array */
        pid_t *buf = realloc( zombies, (++num_zombies) * sizeof(pid_t) );
        if( !buf ) {
            perror("realloc");
            return -1;
        }

        zombies = buf;
        zombies[ num_zombies-1 ] = p;

    }

    /* continue walking list */
    return 0;
}

/* collects all dead processes and */
/* prints their respective exit status and command line  */
static int check_background_jobs() {
    /* init and collect all dead processes */
    num_zombies = 0;
    walkList( &dead );

    /* allocate buffer for removeElement() */
    char *line = malloc( LINE_BUFFER_SIZE );
    if( !line ) {
        perror("malloc");
        return -1;
    }

    /* remove those processes from the list */
    for( int i = 0; i < num_zombies; i++ ) {
        if( removeElement( zombies[i], line, sizeof(char) * MAX_LINE_LENGTH) < 0 )
            fprintf( stderr, "removeElement: a pair with the pid %d does not exist\n", zombies[i] );
    }

    free(line);

    /* free ze zombies! */
    if(zombies) free(zombies);

    zombies = NULL;

    return 0;
}

/* runs a process in the foreground */
/* char **al has the same structure as char **argv */
static int run_foreground( char **al ) {
    int status;
    pid_t p = fork();

    if( p == -1 ) {
        /* fork error */
        perror("fork");
        return -1;
    } else if( p == 0 ) {
        /* child process */
        int r = execvp( al[0], al );
        if( r == -1 ) {
            perror("execvp");
            exit(EXIT_FAILURE);
        }

        /* leave the child */
        exit(r);
    } else {
        /* parent */
        /* wait until the child exits */
        do {
            if( waitpid( p, &status, 0 ) == -1 ) {
                perror("waitpid");
                return -1;
            }

        } while ( !WIFEXITED(status) );

        return WEXITSTATUS(status);
    }
}

/* runs a process in the background, i.e. doesn't wait for the child to exit */
/* char **al has the same structure as char **argv */
/* const char *command_line will be stored in the linked list plist */
static int run_background(char **al, const char *command_line) {
    pid_t p = fork();

    if( p == -1 ) {
        /* fork error */
        perror("fork");
        return -1;
    } else if( p == 0 ) {
        /* child process */
        int r = execvp( al[0] ,al );
        if( r == -1 ) {
            perror("execvp");
            exit(EXIT_FAILURE);
        }

        /* leave the child */
        exit(r);
    } else {
        /* parent */
        /* append pid to the linked list */
        int a = insertElement( p, command_line );
        if( a == -1 ) {
            fprintf( stderr, "insertElement: a pair with the pid %d already exists\n", p );
        }
        if( a == -2 ) {
            fprintf( stderr, "insertElement: insufficient memory to complete element insertion\n" );
            return -1;
        }
    }

    return 0;
}

/* reads the next line from stdin */
static char * read_next_line() {
    char * line = malloc( LINE_BUFFER_SIZE );
    if( ! line )
        return NULL;

    errno = 0;
    if( ! fgets( line, MAX_LINE_LENGTH-1, stdin ) && errno ) {
        perror("fgets");
        free(line);
        line = NULL;
    }

    return line;
}

/* split the input line into several tokens to form */
/* the argument list for exec */
/* @param line: split will destroy the string */
/* @param level: first outside call must be with level 0, otherwise undefined behaviour */
/* @param total: pointer where split stores the total amount of elements in the array */
static char ** split( char * line, unsigned int level, unsigned int * total ) {
    char ** tokens;

    /* strtok only requires the first call to contain a pointer, otherwise NULL */
    if( level != 0 ) {
        line = NULL;
    }

    /* read next token */
    char *tok = strtok( line, DELIM );

    if( tok ) {
        /* go one level deeper */
        tokens = split( line, level+1, total );
        /* insert pointer to token at the current level */
        tokens[level] = tok; // ?
    } else {
        /* no more tokens in line */
        /* allocate memory for all token pointer and set total amount of tokens */
        if( ! (tokens = calloc( sizeof(char *), level+2 ) ) )
            return NULL;

        *total = level+1;
    }

    tokens[level] = tok;
    return tokens;

}

static int print_prompt() {
    char *ps1 = (char *) malloc( LINE_BUFFER_SIZE );
    if( !ps1 ) {
        perror("malloc");
        return -1;
    }

    if( ! getcwd( ps1, LINE_BUFFER_SIZE ) ) {
        perror("getcwd");
    }

    /* print prompt */
    fprintf( stdout, "%s: ", ps1 );

    free(ps1);

    return 0;
}

int main() {

    /* aquire global system parameters */
    MAX_LINE_LENGTH = sysconf(_SC_LINE_MAX);
    if( LINE_BUFFER_SIZE == -1 ) {
        perror("sysconf");
        exit(EXIT_FAILURE);
    }
    LINE_BUFFER_SIZE = sizeof(char) * ( MAX_LINE_LENGTH + 2 );

    while(1) {
        /* print the prompt */
        if( print_prompt() == -1 )
            exit(EXIT_FAILURE);

        /* read the next input line */
        char *line = read_next_line();
        if( !line ) {
            break;
        }

        /* make backup of input line */
        size_t line_length = strlen(line);
        char *input = malloc( sizeof(char) * line_length );
        if( !input ) {
            perror("malloc");
            free(line);
            exit(EXIT_FAILURE);
        }
        /* last character '\n' is not required */
        input = strncpy( input, line, line_length-1 );
        input[ line_length-1 ] = '\0';

        /* create argument list al (argv) */
        unsigned int total = 0;
        char **al = split( line, 0 , &(total) );

        if( ! *al ) {
#ifndef SANE_SHELL
            printf("Syntax error: empty command line\n");
#endif
            free(line);
            free(al);
            continue;
        }

        /* check for shell built-in and/or if the process should run in background */
        if( ! strcmp(*al, "cd") ) {

            /* change directory */
            if( total < 3 ) {
                fprintf( stderr, "Error: too few arguments\n" );
            } else if ( total > 3 ) {
                fprintf( stderr, "Error: too many arguments\n" );
            } else {
                if( chdir( al[1] ) == -1 )
                    perror("chdir");
            }

        } else if ( ! strcmp( al[0], "jobs" ) ) {

            /* print background jobs */
            if( total != 2 ) {
                fprintf( stderr, "Syntax error\n" );
            } else {
                walkList( &alive );
            }

        } else if( al[total-2] && *(al[total-2]) == '&' ) {

            /* run in background */
            al[total-2] = NULL;
            run_background( al, input );

        } else {

            /* run in foreground */
            printf( "Exit status [%s] = %d\n", input, run_foreground( al ) );

        }

        free(line);
        free(input);
        free(al);

        /* print exited jobs */
        if( check_background_jobs() == -1 )
            exit(EXIT_FAILURE);

        /* start all over again */
    }

    /* print final new line */
    fprintf( stdout, "\n" );

    return(EXIT_SUCCESS);
}
