#include <stdio.h>
#include <stdlib.h>

#include "sbuf.h"
#include "sem.h"

struct SBUF {
    SEM ** s; /* Array of Semaphore Handles */
    int max; /* Maximum number of Semaphore Handles (sizeof Array) */
    int num; /* Number of Semaphores Handles currently stored */
};

static SEM *lock;

SBUF * sbufCreate(int maxNumberOfSems) {
    SBUF * r;
    SEM ** buf;
    if( maxNumberOfSems <= 0 ) return NULL;

    if( ! ( lock = semCreate(1) ) )
        return NULL;

    if( ! ( r = malloc( sizeof(struct SBUF) ) ) ) {
        semDestroy(lock);
        return NULL;
    }

    r->max = maxNumberOfSems;
    r->num = 0;

    if( ! ( buf = calloc( maxNumberOfSems, sizeof(SEM *) ) ) ) {
        free(r);
        return NULL;
    }

    r->s = buf;

    return r;
}

int sbufAddSem(SBUF * cl, SEM * sem) {
    if( !cl || !cl->s || cl->num >= cl->max )
        return -1;

    P(lock);
    cl->s[ cl->num ] = sem;
    cl->num++
    V(lock);

    return cl->num;
}

int sbufGetNumberOfSems(SBUF * cl) {
    return cl ? cl->num : -1;
}

SEM * sbufGetSem(SBUF * cl, int index) {
    return ( index > -1 && index < cl->max ) ? cl->s[index] : NULL;
}
