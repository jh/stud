#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "sbuf.h"
#include "sem.h"

#define forever while(1)

#define STDIN_CHARS 100

struct params {
    char *stream;
    SBUF *sbuf;
    int id;
};

/* global string for passing data between threads */
static char *DATA;

/* global semaphore to notify child-threads of new data */
static SEM *write;

/* function for writing thread */
void * t_write( void *parameters ) {
    /* semaphore for synchronisation */
    SEM *written;
    /* file handler for pipe */
    FILE *f;
    /* cast parameters */
    struct params *p = (struct params *) parameters;

    /* open file (hangs until something opens the stream) */
    if( ! ( f = fopen( p->stream, "w" ) ) ) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    /* create and register semaphore for notifications to parent-thread */
    if( ! ( written = semCreate(0) ) ) {
        fprintf( stderr, "Error creating semaphore\n" );
        if( fclose(f) ) perror("fclose");
        exit(EXIT_FAILURE);
    } else {
        if( ( p->id = sbufAddSem( p->sbuf, written ) ) == -1 )
            fprintf( stderr, "Error adding semaphore to sbuffer\n" ), exit(EXIT_FAILURE);
    }

    /* notification (thread is online) */
    fprintf( stderr, "[piper] File %s opened with id %d\n", p->stream, p->id );

    /* main loop */
    forever {
        /* wait for new data from main thread */
        P(write);

        /* new data available, write to file stream (pipe) */
        if(DATA) {
            fprintf( f, "%s", DATA );
            /* force writing of data */
            if( fflush( f ) ) perror("flush");
        } else {
            fprintf( stderr, "Error no new data available\n" );
        }

        /* notify main thread that we finished */
        V(written);
    }

    return NULL;
}

int main( int argc, char ** argv ) {
    /* array for storing thread identifiers */
    pthread_t tids[ argc-1 ];
    /* array of parameters to give newly created threads their parameters */
    struct params p[ argc-1 ];
    /* buffer to store string by fgets */
    char input_buffer[ STDIN_CHARS + 1 ];
    /* semaphore handler */
    SBUF *sbuffer;
    /* semaphores for synchronisation */
    SEM *written;

    /* requires at least one writing pipe */
    if( argc < 2 )
        fprintf( stderr, "Usage: %s <pipes...>\n", argv[0] ), exit(EXIT_FAILURE);

    /* create SBUF structure */
    if( ! (sbuffer = sbufCreate( argc ) ) )
        exit(EXIT_FAILURE);

    /* register global semaphore for writing */
    if( ! ( write = semCreate( 0 ) ) )
    	fprintf( stderr, "Error creating writing semaphore\n" ), exit(EXIT_FAILURE);
    else
    	if( sbufAddSem( sbuffer, write ) == -1 )
    	    fprintf( stderr, "Error adding writing semaphore to sbuffer\n" ), exit(EXIT_FAILURE);

    /* spawn threads for writing pipes */
    for( int i = 0; i < argc-1; i++ ) {
        /* initialize parameter structure for thread */
        p[i].stream = argv[ i+1 ];
        p[i].sbuf = sbuffer;
        p[i].id = -1;

        /* create thread */
        if( ( errno = pthread_create( &tids[i], NULL, t_write, (void *) &p[i]  ) ) )
            perror("pthread_create"), exit(EXIT_FAILURE);
    }

    /* read from stdin and write data to pipes with threads */
    while( fgets( input_buffer, STDIN_CHARS, stdin ) ) {
        fprintf( stderr, "[piper] There are currently %d clients\n",
                 sbufGetNumberOfSems(sbuffer) - 1 );

        /* make new data available to all threads */
        DATA = input_buffer;

        /* unlock (vree) all child-threads */
        for( int i = sbufGetNumberOfSems(sbuffer); i > 1; i-- )
            V(write);

        /* wait until all child-threads finished writing */
        for( int i = 0; i < argc-1; i++ )
            /* retrieve semaphore for this thread (also checks if thread is active) */
            if( ( p[i].id != -1 ) && ( written = sbufGetSem( sbuffer, p[i].id ) ) )
                /* pelegen */
                P(written);
    }

    /* an error occured, we did not reach end-of-file */
    if( ! feof(stdin) )
        fprintf( stderr, "Error on input stream\n" ), exit(EXIT_FAILURE);

    return EXIT_SUCCESS;
}
