#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/* Maximum Line Length as specified in Task */
#define LINELENGTH 100

/* (Module internal) size of the line buffer */
/* +1 char for the newline '\n' and +1 char for the eos '\0' */
const int BUFFERSIZE = LINELENGTH + 2;

/* Module internal function to wrap call to strcmp */
/* This is necessary because qsort() gives the comparator "pointers to pointers to chars", */
/* however strcmp() expects "pointers to chars" */
/* Therefore this function removes one level of pointers */
/* see man 3 qsort */
static int cmpstr(const void *string1, const void *string2) {
    return strcmp(* (char * const *) string1, * (char * const *) string2);
}

int main(int argc, char *argv[]) {
    char **words = NULL;
    char **tmp = NULL;
    char *buffer = NULL;
    int words_length = 0;

    /* allocate memory for line buffer */
    buffer = calloc(BUFFERSIZE, sizeof( char * ));
    if(buffer == NULL) {
        perror("calloc");
        exit(EXIT_FAILURE);
    }


    /* while there are lines to read, read BUFFERSIZE-amount of chars from stdin and store them in *buffer */
    while ( errno = 0, fgets(buffer, BUFFERSIZE, stdin ) != NULL ) {

        if(buffer[BUFFERSIZE-2] != '\0' && buffer[BUFFERSIZE-2] != '\n') {
            fprintf(stderr, "Error: too many chars in previous line\n");

            /* read until next line break and discard the chars */
            while ( fgetc(stdin) != (int) '\n' ) {}

            buffer[BUFFERSIZE-2] = '\0';
            continue;
        }

        /* only use the buffer if it not an empty line */
        if(buffer[0] != '\n') {

            /* increase size of 2d-field */
            tmp = ( char ** ) realloc( words, sizeof( char[++words_length][BUFFERSIZE]) );
            if(tmp == NULL) {
                perror("realloc");
                exit(EXIT_FAILURE);
            }
            words = tmp;

            /* store the read line as the last element of the field */
            words[words_length-1] = buffer;

            /* (re)allocate memory for line buffer */
            buffer = calloc( BUFFERSIZE, sizeof( char * ) );
            if(buffer == NULL) {
                perror("calloc");
                exit(EXIT_FAILURE);
            }

        }

    }

    /* treat any errors from reading stdin */
    if(errno && ! buffer) {
        perror("fgets");
        exit(EXIT_FAILURE);
    }

    /* remove any newline characters */
    for(int i = 0; i < words_length; i++) {
        int c = 0;
        while( words[i][c] ) {
            if( words[i][c] == '\n' )
                words[i][c] = '\0';
            c++;
        }
    }

    /* sort words field using qsort */
    qsort(words, words_length, sizeof( char * ), cmpstr);


    /* print all elements in the (now sorted) field */
    for(int i = 0; i < words_length; i++) {
        printf("%s\n", words[i]);
    }

    free(buffer);
    for(int i = 0; i < words_length; i++) { free( words[i] ); }
    free(words);

    exit(EXIT_SUCCESS);
}
