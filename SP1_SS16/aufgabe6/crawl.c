#include <dirent.h>
#include <errno.h>
#include <fnmatch.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "argumentParser.h"

#define help_text "Usage: %s path... [-maxdepth=n] [-name=pattern] [-type={d,f}] [-size=[+-]n]\n"
#define print_help(program) fprintf( stderr, help_text, program )

typedef enum { ALL, DIRECTORY, REGULAR } FILE_TYPE;
typedef enum { BIGGER, SMALLER, EXACT } SIZE_TYPE;

static struct ARGUMENTS {
    char *program;
    int num_paths;
    char **paths;
    char *pattern;
    FILE_TYPE file_type;
    SIZE_TYPE size_type;
    int size;
    int maxdepth;
} args;

static int parse_args(int argc, char **argv) {
    char *value_buf;

    if( initArgumentParser( argc, argv ) == -1 )
        return -1;

    /* retrieve program name */
    args.program = getCommand();

    /* retrieve paths */
    args.num_paths = 0;
    while( ( value_buf = getArgument(args.num_paths) ) ) {

        /* Remove trailing slash (if any) */
        size_t l = strlen(value_buf);
        if( value_buf[ l-1 ] == '/' )
            value_buf[ l-1 ] = '\0';

        args.num_paths++;
        char **realloc_buf = (char **) realloc( args.paths, sizeof(char *) * ( args.num_paths ) );
        if( ! realloc_buf )
            perror("realloc"), exit(EXIT_FAILURE);

        args.paths = realloc_buf;
        args.paths[ args.num_paths-1 ] = value_buf;
    }

    /* retrieve pattern */
    args.pattern = getValueForOption("name");

    /* retrieve type of entry (enum TYPE) */
    value_buf = getValueForOption("type");
    if( value_buf && value_buf[0] == 'd' )
        args.file_type = DIRECTORY;
    else if( value_buf && value_buf[0] == 'f' )
        args.file_type = REGULAR;
    else
        args.file_type = ALL;


    /* retrieve (relative) size of file */
    value_buf = getValueForOption("size");
    if( value_buf ) {
        if( value_buf[0] == '+' )
            args.size_type = BIGGER;
        else if( value_buf[0] == '-' )
            args.size_type = SMALLER;
        else
            args.size_type = EXACT;

        args.size = atoi(value_buf);
        if( args.size < 0 ) args.size *= -1;

    } else {
        args.size = -1;
        args.size_type = 0;
    }

    /* retrieve maximum recursion depth */
    value_buf = getValueForOption("maxdepth");
    if( ! value_buf ) {
        args.maxdepth = -2;
    } else if( ( args.maxdepth = atoi( value_buf ) ) < 0 ) {
        fprintf( stderr, "-maxdepth argument must be >= 0\n" );
        return -1;
    }

#ifdef DEBUG
    fprintf( stdout, "program  : %s\n", args.program );
    for( int i = 0; i < args.num_paths; i++ )
        fprintf( stdout, "path %d   : %s\n", i, args.paths[i] );
    fprintf( stdout, "pattern  : %s\n", args.pattern );
    fprintf( stdout, "type     : %d\n", args.file_type );
    fprintf( stdout, "size     : %d %d\n", args.size_type, args.size );
    fprintf( stdout, "maxdepth : %d\n", args.maxdepth );
#endif

    /* finish */
    return 0;
}

/* returns 0 when the file should not be printed */
static int check_pattern(const char *name) {
    if( ! name || ! args.pattern )
        return 1;

    return ( ! fnmatch( args.pattern, name, 0 ) );
}

/* returns 0 when the file should not be printed */
static int check_filetype(mode_t m) {
    if( ( args.file_type == ALL || args.file_type == DIRECTORY ) && S_ISDIR( m ) )
        return 1;

    if( (args.file_type == ALL || args.file_type == REGULAR) && S_ISREG( m ) )
        return 1;

    return 0;
}

/* returns 0 when file should not be printed */
static int check_filesize(off_t s) {
    if( args.size == -1 )
        return 1;

    if( args.size_type == BIGGER )
        return ( s > args.size );
    else if( args.size_type == SMALLER )
        return ( s < args.size );
    else
        /* EXACT match */
        return ( s == args.size );

    return 1;
}

static void recurse_dir(const char * path, int depth) {
    DIR *d;
    struct dirent *e;
    struct stat status;
    char *name;
    char *name_buf = NULL;

    if( depth == -1 )
        return;

    if( lstat( path, &status ) == -1 ) {
        perror("stat");
        return;
    }

    if( ! S_ISDIR( status.st_mode ) )
        return;

    name_buf = strrchr( path, (int) '/' );
    /* print directory if */
    if(
    	/* it matches the given pattern (if any) */
    	check_pattern( name_buf ? &name_buf[1] : path  )
    	/* it has the given file type (if any) */
    	&& check_filetype( status.st_mode )
    	/* it matches the given size */
    	&& check_filesize( status.st_size ) )
    	fprintf( stdout, "%s\n", path );

    /* concatenate basedir and directory name */
    int pl = strlen(path);
    if( ! ( name = malloc( pl + 2 ) ) ) {
        perror("malloc");
        return;
    }
    strncpy( name, path, pl );
    name[ pl ] = '/';
    name[ pl + 1 ] = '\0';
    path = name;
    pl++;

    /* open directory */
    if( ! ( d = opendir(path) ) ) {
        perror("opendir");
        return;
    }


    /* go through all items in the directory */
    /* readdir only sets errno when there was an error */
    /* therefore, we set errno to zero on each loop */
    /* and check afterwards if errno changed */
    while( ! ( errno = 0 ) && ( e = readdir(d) ) ) {
        /* concatenate directory name and file name */
        int nl = strlen(e->d_name);
        if ( ! ( name_buf = malloc( pl + nl + 1) ) ) {
            perror("malloc");
            return;
        }
        strncpy( name_buf, path, pl );
        strncpy( &(name_buf[pl]), e->d_name, nl );
        name_buf[ pl + nl ] = '\0';

        /* retrieve file status (metadata) */
        if( stat( name_buf, &status ) == -1 ) {
            perror("stat");
            free(name_buf);
            continue;
        }

        /* check if we are currently working with a directory or a regular file */
        if( S_ISREG( status.st_mode ) ) {
            /* print file if */
            if(
                /* it matches the given pattern (if any) */
                check_pattern( e->d_name )
                /* it has the given file type (if any) */
                && check_filetype( status.st_mode )
                /* it has the given file size (if any) */
                && check_filesize( status.st_size ) )

                fprintf( stdout, "%s\n", name_buf );


        } else if ( S_ISDIR( status.st_mode ) ) {
            /* recurse through directory if it is not a pseudo-dir */
            if( strcmp( e->d_name, ".." ) && strcmp( e->d_name, "." ) )
                recurse_dir( name_buf, depth-1 );

        }

        free(name_buf);
    }

    /* readdir returned NULL and set the errno */
    if( errno )
        perror("readdir");

    /* closedir */
    if( closedir(d) == -1 )
        perror("closedir");

    free(name);

    return;
}

int main(int argc, char ** argv) {
    if( argc < 2 )
        print_help( * argv ), exit(EXIT_FAILURE);

    if( parse_args( argc, argv ) == -1 )
        print_help( * argv ), exit(EXIT_FAILURE);

    /* start depth first search over all given directories */
    for( int i = 0; i < args.num_paths; i++ ) {
        recurse_dir( args.paths[i], args.maxdepth );
    }

    free(args.paths);

    exit(EXIT_SUCCESS);
}
