#include <errno.h>
#include <string.h>

#include "argumentParser.h"

/* delimiter for key and value */
#define DELIM '='

/* global variable for storing the total of arguments */
static int g_argc;
/* global array of strings for storing the arguments */
static char **g_argv;
static char **arguments;
static int num_arguments;
static char **options;
static int num_options;

int initArgumentParser(int argc, char ** argv) {
    if( argc < 1 || ! argv ) {
        errno = EINVAL;
        return -1;
    }

    int i = 1;
    arguments = &argv[1];
    while( i < argc && argv[i][0] != '-' ) {
        num_arguments++;
        i++;
    }
    options = &argv[i];
    while( i < argc && argv[i][0] == '-' ) {
        num_options++;
        i++;
    }
    if( argc != i ) {
        errno = EINVAL;
        return -1;
    }

    g_argc = argc;
    g_argv = argv;

    return 0;
}

char * getCommand(void) {
    return g_argv[0];
}

char * getValueForOption(char * keyName) {
    size_t kl;

    if( !keyName )
        return NULL;

    /* extract key length */
    kl = strlen(keyName);

    if( !num_options )
        return NULL;

    /* search for argument containing key */
    for( int i = 0; i < num_options; i++ ) {
        if( ! strncmp( options[i] + 1, keyName, kl ) )
            /* extract and return value */
            return ( strchr( options[i], (int) DELIM ) + 1 );
    }

    return NULL;

}

int getNumberOfArguments(void) {
    return num_options;
}

char * getArgument(int index) {
    if( ! g_argv || index < 0 || index >= num_arguments )
        return NULL;

    return arguments[index];
}
