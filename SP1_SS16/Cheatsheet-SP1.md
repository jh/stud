# SP1 Cheatsheet

## Speicherverwaltung

```
#include <stdlib.h>
void *malloc(size_t size);
void free(void *ptr);
void *calloc(size_t nmemb, size_t size);
void *realloc(void *ptr, size_t size);
```


## Dateien öffnen


```
#include <stdio.h>
FILE *fopen (const char* path, const char* mode)
FILE *fdopen(int fd, const char *mode);
[mode: r, w, a]
int fclose(FILE *stream);
```

## Datei-Infos

```
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int stat(const char *pathname, struct stat *buf); // follows symlinks
int lstat(const char *pathname, struct stat *buf);
```

```
struct stat {
 dev_t     st_dev;         /* ID of device containing file */
 ino_t     st_ino;         /* inode number */
 mode_t    st_mode;        /* file type and mode */
 nlink_t   st_nlink;       /* number of hard links */
 uid_t     st_uid;         /* user ID of owner */
 gid_t     st_gid;         /* group ID of owner */
 dev_t     st_rdev;        /* device ID (if special file) */
 off_t     st_size;        /* total size, in bytes */
 blksize_t st_blksize;     /* blocksize for filesystem I/O */
 blkcnt_t  st_blocks;      /* number of 512B blocks allocated */
 struct timespec st_atim;  /* time of last access */
 struct timespec st_mtim;  /* time of last modification */
 struct timespec st_ctim;  /* time of last status change */
};
```

## Verzeichnis durchlaufen
```
DIR *opendir(const char *dirname);
struct dirent *readdir(DIR *dirp); // iterator
int closedir(DIR *dirp);
```

```
struct dirent {
ino_t d_ino; /* inode number */
off_t d_off; /* offset to the next dirent */
unsigned short d_reclen; /* length of this record */
unsigned char d_type; /* type of file; not supported
by all file system types */
char d_name[256]; /* filename */
};
```

## Prozesse

```
#include <unistd.h>
pid_t fork();
int execlp(const char* file, const char* arg0, ... , NULL);
int execvp(const char* file, const char* argv[] );
/* name of executable must be in 'file' AND 'arg0' */

#include <sys/wait.h>
pid_t wait(int *stat_loc);
pid_t waitpid(pid_t pid , int *stat_loc, int options);
[options: WCONTINUED, WNOHANG, WUNTRACED]

```

```
pid_t p = fork();
if ( p == -1 ) {
 perror("fork"); // fork failed
} else if ( p == 0 ) {
 // child
 // work
 exit(EXIT_STATUS);
} else {
 // parent, p is PID of child
 // wait until child exits:
do {
 if ( waitpid( p, &status, 0 ) == -1 ) { perror("waitpid"); return -1; }
} while ( !WIFEXITED(status) );
WEXITSTATUS(status);
}
```

## Threads

```
#include <pthread.h>
int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
void *(*start_routine)(void *), void *arg);
// save return value in errno and call perror
pthread_t pthread_self(); // own thread-id
void pthread_exit(void *retval); // leave thread
void pthread_join(pthread_t thread, void **retvalp); // waits until thread finished
int pthread_detach(pthread_t thread); // do not wait for thread to finish
```

## Semaphore

```
#include "sem.h"
SEM* semCreate (int initVal);
void P (SEM *sem); // decrement semaphpre
void V (SEM *sem); // increment semaphore
void semDestroy (SEM *sem);
```
