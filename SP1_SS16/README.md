## SP1 SS16

Lösungen für Systemprogrammierung 1 aus dem Sommersemester 2016.

## Aufgaben:
https://www4.cs.fau.de/Lehre/SS16/V_SP1/Uebung/aufgaben.shtml

* [aufgabe1 - lilo](aufgabe1/)
* [aufgabe2 - wsort](aufgabe2/)
* [aufgabe3 - halde](aufgabe3/)
* [aufgabe4 - clash](aufgabe4/)
* [aufgabe5 - piper](aufgabe5/)
* [aufgabe6 - crawl](aufgabe6/)
