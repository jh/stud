#include "halde.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdint.h>

/// Magic value for occupied memory chunks.
#define MAGIC ((void*)0xbaadf00d)

/// Size of the heap (in bytes).
#define SIZE (1024*1024*1)

/// Memory-chunk structure.
struct mblock {
    struct mblock *next;
    size_t size;
    char memory[];
};

/// Heap-memory area.
static char memory[SIZE];

/// Pointer to the first element of the free-memory list.
static struct mblock *head;

/// glob var
static int init = 0; // false

/// Helper function to visualise the current state of the free-memory list.
void printList(void) {
    struct mblock* lauf = head;

    // Empty list
    if ( head == NULL ) {
        fprintf(stderr, "(empty)\n");
        return;
    }

    // Print each element in the list
    while ( lauf ) {
        fprintf(stderr, "(addr: 0x%08zx, off: %7zu, ", (uintptr_t) lauf, (uintptr_t)lauf - (uintptr_t)memory);
        fflush(stderr);
        fprintf(stderr, "size: %7zu)", lauf->size);
        fflush(stderr);

        if ( lauf->next != NULL ) {
            fprintf(stderr, "  -->  ");
            fflush(stderr);
        }
        lauf = lauf->next;
    }
    fprintf(stderr, "\n");
    fflush(stderr);
}

void *malloc (size_t size) {
    if( size == 0 )
        return NULL;

    if( sizeof(memory) - sizeof(struct mblock) < size ) {
        errno = ENOMEM;
        return NULL;
    }


    if( !init && !head ) {
        /* list has not been initialized */
        /* set head pointer and assign appropriate values */
        head = (struct mblock*) memory;
        head->size = sizeof(memory) - sizeof(struct mblock);
        head->next = NULL;
        init = 1;
    }

    /* walk through the linked list until */
    /* either a big enough free space is found (first-fit) */
    /* or until we reach the end (NULL) */
    struct mblock *walker = head;
    struct mblock *follower = NULL;

    while( walker && walker->size < size ) {
        follower = walker;
        walker = walker->next;
    }

    if( !walker ) {
        /* we reached the end of the list */
        /* without finding a big enough free element */
        errno = ENOMEM;
        return NULL;
    }

    /* calculate free space after insertion */
    int free = walker->size - sizeof(struct mblock) - size;


    if ( free > 0 ) {
        /* enough free space for a new mblock left */
        walker->size = size;

        /* push sizeof(struct mblock) + size further */
        struct mblock *new  = (struct mblock *) &( walker->memory[size] );
        new->size = free;
        new->next = walker->next;
        walker->next = new;
    }

    /* update drag pointer (if exists) */
    if( follower ) {
        follower->next = walker->next;
    }

    /* update head (if we are removing the head) */
    if( head == walker ) {
        head = walker->next;
    }

    /* mark this element as used (set magic number)*/
    walker->next = MAGIC;

    /* return pointer to newly allocated memory */
    return walker->memory;
}

void free (void *ptr) {
    /* not a valid pointer */
    if( !ptr )
        return;

    /* calculate pointer to struct */
    struct mblock *mbp = ( (struct mblock*) ptr ) - 1;

    /* check if we found a valid (used) element */
    if( mbp->next == MAGIC ) {
        mbp->next = head;
        head = mbp;
    } else {
        abort();
    }

    return;
}

void *realloc (void *ptr, size_t size) {
    void *n = NULL;
    if( !ptr || size ) {
        n = malloc(size);
        if( !n )
            return NULL;

        if( ptr ) {
            /* derefence pointer to the original struct mblock */
            struct mblock *old = ( (struct mblock*) ptr ) - 1;
            /* check if the old size or the new size is bigger */
            /* this is important because we may not copy too much data with memcpy */
            unsigned long s = size > old->size ? old->size : size;
            n = memcpy(n, ptr, s);
            if( !n )
                return NULL;
        }

    }

    free( ptr );

    return n;
}

void *calloc (size_t nmemb, size_t size) {
    /* calculate total size */
    size_t total = nmemb * size;

    /* at this point one would have to check if an overflow occured in size_t total */
    /* but that is outside the scope of this assignement */

    /* nothing to do, return */
    if( !total )
        return NULL;

    void *ptr = malloc(total);

    /* malloc failed :-( */
    if( !ptr )
        return NULL;

    /* initialize the memory with 0s and return */
    return memset(ptr, 0x0, total);
}
