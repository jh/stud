#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "halde.h"

int main(int argc, char *argv[]) {
	printList();

	char* t1 = malloc(200*1024);
	printList();

	free(t1);
	printList();

	int* e1 = malloc(sizeof(int*)*1024); printList();
	char* e2 = malloc(sizeof(char*)*512); printList();
	long* e3 = malloc(sizeof(long*)*128); printList();
	/* edge case (malloc with size 0) */
	int* e4 = malloc(0);  printList();

	/* if the previous edge case worked, we are now trying to free with NULL */
	free(e4); printList();
	free(e2); printList();
	free(e1); printList();
	free(e3); printList();

	int* m1 = malloc(sizeof(int*)*1024); printList();
	char* m2 = malloc(sizeof(char*)*1024); printList();
	/* edge case (too large) */
	long* m3 = malloc(sizeof(long*)*1024*1024*2);  printList();
	int* m4 = malloc(sizeof(int*)); printList();
	
	free(m1); printList();
	free(m4); printList();
	free(m2); printList();
	/* again: if the previous edge case worked, we are now trying to free with NULL */
	free(m3); printList();

	exit(EXIT_SUCCESS);
}
