#include <stdio.h>
#include <stdlib.h>

/* Define Structure for a Node */
/* (contains its own value and the pointer to the next node) */
struct node {
    int e;
    struct node *next;
};

/* Global struct to store the head element of the singly-linked list */
static struct node *head = NULL;

/* insertElement(int value) Insert a new element at the end of the list */
/* Throws an error (return code -1) if the given value is negative, */
/* the memory allocation for the new node failed or if list already */
/* contains an element with the same value */
int insertElement(int value) {
    /* Check if the value is negative (-> return error code -1) */
    if(value < 0) {
        return -1;
    }

    /* Allocate memory for the new node */
    /* malloc() takes the amount (size) of the memory to allocate as an agrument */
    /* and returns a pointer the memory - this pointer needs to be casted */
    /* to the correct type (struct node *) */
    struct node *n = (struct node *) malloc( sizeof(struct node) );
    /* check if memory allocation worked - otherwise return error code -1 */
    if (n == NULL) {
        return -1;
    }

    /* store the value inside the node */
    n->e = value;

    n->next = NULL;

    if(head == NULL) {
        /* list is empty, we simply need to set the newly created node as the head */
        head = n;
    } else {
        /* list is not empty */
        /* set current and drag pointers */
        struct node *cur = head->next;
        struct node *drag = head;
        /* iterate through the list */
        /* stop when cur does not point to anything */
        while(cur != NULL) {
            /* a new with this value already exists in the linked list - exit with error code -1 */
            if(drag->e == value) {
                free(n);
                return -1;
            }
            /* move drag and cur pointer each one node forward */
            drag = cur;
            cur = cur->next;
        }
        /* check last node */
        if (drag->e == value) {
            free(n);
            return -1;
        }

        /* set the newly created node at the end of the list */
        drag->next = n;
    }

    return value;
}
/* removeElement(void): remove the first (i.e. the oldest) element of the singly-linked list */
/* returns error code -1 if the list is empty (nothing to remove), */
/* otherwise returns the value of the removed node */
int removeElement(void) {
    if(head == NULL) {
        /* list is empty, return error code -1 */
        return -1;
    }

    /* store a pointer the second node of the linked list */
    struct node *next = head->next;
    /* store the value of the first node (head) of the list */
    int v = head->e;
    /* free the memory allocated by the first node */
    /* free() does not return an exit code */
    free(head);
    /* let the head point to the second node (now the first one) of the list */
    head = next;

    /* return the value of the removed node */
    return v;
}

int main (int argc, char* argv[]) {
	printf("insert 47: %d\n", insertElement(47));
	printf("insert 11: %d\n", insertElement(11));
	printf("insert 23: %d\n", insertElement(23));
	printf("insert 11: %d\n", insertElement(11));
	printf("insert -7: %d\n", insertElement(-7)); /* custom test */

	printf("remove: %d\n", removeElement());
	printf("remove: %d\n", removeElement());
	printf("remove: %d\n", removeElement()); /* custom test */
	exit(EXIT_SUCCESS);
}
