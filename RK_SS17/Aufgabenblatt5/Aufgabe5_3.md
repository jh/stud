# Aufgabenblatt 5

## Aufgabe 5.3: Persistentes HTTP mit Pipelining

Folgende Latenzen treten auf:

- $2RTT$ zum Aufbau der Verbindung und zum Anfragen der
  Basis-HTML-Seite;

- $(M+1)\cdot\frac{O}{R}$ zum Übertragen der $(M+1)$ Objekte der Größe
  (jeweils) $O$;

- $P\left[RTT+\frac{S}{R}\right]-\left(2^P-1\right)\frac{S}{R}$ als
  Slow-Start-Latenzen einmal am Anfang (später aber nicht mehr, weil
  die Verbindung dann ja schon steht).

Insgesamt also $\left(M+1\right)\frac{O}{R}+2RTT+P\left[RTT+\frac{S}{R}\right]-\left(2^P-1\right)\frac{S}{R}$.

Für die Anfrage der Bilder fällt eine weitere Latenz von $RTT$ an
(gerade die Wartezeit zwischen dem $K$-ten und dem $(K+1)$-ten
Fenster). Die korrekte Formel lautet also

$\left(M+1\right)\frac{O}{R}+3RTT+P\left[RTT+\frac{S}{R}\right]-\left(2^P-1\right)\frac{S}{R}$.

![Persistentes HTTP mit Pipelining](Grafik5_3.pdf)
