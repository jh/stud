# Aufgabenblatt 5

## Aufgabe 5.5: Fragen zu den HTTP-Varianten

1. Richtig: Weil auf die eine Anfrage des einen Objekts keine weiteren
   Anfragen folgen, kann die Persistenz der Verbindung nicht
   ausgenutzt werden, weshalb hier kein Unterschied zwischen beiden
   Varianten besteht.
2. Richtig: Aufgrund des Slow-Start-Mechanismus beträgt die
   Fenstergröße zu Beginn 1; da $O > L$ ist, reicht das aber nicht, um
   das Objekt vollständig zu übertragen. Der Server muss also auf das
   erste ACK warten und dann weitersenden; daher ergibt sich
   mindestens eine Wartezeit für den Server.
3. Falsch: Bei persistentem HTTP fallen nur die Latenzen $2RTT$ am
   Anfang für den Verbindungsaufbau und die erste Anfrage und dann je
   eine weitere Latenz $RTT$ pro Objektanfrage an, hier also 9
   weitere. Insgesamt ergibt sich also ein RTT-Anteil an der
   Antwortzeit von $11$ RTT.
4. Falsch: Die Objekte können in zwei "Schüben" zu je 5 Objekten
   angefragt werden. Dabei fällt jeweils eine Latenz von $2RTT$ an; da
   die ersten fünf und die zweiten fünf Objekte jeweils gleichzeitig
   angefragt werden können, beträgt der RTT-Anteil an der Antwortzeit
   nur $4$ RTT.
