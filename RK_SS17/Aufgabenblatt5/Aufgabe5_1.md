# Aufgabenblatt 5

## Aufgabe 5.1: Nichtpersistentes HTTP mit sequentiellen Verbindungen

Verzögerung für die Übertragung eines Objektes über eine
TCP-Verbindung (laut Vorlesungsfolie Transportschicht-163):

$2RTT+\frac{O}{R}+P\left[RTT+\frac{S}{R}\right]-\left(2^P-1\right)\frac{S}{R}$

Da $(M+1)$ Objekte übertragen werden müssen ergibt sich für die
Gesamtlatenz:

$\left(2RTT+\frac{O}{R}+P\left[RTT+\frac{S}{R}\right]-\left(2^P-1\right)\frac{S}{R}\right)\cdot{(M+1)}$
