# Aufgabenblatt 5

## Aufgabe 5.2: Nichtpersistentes HTTP mit parallelen Verbindungen

Folgende Latenzen treten auf:

- $\frac{O}{R}$ für die Übertragung der Basis-HTML-Datei;

- $\frac{1}{X} \cdot M\frac{O}{\frac{R}{X}} = \frac{MO}{R}$ für die
  Übertragung der $M$ Bilder auf $X$ parallelen Verbindungen, die sich
  jedoch alle die verfügbare Bandbreite teilen müssen;

- $2RTT$ für den ersten Verbindungsaufbau und die Anfrage der
  Basis-HTML-Datei;

- $\frac{M}{X} \cdot 2RTT$ für die längste Folge sequentieller
  Verbindungsaufbauten und HTTP-Anfragen (da ja nur $X$ Verbindungen
  gleichzeitig offen sind, können nicht alle $M$ Bilder gleichzeitig
  angefragt werden);

- Slow-Start-Wartezeiten.

Zusammen also $\left(M+1\right)\frac{O}{R}+2\left(\frac{M}{X}+1\right)RTT+\emph{Slow-Start-Latenz}$.

Die Slow-Start-Wartezeiten betragen, analog zu Aufgabe 5.1,
$P\left[RTT+\frac{S}{R}\right]-\left(2^P-1\right)\frac{S}{R}$ für jede
neu aufgebaute Verbindung. Davon gibt es $\left(1+\frac{M}{X}\right)$
"hintereinander", weil zuerst die Basis-HTML-Datei angefragt wird und
dann $M$ Bilder mit $X$ parallelen Verbindungen angefragt werden, was
(bei $X \leq M$) $\frac{M}{X}$ "aufeinanderfolgende" Verbindungen
benötigt, da nicht alle Anfragen gleichzeitig gestellt werden können.

Insgesamt also:

$\left(M+1\right)\frac{O}{R}+2\left(\frac{M}{X}+1\right)\cdot{RTT}\cdot{\left(P\left[RTT+\frac{S}{R}\right]-\left(2^P-1\right)\frac{S}{R}\right)}$

![Nichtpersistentes HTTP mit parallelen Verbindungen](Grafik5_2.pdf)
