#!/usr/bin/perl

# On Debian systems, make sure you have the package
# "libchart-gnuplot-perl" installed

use strict;
use warnings;

use Chart::Gnuplot;
use List::Util qw[min];
use POSIX qw[ceil floor];

my $O = 5 * 1000 * 8;
my $M = 10;
my $L = 536 * 8;
my $X = 10;
my @R = ( 28000, 100000, 1000000, 10000000 );

sub getP {
    my $RTT = shift;
    my $R = shift;
    my $P = min(floor(log(1 + ($RTT / ($L / $R))) / log(2)) + 1, ceil(log(($O / $L) + 1) / log(2)) - 1);
    return $P;
}

sub get_O_R {
    my $RTT = shift;

    my @x;
    my @y;

    for my $i (0 .. $#R) {
        push @x, $R[$i];
        push @y, ($O / $R[$i]);
    }

    return (\@x, \@y);
}

sub get_lat_1 {
    my $RTT = shift;

    my @x;
    my @y;

    for my $i (0 .. $#R) {
        my $P = getP($RTT, $R[$i]);

        push @x, $R[$i];
        push @y, (((2 * $RTT) + ($O / $R[$i]) + ($P * ($RTT + ($L / $R[$i]))) - (($L / $R[$i]) * ((2 ** $P) - 1))) * ($M + 1));
    }

    return (\@x, \@y);
}

sub get_lat_2 {
    my $RTT = shift;

    my @x;
    my @y;

    for my $i (0 .. $#R) {
        my $P = getP($RTT, $R[$i]);

        push @x, $R[$i];
        push @y, ((($M + 1) * ($O / $R[$i])) + (2 * (($M / $X) + 1) * $RTT * (($P * ($RTT + ($L / $R[$i]))) - (($L / $R[$i]) * ((2 ** $P) - 1)))));
    }

    return (\@x, \@y);
}

sub get_lat_3 {
    my $RTT = shift;

    my @x;
    my @y;

    for my $i (0 .. $#R) {
        my $P = getP($RTT, $R[$i]);

        push @x, $R[$i];
        push @y, ((($M + 1) * ($O / $R[$i])) + (3 * $RTT) + (($P * ($RTT + ($L / $R[$i]))) - (($L / $R[$i]) * ((2 ** $P) - 1))));
    }

    return (\@x, \@y);
}

sub draw {
    my $RTT = shift;

    my @dataSets = ();

    my ($x, $y) = get_O_R($RTT);
    push @dataSets, Chart::Gnuplot::DataSet->new(
        xdata => \@{$x},
        ydata => \@{$y},
        title => "O/R",
        style => "linespoints",
        );
    ($x, $y) = get_lat_1($RTT);
    push @dataSets, Chart::Gnuplot::DataSet->new(
        xdata => \@{$x},
        ydata => \@{$y},
        title => "Lat 5.1",
        style => "linespoints",
        );
    ($x, $y) = get_lat_2($RTT);
    push @dataSets, Chart::Gnuplot::DataSet->new(
        xdata => \@{$x},
        ydata => \@{$y},
        title => "Lat 5.2",
        style => "linespoints",
        );
    ($x, $y) = get_lat_2($RTT);
    push @dataSets, Chart::Gnuplot::DataSet->new(
        xdata => \@{$x},
        ydata => \@{$y},
        title => "Lat 5.3",
        style => "linespoints",
        );

    my $chart = Chart::Gnuplot->new(
        output => "chart_" . $RTT . ".png",
        title => "Aufgabe 5.4, RTT = " . $RTT . "s",
        xlabel => "R [bps]",
        ylabel => "Time [s]",
        );
    $chart->plot2d(@dataSets);
}

draw 0.1;
draw 1;
