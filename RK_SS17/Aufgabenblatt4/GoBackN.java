import fau.cs7.nwemu.*;
import java.util.zip.CRC32;
import java.util.LinkedList;

public class GoBackN
{
    public static final int windowWidth = 8;
    public static final int m = 2 * windowWidth;

    public static void main (String[] args)
    {
        AbstractHost hostA = new SendingHost ();
        AbstractHost hostB = new ReceivingHost ();

        NWEmu testEmu = new NWEmu (hostA, hostB);
        testEmu.randTimer ();
        testEmu.emulate (20, 0.9, 0.9, 10.0, 2);
    }
}

class ChecksumHelper
{
    private static byte[] int2bytes (int i)
    {
        byte[] result = new byte [4];

        result [0] = (byte) ((i & 0xFF000000) >> 24);
        result [1] = (byte) ((i & 0x00FF0000) >> 16);
        result [2] = (byte) ((i & 0x0000FF00) >> 8);
        result [3] = (byte) (i & 0x000000FF);

        return result;
    }

    public static int getChecksum (NWEmuPkt pkg)
    {
        byte[] b = new byte [16+NWEmu.PAYSIZE];

        System.arraycopy (int2bytes (pkg.seqnum), 0, b, 0, 4);
        System.arraycopy (int2bytes (pkg.acknum), 0, b, 4, 4);
        System.arraycopy (int2bytes (pkg.flags), 0, b, 8, 4);
        System.arraycopy (int2bytes (0), 0, b, 12, 4);
        System.arraycopy (pkg.payload, 0, b, 16, NWEmu.PAYSIZE);

        CRC32 crc32 = new CRC32 ();
        crc32.update (b);

        return (int) crc32.getValue ();
    }
}

class SendingHost extends AbstractHost
{
    private int base = 0, next = 0; // used as indices for lastPkgs
    private int seqBase = 0, seqNext = 0; // used for sequence numbers
    private NWEmuPkt[] lastPkgs = new NWEmuPkt [GoBackN.windowWidth];
    private LinkedList<NWEmuMsg> pkgQueue = new LinkedList<NWEmuMsg> ();
    private double timerDelay = 7.5;

    @Override public void init ()
    {
    }

    @Override public void timerInterrupt ()
    {
        if (base != next)
        {
            for (int i = base; i != next; i = (i + 1) % GoBackN.windowWidth)
                toLayer3 (lastPkgs [i]);
        }
        else
        {
            for (int i = 0; i < GoBackN.windowWidth; ++i)
                toLayer3 (lastPkgs [(base + i) % GoBackN.windowWidth]);
        }

        startTimer (this.timerDelay);
    }

    @Override public void input (NWEmuPkt packet)
    {
        boolean isInRange = false;
        for (int i = 0; (this.seqBase + i) % GoBackN.m != this.seqNext; ++i)
        {
            if ((this.seqBase + i) % GoBackN.m == packet.acknum)
            {
                isInRange = true;
                break;
            }
        }

        if (isInRange)
        {
            if (packet.checksum == ChecksumHelper.getChecksum (packet))
            {
                stopTimer ();

                int delta = 0;
                while ((this.seqBase + delta) % GoBackN.m != packet.acknum)
                    ++delta;
                delta += 1;

                this.base = (this.base + delta) % GoBackN.windowWidth;
                this.seqBase = (this.seqBase + delta) % GoBackN.m;

                if (this.seqBase != this.seqNext)
                    startTimer (this.timerDelay);

                sendMessages ();
            }
        }
    }

    @Override public Boolean output (NWEmuMsg message)
    {
        this.pkgQueue.add (message);
        sendMessages ();

        return true;
    }

    private void sendMessages ()
    {
        boolean startTimer = (this.seqBase == this.seqNext);
        boolean firstIt = true;

        while (this.seqNext != (this.seqBase + GoBackN.windowWidth) % GoBackN.m)
        {
            NWEmuMsg message = pkgQueue.pollFirst ();
            if (message == null)
            {
                if (firstIt)
                    startTimer = false;

                break;
            }

            NWEmuPkt pkg = new NWEmuPkt ();
            System.arraycopy (message.data, 0, pkg.payload, 0, NWEmu.PAYSIZE);

            pkg.flags = 0;
            pkg.acknum = 0;
            pkg.seqnum = this.seqNext;
            pkg.checksum = ChecksumHelper.getChecksum (pkg);

            this.lastPkgs [this.next] = pkg;
            this.next = (this.next + 1) % GoBackN.windowWidth;
            this.seqNext = (this.seqNext + 1) % GoBackN.m;

            toLayer3 (pkg);
            firstIt = false;
        }

        if (startTimer)
            startTimer (this.timerDelay);
    }
}

class ReceivingHost extends AbstractHost
{
    private int seq = 0;
    private int ack = -1;

    @Override public void init ()
    {
        this.seq = 0;
        this.ack = -1;
    }

    @Override public void input (NWEmuPkt packet)
    {
        if (packet.seqnum == this.seq)
        {
            if (packet.checksum == ChecksumHelper.getChecksum (packet))
            {
                NWEmuMsg msg = new NWEmuMsg ();
                System.arraycopy (packet.payload, 0, msg.data, 0, NWEmu.PAYSIZE);

                toLayer5 (msg);
                this.ack = this.seq;
                sendAck (this.ack);
                this.seq = (this.seq + 1) % GoBackN.m;

                return;
            }
        }

        sendAck (this.ack);
    }

    private void sendAck (int acknum)
    {
        NWEmuPkt pkg = new NWEmuPkt ();
        pkg.acknum = acknum;
        pkg.checksum = ChecksumHelper.getChecksum (pkg);
        toLayer3 (pkg);
    }
}
