import fau.cs7.nwemu.*;
import java.util.zip.CRC32;

public class ABP
{
    public static void main (String[] args)
    {
        AbstractHost hostA = new SendingHost ();
        AbstractHost hostB = new ReceivingHost ();

        NWEmu testEmu = new NWEmu (hostA, hostB);
        testEmu.randTimer ();
        testEmu.emulate (10, 0.1, 0.3, 1000.0, 2);
    }
}

class ChecksumHelper
{
    private static byte[] int2bytes (int i)
    {
        byte[] result = new byte [4];

        result [0] = (byte) ((i & 0xFF000000) >> 24);
        result [1] = (byte) ((i & 0x00FF0000) >> 16);
        result [2] = (byte) ((i & 0x0000FF00) >> 8);
        result [3] = (byte) (i & 0x000000FF);

        return result;
    }

    public static int getChecksum (NWEmuPkt pkg)
    {
        byte[] b = new byte [16+NWEmu.PAYSIZE];

        System.arraycopy (int2bytes (pkg.seqnum), 0, b, 0, 4);
        System.arraycopy (int2bytes (pkg.acknum), 0, b, 4, 4);
        System.arraycopy (int2bytes (pkg.flags), 0, b, 8, 4);
        System.arraycopy (int2bytes (0), 0, b, 12, 4);
        System.arraycopy (pkg.payload, 0, b, 16, NWEmu.PAYSIZE);

        CRC32 crc32 = new CRC32 ();
        crc32.update (b);

        return (int) crc32.getValue ();
    }
}

class SendingHost extends AbstractHost
{
    private int seq = 1;
    private NWEmuPkt lastPkg = null;
    private double timerDelay = 30;
    private boolean waitingForAck = false;

    @Override public void init ()
    {
        seq = 1;
        waitingForAck = false;
    }

    @Override public void timerInterrupt ()
    {
        toLayer3 (lastPkg);
        startTimer (this.timerDelay);
    }

    @Override public void input (NWEmuPkt packet)
    {
        if (packet.acknum == this.seq)
        {
            if (packet.checksum == ChecksumHelper.getChecksum (packet))
            {
                stopTimer ();
                this.waitingForAck = false;
                this.seq = (this.seq + 1) % 2;
            }
        }
    }

    @Override public Boolean output (NWEmuMsg message)
    {
        if (this.waitingForAck)
            return false;

        NWEmuPkt pkg = new NWEmuPkt ();
        System.arraycopy (message.data, 0, pkg.payload, 0, NWEmu.PAYSIZE);

        pkg.flags = 0;
        pkg.acknum = 0;
        pkg.seqnum = this.seq;

        pkg.checksum = ChecksumHelper.getChecksum (pkg);

        this.waitingForAck = true;
        this.lastPkg = pkg;
        startTimer (this.timerDelay);
        toLayer3 (pkg);

        return true;
    }
}

class ReceivingHost extends AbstractHost
{
    private int seq = 1;

    @Override public void init ()
    {
        this.seq = 1;
    }

    @Override public void input (NWEmuPkt packet)
    {
        if (packet.seqnum == this.seq)
        {
            if (packet.checksum == ChecksumHelper.getChecksum (packet))
            {
                NWEmuMsg msg = new NWEmuMsg ();
                System.arraycopy (packet.payload, 0, msg.data, 0, NWEmu.PAYSIZE);

                toLayer5 (msg);
                sendAck (this.seq);
                this.seq = (this.seq + 1) % 2;

                return;
            }
        }

        sendAck ((this.seq + 1) % 2);
    }

    private void sendAck (int acknum)
    {
        NWEmuPkt pkg = new NWEmuPkt ();
        pkg.acknum = acknum;
        pkg.checksum = ChecksumHelper.getChecksum (pkg);
        toLayer3 (pkg);
    }
}
