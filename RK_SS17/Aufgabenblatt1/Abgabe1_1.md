# Aufgabenblatt 1

## Aufgabe 1.1

1. $d_{ges} = (N-1+E) \cdot \frac{L+h}{R} + d_{con}$

2. $d_{ges} = (N-1+E) \cdot \frac{L+2h}{R}$

3. $d_{ges} = E \cdot \frac{L + 2h}{R}$

4. $d_{ges} = \frac{N \cdot L + h}{R} + d_{con}$
