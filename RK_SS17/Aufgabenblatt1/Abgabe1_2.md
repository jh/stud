# Aufgabenblatt 1

## Aufgabe 1.2

Ansatz (aus Aufgabe 1.1): $d = \frac{L+40}{R}\cdot\left(N-1+2\right) = \frac{L+40}{R}\cdot\left(N+1\right)$

Es ist $N = \frac{O}{L}$, also $d = \frac{L+40}{R}\cdot\left(\frac{O}{L}+1\right)$. Diese Formel wollen wir minimieren:

$\frac{\partial}{\partial L} d = \frac{1}{R}\cdot\left(1+40\cdot\left(-\frac{O}{L^2}\right)\right) = \frac{1}{R}\cdot\left(1-\frac{40 \cdot O}{L^2}\right) \stackrel{!}{=} 0$

$\Leftrightarrow$ $1 - \frac{40 \cdot O}{L^2} = 0$

$\Leftrightarrow$ $\frac{40 \cdot O}{L^2} = 1$

$\Leftrightarrow$ $L^2 = 40 \cdot O$

$\Leftrightarrow$ $L = \sqrt{40 \cdot O}$ $\quad$ (da $L \geq 0$)


Außerdem ist $\frac{\partial^2}{\partial L^2} d = \frac{1}{R}\cdot\left(1+\frac{80 \cdot O}{L^3}\right)$ und daher insbesondere $\frac{\partial^2}{\partial L^2} d > 0$ für $L = \sqrt{40 \cdot O}$, weshalb es sich dabei um einen Tiefpunkt handeln muss.

$\implies L = \sqrt{40 \cdot O}$. Sinnvollerweise könnte man diesen Wert dann noch zum nächsten Teiler von $O$ runden (insbesondere würde diese Art der Rundung sicherstellen, dass die Paketgröße nicht größer als $O$ ist, da für Zahlen größer als $O$ der nächste Teiler von $O$ $O$ selbst ist, ein Fall, der auftritt, falls $O < 40$).
