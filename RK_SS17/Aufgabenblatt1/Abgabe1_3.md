# Aufgabenblatt 1

## Aufgabe 1.3

1. $d_{prop} = \frac{l}{v}$

2. $d_{trans} = \frac{L}{R}$

3. $d_{ee} = d_{trans} + d_{prop} + d_{queue}$

4. Zum Zeitpunkt $t = d_{trans}$ hat das letzte Bit des Paket den Host A gerade verlassen (es befindet sich auf der Leitung).

5. Wenn $d_{prop}$ größer als $d_{trans}$ ist, befindet sich das erste Bit des Pakets zum Zeitpunkt $t = d_{trans}$ immernoch auf der Leitung.

6. Wenn $d_{prop}$ kleiner als $d_{trans}$ ist, ist das erste Bit des Pakets zum Zeitpunkt $t = d_{trans}$ bereits beim Host B angekommen.

7. $d_{prop} \overset{!}{=} d_{trans}$

    $\frac{l}{v} \overset{!}{=} \frac{L}{R}$

    $l = \frac{L}{R} \cdot v = \frac{100 bits}{28 kbps} \cdot 2,5 \cdot 10^8 \frac{m}{s} = 892.857 m \approx 893 km$
