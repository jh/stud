#!/bin/sh
set -x

# Launch web server and store PID
java WebServer &
ws_pid=$!

# Start test and store exit code
java -jar wsc_mt.jar --host 127.0.0.1 --port 4711 --show-exceptions
test=$?

# Kill web server
kill $ws_pid

# exit with return code of test
exit $test
