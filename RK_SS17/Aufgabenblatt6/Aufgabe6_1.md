# Aufgabe 6.1

## Schritt 1

von A zu | $D_{A}(\cdot)$ | $nh_{A}(\cdot)$
:-------:|:--------------:|:-------------:
A	 | 0		  | $-$
B        | 5		  | B
C        | 3		  | C
D        | 10		  | D

von B zu | $D_{B}(\cdot)$ | $nh_{B}(\cdot)$
:-------:|:--------------:|:-------------:
A	 | 5		  | A
B        | 0		  | $-$
C        | 3		  | C
D        | 4		  | D

von C zu | $D_{C}(\cdot)$ | $nh_{C}(\cdot)$
:-------:|:--------------:|:-------------:
A	 | 3		  | A
B        | 3		  | B
C        | 0		  | $-$
D        | $\infty$	  | $-$

von D zu | $D_{D}(\cdot)$ | $nh_{D}(\cdot)$
:-------:|:--------------:|:-------------:
A	 | 10		  | A
B        | 4		  | B
C        | $\infty$	  | $-$
D        | 0		  | $-$

## Schritt 2

von A zu | $D_{A}(\cdot)$ | $nh_{A}(\cdot)$
:-------:|:--------------:|:-------------:
A	 | 0		  | $-$
B        | 5		  | B
C        | 3		  | C
D        | 9		  | B

von B zu | $D_{B}(\cdot)$ | $nh_{B}(\cdot)$
:-------:|:--------------:|:-------------:
A	 | 5		  | A
B        | 0		  | $-$
C        | 3		  | C
D        | 4		  | D

von C zu | $D_{C}(\cdot)$ | $nh_{C}(\cdot)$
:-------:|:--------------:|:-------------:
A	 | 3		  | A
B        | 3		  | B
C        | 0		  | $-$
D        | 7    	  | $-$

von D zu | $D_{D}(\cdot)$ | $nh_{D}(\cdot)$
:-------:|:--------------:|:-------------:
A	 | 9		  | B
B        | 4		  | B
C        | 7    	  | B
D        | 0		  | $-$

## Schritt 3

von A zu | $D_{A}(\cdot)$ | $nh_{A}(\cdot)$
:-------:|:--------------:|:-------------:
A	 | 0		  | $-$
B        | 5		  | B
C        | 3		  | C
D        | 9		  | B

von B zu | $D_{B}(\cdot)$ | $nh_{B}(\cdot)$
:-------:|:--------------:|:-------------:
A	 | 5		  | A
B        | 0		  | $-$
C        | 3		  | C
D        | 4		  | D

von C zu | $D_{C}(\cdot)$ | $nh_{C}(\cdot)$
:-------:|:--------------:|:-------------:
A	 | 3		  | A
B        | 3		  | B
C        | 0		  | $-$
D        | 7    	  | $-$

von D zu | $D_{D}(\cdot)$ | $nh_{D}(\cdot)$
:-------:|:--------------:|:-------------:
A	 | 9		  | B
B        | 4		  | B
C        | 7    	  | B
D        | 0		  | $-$

Bei Schritt 3 haben sich keine Änderungen ergeben. Folglich hat das Verfahren
die Konvergenz erreicht. In Schritt 4 und 5 wird es deshalb auch kein Änderungen
mehr geben.
