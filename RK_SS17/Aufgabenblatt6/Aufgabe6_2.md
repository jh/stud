# Aufgabe 6.2

## Tabelle für Knoten A

Schritt | N                | D(B), p(B) | D(C), p(C) | D(D), p(D) | D(E), p(E) | D(F), p(F)
:------:|------------------|------------|------------|------------|------------|------------
0       | A                | 10, A      |$\infty$, - | 8, A       |$\infty$, - | 1, A
1       | A, F             | 10, A      | 4, F       | 8, A       | 2, F       | 1, A
2       | A, F, E          | 10, A      | 4, F       | 8, E       | 2, F       | 1, A
3       | A, F, E, C       | 6, C       | 4, F       | 8, E       | 2, F       | 1, A
4       | A, F, E, C, B    | 6, C       | 4, F       | 7, B       | 2, F       | 1, A
5       | A, F, E, C, B, D | 6, C       | 4, F       | 7, B       | 2, F       | 1, A


## Forward-Search-Algorithmus

Schritt | bestätigte Liste                                     | vorläufige Liste
:------:|------------------------------------------------------|-----------------
0       | (A,0,-)                                              | -
1       | (A,0,-)                                              | (B,10,B), (D,8,D), (F,1,F)
2       | (A,0,-), (F,1,F)                                     | (B,10,B), (D,8,D), (E,2,F), (C,4,F)
3       | (A,0,-), (F,1,F), (E,2,F)                            | (B,10,B), (D,8,D), (C,4,F)
4       | (A,0,-), (F,1,F), (E,2,F), (C,4,F)                   | (B,6,F), (D,8,D)
5       | (A,0,-), (F,1,F), (E,2,F), (C,4,F), (B,6,F)          | (D,7,F)
6       | (A,0,-), (F,1,F), (E,2,F), (C,4,F), (B,6,F), (D,7,F) | -

## Resultierende Forwarding-Tabelle für Knoten A

Ziel | Link
-----|-----
B    | F
C    | F
D    | F
E    | F
F    | F
