# Aufgabenblatt 2

## Aufgabe 2.5

- Beginn der Paketaufzeichnung: Apr 20, 2005 09:53:14.648946
  mitteleuropäische Sommerzeit (CEST), Ende der Paketaufzeichnung:
  Apr 20, 2005 09:54:35.499049 CEST
- Webserver:
    - www.heise.de (193.99.144.85)
    - heise.ivwbox.de (193.99.144.250)
    - www.lwn.net (66.216.68.48)
    - lwn.net (66.216.68.48)
    - old.lwn.net (66.216.68.48)
- Objekttypen:
    - text/html
    - text/css
    - application/x-javascript
    - image/gif
    - JPEG JFIF image
    - text/plain
    - PNG
- 37 Requests, $32\times$ 200 OK, $1\times$ 301 Moved Permanently, $4\times$ 302 Found
- die meisten Objekte wurden bei www.heise.de abgefragt
- FTP-Server: 131.188.37.33
- Benutzername: ```ti4uebung```, Passwort: ```FTP_inseCure```   
  Passwort wird im Klartext übertragen, also unsicher
- Datei ```/8fau110.txt```, Text: Faust I
- FTP: 1948 Bytes, FTP-DATA: 216663 Bytes.  
  Das Protokoll nutzt zwei Ports: FTP für Kontrolldaten und FTP-Data für die eigentliche Datenübertragung.  
  In diesem Fall nutzt der Server die Well-known Ports 21 und 20, wobei der Client Port 38701 für FTP und Port 38702/38703 für FTP-Data nutzt.
