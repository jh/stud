import java.io.*;
import java.net.*;
import java.util.concurrent.TimeUnit;

public class UDPClient
{
    public static final int N = 4711;

    public static void main (String[] argv) throws IOException, InterruptedException
    {
        String host;
        if (argv.length > 0)
            host = argv [0];
        else
            host = "localhost";

        int wait = 0;
        if (argv.length > 1)
            wait = Integer.parseInt (argv [1]);

        DatagramSocket sock = new DatagramSocket ();

        byte[] sendData = new byte [4];

        for (int i = 0; i <= 1000000; ++i)
        {
            sendData [0] = (byte) ((i & 0xFF000000) >> 24);
            sendData [1] = (byte) ((i & 0x00FF0000) >> 16);
            sendData [2] = (byte) ((i & 0x0000FF00) >> 8);
            sendData [3] = (byte) (i & 0xFF);

            DatagramPacket send = new DatagramPacket (sendData, sendData.length,
                                                      InetAddress.getByName (host),
                                                      N);

            sock.send (send);

            if (wait > 0)
                TimeUnit.MILLISECONDS.sleep (wait);
        }
    }
}
