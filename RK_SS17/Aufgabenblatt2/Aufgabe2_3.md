# Aufgabenblatt 2

## Aufgabe 2.3

```
$ telnet faui45.informatik.uni-erlangen.de 25
# 220
HELO faui06f.informatik.uni-erlangen.de
# 250
MAIL FROM: fabian.wolff@fau.de
# 250
RCPT TO: rk17abgabe@i7.informatik.uni-erlangen.de
# 250
DATA
# 354
From: fabian.wolff@fau.de
To: rk17abgabe@i7.informati.uni-erlangen.de
Subject: Aufgabe 2.2 Donnerstag Henschel Sauerwein Schmid Wolff


.
# 250
QUIT
# 221
```

Lösung: Donnerstag Henschel Sauerwein Schmid Wolff
