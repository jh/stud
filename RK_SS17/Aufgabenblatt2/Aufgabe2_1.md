# Aufgabenblatt 2

## Aufgabe 2.1

```
$ telnet www7.cs.fau.de 80
Trying 131.188.16.200...
Connected to www7.cs.fau.de
GET /en/ HTTP/1.0

HTTP/1.0 404 Not Found
...
```

Das `telnet` Programm wird angewiesen sich mit dem Host `www7.cs.fau.de` auf Port 80 zu verbinden. Um diese Verbindng herzustellen wird zunächst der Hostname `www7.cs.fau.de` mit dem DNS (*Domain Name System*) zu einer IP Adresse (`131.188.16.200`) aufgelöst. Beim Absenden der Anfrage (`GET /en/ HTTP/1.0`) wird jedoch der `Host` HTTP-Header nicht gesetzt, somit weiß der Webserver `131.188.16.200:80` nicht mit welcher Seite sich der Client verbinden möchte. (Ein Webserver kann mehrere Webseiten zur Verfügung stellen, so genannte *Virtual Hosts*).


```
$ telnet www7.cs.fau.de 80
GET /en/ HTTP/1.0
Host: www7.cs.fau.de

HTTP/1.0 200 OK
Date: Tue, 16 May 2017 08:26:14 GMT
Server: Apache
Expires: Wed, 11 Jan 1984 05:00:00 GMT
Cache-Control: no-cache, must-revalidate, max-age=0
...
```
Die Zeile `Host: www7.cs.fau.de` setzt den `Host` HTTP-Header. Mithilfe dieser weiß der Webserver mit welcher Website sich der Client verbinden möchte und kann somit die richtige Seite ausliefern.

Der `Date` HTTP-Header gibt das Datum und die Uhrzeit des Absendezeitpunkts der Antwort an.
Der `Expires` HTTP-Header gibt an, ab wann der Inhalt der Datei erneut geladen werden sollte. Bis zu diesem Zeitpunkt kann der Client (z.B. ein Webbrowser) eine lokale Kopie (*Cache*) der Datei nutzen. (In diesem Beispiel liegt dieses Datum weit in der Vergangenheit, somit holt sich jeder Client immer eine neue Kopie.)

Der `Last-Modified` HTTP-Header gibt den Zeitpunkt der letzten Modifikation der Datei auf dem Server an. Das Fehlen dieses Headers kann viele Gründe haben, so könnte der Webserver dieses HTTP-Header ganz einfach abgeschalten haben oder es handelt sich um dynamisch generierte Inhalte, welche bei jedem Aufruf der Webseite neu erzeugt werden und der `Last-Modified` Header somit immer gleich dem `Date` Header wäre.

```
$ telnet www7.cs.fau.de 80
GET /en/people/german/ HTTP/1.0
Host: www7.cs.fau.de

HTTP/1.0 200 OK
...
```
