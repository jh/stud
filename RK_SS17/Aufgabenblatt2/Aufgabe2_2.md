# Aufgabenblatt 2

## Aufgabe 2.2

```
$ dig www.informatik.uni-erlangen.de
; <<>> DiG 9.10.3-P4-Debian <<>> www.informatik.uni-erlangen.de
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 37959
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 2, AUTHORITY: 4, ADDITIONAL: 4

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;www.informatik.uni-erlangen.de.	IN	A

;; ANSWER SECTION:
www.informatik.uni-erlangen.de.	80277 IN CNAME	infwww.informatik.uni-erlangen.de.
infwww.informatik.uni-erlangen.de. 80277 IN A	131.188.34.49

;; AUTHORITY SECTION:
uni-erlangen.de.	78516	IN	NS	tuminfo1.informatik.tu-muenchen.de.
uni-erlangen.de.	78516	IN	NS	ns1.rrze.uni-erlangen.de.
uni-erlangen.de.	78516	IN	NS	rrzs2.rz.uni-regensburg.de.
uni-erlangen.de.	78516	IN	NS	ns2.rrze.uni-erlangen.de.

;; ADDITIONAL SECTION:
ns1.rrze.uni-erlangen.de. 78517	IN	A	131.188.3.2
ns2.rrze.uni-erlangen.de. 78516	IN	A	131.188.12.100
rrzs2.rz.uni-regensburg.de. 86172 IN	A	132.199.1.2

;; Query time: 2 msec
;; SERVER: 2001:638:a000:1053:53::1#53(2001:638:a000:1053:53::1)
;; WHEN: Tue May 16 11:07:55 CEST 2017
;; MSG SIZE  rcvd: 269
```

Der DNS-Server antwortet mit einem `CNAME` Eintrag für die Domain `www.informatik.uni-erlangen.de`. Der `A-Record` für `infwww.informatik.uni-erlangen.de` wiederum zeigt auf die IP-Adresse `131.188.34.49`.

Die zuständigen Namensserver für `www.informatik.uni-erlangen.de` lauten:

* `tuminfo1.informatik.tu-muenchen.de`
* `ns1.rrze.uni-erlangen.de`
* `rrzs2.rz.uni-regensburg.de`
* `ns2.rrze.uni-erlangen.de`


Domains mit mehreren IP-Adressen (mehrere A-Records):
```
$ dig +short www.yahoo.com
hr-mig-atsv2.wg1.b.yahoo.com.
46.228.47.115
46.228.47.114
```

```
$ dig +short debian.org
140.211.166.202
130.89.148.14
5.153.231.4
128.31.0.62
149.20.4.15
```

```
$ dig +short digitalocean.com
104.16.113.208
104.16.109.208
104.16.110.208
104.16.112.208
104.16.111.208
```

```
$ dig +short linode.com
72.14.180.202
69.164.200.202
72.14.191.202
```

```
$ dig +noall +comments +answer +additional i7.informatik.uni-erlangen.de mx
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 31052
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 5, AUTHORITY: 4, ADDITIONAL: 14

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; ANSWER SECTION:
i7.informatik.uni-erlangen.de. 3462 IN	MX	20 faui45.informatik.uni-erlangen.de.
i7.informatik.uni-erlangen.de. 3462 IN	MX	10 faui7s0.informatik.uni-erlangen.de.
i7.informatik.uni-erlangen.de. 3462 IN	MX	30 mx-rz-2.rrze.uni-erlangen.de.
i7.informatik.uni-erlangen.de. 3462 IN	MX	30 mx-rz-1.rrze.uni-erlangen.de.
i7.informatik.uni-erlangen.de. 3462 IN	MX	30 mx-rz-3.rrze.uni-erlangen.de.

;; ADDITIONAL SECTION:
faui7s0.informatik.uni-erlangen.de. 77471 IN A	131.188.37.210
faui45.informatik.uni-erlangen.de. 77314 IN A	131.188.34.45
faui45.informatik.uni-erlangen.de. 77472 IN AAAA 2001:638:a000:4134::ffff:45
mx-rz-1.rrze.uni-erlangen.de. 77291 IN	A	131.188.11.20
mx-rz-1.rrze.uni-erlangen.de. 77312 IN	AAAA	2001:638:a000:1025::14
mx-rz-2.rrze.uni-erlangen.de. 77290 IN	A	131.188.11.21
mx-rz-2.rrze.uni-erlangen.de. 77293 IN	AAAA	2001:638:a000:1025::15
mx-rz-3.rrze.uni-erlangen.de. 77285 IN	A	131.188.11.22
mx-rz-3.rrze.uni-erlangen.de. 77285 IN	AAAA	2001:638:a000:1025::16
ns1.rrze.uni-erlangen.de. 77275	IN	A	131.188.3.2
ns2.rrze.uni-erlangen.de. 77363	IN	A	131.188.12.100
rrzs2.rz.uni-regensburg.de. 84736 IN	A	132.199.1.2
tuminfo1.informatik.tu-muenchen.de. 84353 IN A	131.159.0.1
```

Mail-Server (nach Priorität geordnet):

* `faui7s0.informatik.uni-erlangen.de`: `131.188.37.210`
* `faui45.informatik.uni-erlangen.de`: `131.188.34.45` / `2001:638:a000:4134::ffff:45`
* `mx-rz-2.rrze.uni-erlangen.de`: `131.188.11.21` / `2001:638:a000:1025::15`
* `mx-rz-1.rrze.uni-erlangen.de`: `131.188.11.20` / `2001:638:a000:1025::14`
* `mx-rz-3.rrze.uni-erlangen.de`: `131.188.11.22` / `2001:638:a000:1025::16`

Mehrere IP-Adressen für eine Domain auf dem DNS-Server zu hinterlegen hilft dabei die Auslastung der Server gleichmäßiger zu verteilen und erhöht die Ausfallsicherheit (ist einer der angegeben Server nicht erreichbar kann einfach der nächste Eintrag versucht werden).
