import java.io.*;
import java.net.*;

public class UDPServer
{
    public static final int N = 4711;

    public static void main (String[] argv) throws IOException
    {
        DatagramSocket sock = new DatagramSocket (N);

        byte[] recvData = new byte [4];

        for (int i = 0; i <= 1000000; ++i)
        {
            DatagramPacket recv = new DatagramPacket (recvData, recvData.length);
            sock.receive (recv);

            int num = ((recvData [0] & 0xFF) << 24)
                | ((recvData [1] & 0xFF) << 16)
                | ((recvData [2] & 0xFF) << 8)
                | (recvData [3] & 0xFF);

            if (num != i)
            {
                System.out.println ("Error: expected '" + i + "', got '" + num + "'");
                return;
            }
        }
    }
}
