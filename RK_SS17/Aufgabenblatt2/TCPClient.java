import java.io.*;
import java.net.*;

public class TCPClient
{
    public static final int N = 4711;

    public static void main (String[] argv) throws IOException
    {
        Socket sock;
        if (argv.length > 0)
            sock = new Socket (argv [0], N);
        else
            sock = new Socket ("localhost", N);

        DataOutputStream out = new DataOutputStream (sock.getOutputStream ());
        byte[] sendData = new byte [4];

        for (int i = 0; i <= 1000000; ++i)
        {
            sendData [0] = (byte) ((i & 0xFF000000) >> 24);
            sendData [1] = (byte) ((i & 0x00FF0000) >> 16);
            sendData [2] = (byte) ((i & 0x0000FF00) >> 8);
            sendData [3] = (byte) (i & 0xFF);

            out.write (sendData, 0, sendData.length);
        }
    }
}
