import java.io.*;
import java.net.*;

public class TCPServer
{
    public static final int N = 4711;

    public static void main (String[] argv) throws IOException
    {
        ServerSocket sock = new ServerSocket (N);
        Socket client = sock.accept ();

        DataInputStream in = new DataInputStream (client.getInputStream ());

        byte[] recvData = new byte [4];

        for (int i = 0; i <= 1000000; ++i)
        {
            in.read (recvData, 0, recvData.length);

            int num = ((recvData [0] & 0xFF) << 24)
                | ((recvData [1] & 0xFF) << 16)
                | ((recvData [2] & 0xFF) << 8)
                | (recvData [3] & 0xFF);

            if (num != i)
            {
                System.out.println ("Error: expected '" + i + "', got '" + num + "'");
                return;
            }
        }
    }
}
