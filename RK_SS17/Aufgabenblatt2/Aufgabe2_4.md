# Aufgabenblatt 2

## Aufgabe 2.4

Bei der UDP-Variante tritt vereinzelt der Fehler auf, dass Pakete in
der falschen Reihenfolge (oder gar nicht) ankommen und so Fehler
auslösen (weil die in den Paketen enthaltenen Zahlen dann nicht mehr
fortlaufend sind), wohingegen die TCP-Variante stets korrekt
funktioniert.


TCP/TCP:

```
Exception in thread "main" java.net.BindException: Address already in use (Bind failed)
  at java.net.PlainSocketImpl.socketBind(Native Method)
  at java.net.AbstractPlainSocketImpl.bind(AbstractPlainSocketImpl.java:387)
  at java.net.ServerSocket.bind(ServerSocket.java:375)
  at java.net.ServerSocket.<init>(ServerSocket.java:237)
  at java.net.ServerSocket.<init>(ServerSocket.java:128)
  at TCPServer.main(TCPServer.java:10)
```

TCP/UDP: Kein Fehler

UDP/UDP:

```
Exception in thread "main" java.net.BindException: Address already in use (Bind failed)
  at java.net.PlainDatagramSocketImpl.bind0(Native Method)
  at java.net.AbstractPlainDatagramSocketImpl.bind(AbstractPlainDatagramSocketImpl.java:93)
  at java.net.DatagramSocket.bind(DatagramSocket.java:392)
  at java.net.DatagramSocket.<init>(DatagramSocket.java:242)
  at java.net.DatagramSocket.<init>(DatagramSocket.java:299)
  at java.net.DatagramSocket.<init>(DatagramSocket.java:271)
  at UDPServer.main(UDPServer.java:10)
```

Es kann immer nur ein Socket des selben Typs an den selben Port gebunden werden.

Client-Applikation mit Namen eines nicht existenten Rechners
gestartet:

```
$ java TCPClient nonExistentHostname
Exception in thread "main" java.net.UnknownHostException: nonExistentHostname
  at java.net.AbstractPlainSocketImpl.connect(AbstractPlainSocketImpl.java:184)
  at java.net.SocksSocketImpl.connect(SocksSocketImpl.java:392)
  at java.net.Socket.connect(Socket.java:589)
  at java.net.Socket.connect(Socket.java:538)
  at java.net.Socket.<init>(Socket.java:434)
  at java.net.Socket.<init>(Socket.java:211)
  at TCPClient.main(TCPClient.java:12)
$ java UDPClient nonExistentHostname
Exception in thread "main" java.net.UnknownHostException: nonExistentHostname: \
Name or service not known
  at java.net.Inet6AddressImpl.lookupAllHostAddr(Native Method)
  at java.net.InetAddress$2.lookupAllHostAddr(InetAddress.java:928)
  at java.net.InetAddress.getAddressesFromNameService(InetAddress.java:1323)
  at java.net.InetAddress.getAllByName0(InetAddress.java:1276)
  at java.net.InetAddress.getAllByName(InetAddress.java:1192)
  at java.net.InetAddress.getAllByName(InetAddress.java:1126)
  at java.net.InetAddress.getByName(InetAddress.java:1076)
  at UDPClient.main(UDPClient.java:28)
```

Client-Applikation mit Namen eines Rechners gestartet, auf dem der
Server nicht läuft:

```
$ java UDPClient someOtherHost # kein Fehler
$ java TCPClient someOtherHost
Exception in thread "main" java.net.ConnectException: Connection refused \
(Connection refused)
  at java.net.PlainSocketImpl.socketConnect(Native Method)
  at java.net.AbstractPlainSocketImpl.doConnect(AbstractPlainSocketImpl.java:350)
  at java.net.AbstractPlainSocketImpl.connectToAddress(AbstractPlainSocketImpl.java:206)
  at java.net.AbstractPlainSocketImpl.connect(AbstractPlainSocketImpl.java:188)
  at java.net.SocksSocketImpl.connect(SocksSocketImpl.java:392)
  at java.net.Socket.connect(Socket.java:589)
  at java.net.Socket.connect(Socket.java:538)
  at java.net.Socket.<init>(Socket.java:434)
  at java.net.Socket.<init>(Socket.java:211)
  at TCPClient.main(TCPClient.java:12)
```

Die Fehlerrate der UDP-Variante nimmt ab, wenn eine Wartezeit zwischen
den Sendeversuchen eingeführt wird.
