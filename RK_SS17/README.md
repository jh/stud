# Übungsaufgaben Rechnerkommunikation SS 2017
[![Build Status](https://drone.cubieserver.de/api/badges/jh/rk/status.svg)](https://drone.cubieserver.de/jh/rk) - [Artifacts](https://jh-rk.s3.amazonaws.com/list.html)

Homepage: http://www7.cs.fau.de/de/teaching/rechnerkommunikation-2017s/

* [Aufgabenblatt 1](Aufgabenblatt1/)
* [Aufgabenblatt 2](Aufgabenblatt2/)
* [Aufgabenblatt 3](Aufgabenblatt3/)
* [Aufgabenblatt 4](Aufgabenblatt4/)
* [Aufgabenblatt 5](Aufgabenblatt5/)
* [Aufgabenblatt 6](Aufgabenblatt6/)

Nutzername: `rk17`
Passwort: `siebenschichten!`

## LaTeX Hilfen

* [LaTeX bei WikiBooks](https://en.wikibooks.org/wiki/LaTeX)
* [LaTeX/Mathematics](https://en.wikibooks.org/wiki/LaTeX/Mathematics)
* [LaTeX/Algorithms](https://en.wikibooks.org/wiki/LaTeX/Algorithms)
* [LaTeX/Source Code](https://en.wikibooks.org/wiki/LaTeX/Source_Code_Listings)
