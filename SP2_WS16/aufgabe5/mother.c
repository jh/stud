/* sister.c */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <limits.h>
#include <unistd.h>

#include "cmdline.h"
#include "connection.h"

#define forever while(true)

#define DEFAULT_PORT 2016

static uint16_t get_port() {
    char* str = (char*) cmdlineGetValueForKey("port");
    char *t;
    unsigned long int port;

    if ( str && *str ) {
        errno = 0;
        port = strtoul( str, &t, 10 );

        if ( t != NULL
             || ( port == ULONG_MAX && errno == ERANGE ) /* outside unsigned long range? */
             || port > 65535 ) /* port numbers are unsigned 16-bit integers */
            port = DEFAULT_PORT;
    } else {
        port = DEFAULT_PORT;
    }

    return (uint16_t) port;
}

int main( int argc, char** argv ) {
    if ( cmdlineInit( argc, argv ) )
        perror("cmdlineinit"), exit(EXIT_FAILURE);

    if ( initConnectionHandler() )
        fprintf( stderr, "Usage: %s --wwwpath=<dir> [--port=<p>] [--bufsize=<b>] [--threads=<t>]\n", cmdlineGetProgramName() ),
            exit(EXIT_FAILURE);

    uint16_t port = get_port();
    #ifdef DEBUG
    printf("Listening on port %u\n", port);
    #endif

    int listensock;

    /* open socket */
    if ( ( listensock = socket( AF_INET6, SOCK_STREAM, 0 ) ) == -1 )
        perror("socket"),
            exit(EXIT_FAILURE);

    /* for immediate rebinding */
    int flag = 1;
    if ( setsockopt( listensock, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag) ) ) {
        perror("setsockopt");
    }

    /* name for socket */
    struct sockaddr_in6 name = {
        .sin6_family = AF_INET6,
        .sin6_port = htons( port ),
        .sin6_addr = in6addr_any,
    };

    /* bind name to socket */
    if ( bind( listensock, (struct sockaddr *) &name, sizeof(name) ) )
        perror("bind"),
            exit(EXIT_FAILURE);

    /* begin listening on initialized socket */
    if ( listen( listensock, SOMAXCONN ) )
        perror("listen"),
            exit(EXIT_FAILURE);

    /* accept connections */
    int clientsock;
    forever {
        /* wait for connection */
        if ( ( clientsock = accept( listensock, NULL, NULL ) ) == -1 ) {
            perror("accept");
            continue;
        }

        /* spawn new connection handler */
        handleConnection( clientsock, listensock );
    }

    exit(EXIT_SUCCESS);
}
