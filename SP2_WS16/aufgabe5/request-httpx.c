#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fnmatch.h>

#include "dirlisting.h"
#include "request.h"
#include "cmdline.h"
#include "i4httools.h"

#define BUF_SIZE 4096
#define MAX_LINE_LENGTH 8192
#define INDEX_FILE "index.html"

static char *indexFile(const char *path) {
    char *index;
    size_t len;

    if ( ! path )
        return NULL;

    len = strlen(path) + strlen(INDEX_FILE) + 1;

    if ( ! ( index = malloc( sizeof(char) * len ) ) ) {
        return NULL;
    }

    if ( snprintf( index, len, "%s%s", path, INDEX_FILE ) < 0 ) {
        free(index);
        return NULL;
    }

    return index;
}

static int matchExtension(const char *file, const char *ext) {
    char *p;

    return ( ( p = strrchr( file, '.' ) ) && ( strcmp( &p[1], ext ) == 0 ) );
}

// returns 0 for hidden files
static int hidden(const struct dirent *e) {
    return e->d_name[0] != '.';
}

static void indexDirectory(FILE *tx, const char *path) {
    struct dirent **files;

    int n = scandir( path, &files, hidden, alphasort );
    if (n < 0) {
        perror("scandir");
        httpInternalServerError( tx, path );
    } else {
        httpOK(tx);
        dirlistingBegin( tx, path );

        /* iterate over all entries */
        while (n--) {
            dirlistingPrintEntry( tx, path, files[n]->d_name );
            free( files[n] );
        }

        free( files );
        dirlistingEnd( tx );
    }

}

static void executePerlScript(FILE *tx, char *path) {

    /* construct argv array */
    char *argv[2];
    argv[0] = path;
    argv[1] = NULL;

    pid_t pid = fork();
    if (pid == -1) {
        httpInternalServerError(tx, &path[1]);
    } else if (pid == 0) {

        if ( dup2( fileno(tx), STDOUT_FILENO ) == -1 ) {
            perror("dup2");
            exit(EXIT_FAILURE);
        }

        execvp(argv[0], argv);
    } else {
#ifdef DEBUG
        printf( "Spawned script %s with PID %d\n", path, pid );
#endif
    }

}

static void serveFile(FILE *tx, const char *file) {
    FILE *f;
    char s[BUF_SIZE];

    if ( ! ( f = fopen( file, "r" ) ) ) {
        perror("fopen");
        httpInternalServerError( tx, file );
        return;
    }

    while ( errno = 0, fgets( s, BUF_SIZE-1, f ) ) {
        if ( fputs( s, tx ) == EOF )
            perror("fputs");
    }
    if ( errno )
        perror("fgets");

    if ( fclose(f) )
        perror("fclose");

    return;
}

int initRequestHandler(void) {
    const char* wwwpath = cmdlineGetValueForKey("wwwpath");

    if ( ! wwwpath )
        return -1;

    /* switch to webroot */
    if ( chdir(wwwpath) )
        perror("chdir"),
            exit(EXIT_FAILURE);

    /* no zombies ! */
    struct sigaction sa = {
        .sa_flags = SA_NOCLDWAIT | SA_NOCLDSTOP,
        .sa_handler = SIG_DFL,
    };
    sigemptyset(&sa.sa_mask);
    if ( sigaction( SIGCHLD, &sa, NULL ) ) {
        perror("sigaction");
        exit(EXIT_FAILURE);
    }

    return 0;
}

void handleRequest(FILE *rx, FILE *tx) {
    char LINE_BUFFER[MAX_LINE_LENGTH+1];
    char *method, *uri, *version;

    if ( ! fgets( LINE_BUFFER, MAX_LINE_LENGTH, rx ) ) {
        perror("fgets");
        return;
    }

#ifdef DEBUG
    printf("Read line: %s", LINE_BUFFER);
#endif

    char *saveptr;
    method = strtok_r( LINE_BUFFER, " ", &saveptr );
    uri = strtok_r( NULL, " ", &saveptr );
    version = strtok_r( NULL, " ", &saveptr );

#ifdef DEBUG
    printf("Method: %s, URI: %s, Version: %s", method, uri, version);
#endif

    if ( ! method ) {
        httpBadRequest(tx);
        return;
    } else if ( strcmp ( "GET", method ) != 0 ) {
        /* actually 501 Not Implemented */
        httpBadRequest(tx);
        return;
    }

    if ( ! uri ) {
        httpNotFound( tx, uri );
        return;
    } else if ( checkPath( uri ) < 0 ) {
        httpForbidden( tx, uri );
        return;
    }

    if ( ! version ) {
        httpBadRequest(tx);
        return;
    } else if ( strncmp( "HTTP/1.0", version, 8 ) != 0
                && strncmp( "HTTP/1.1", version, 8 ) != 0 ) {
        /* actually 505 HTTP Version Not Supported */
        httpBadRequest(tx);

        return;
    }

    /* make uri relative by prepending a dot */
    char reluri[MAX_LINE_LENGTH];
    sprintf( reluri, uri[0] == '/' ? ".%s" : "./%s", uri );

#ifdef DEBUG
    printf("reluri: _%s_\n", reluri);
#endif

    /* check if file or directory */
    struct stat status;
    if ( stat( reluri, &status ) == -1 ) {
        perror("stat");
        httpNotFound( tx, uri );
        return;
    }

    if ( S_ISREG( status.st_mode ) ) {
        if ( (status.st_mode & S_IXUSR) == S_IXUSR
             && matchExtension( reluri, "pl" ) ) {
            executePerlScript( tx, reluri );
        } else {
            httpOK(tx);
            serveFile( tx, reluri );
        }

    } else if ( S_ISDIR( status.st_mode ) ) {

        /* if uri does not end with trailing slash redirect */
        size_t end = strlen(reluri)-1;
        if ( reluri[end] != '/' ) {
            reluri[++end] = '/';
            reluri[++end] = '\0';
            httpMovedPermanently( tx, &reluri[1] );
            return;
        }

        /* generate path to index file */
        char *index = indexFile(reluri);

        /* check for index.html */
        if ( access( index, R_OK ) == -1 ) {
            /* no index present, generate one */
            indexDirectory( tx, reluri );
        } else {
            httpOK(tx);
            serveFile( tx, index );
        }
        free(index);

    } else {
        /* neither regular file nor directory */
        httpNotFound(tx, uri);
    }

    return;
}
