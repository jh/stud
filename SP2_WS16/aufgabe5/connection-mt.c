#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <limits.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>

#include "cmdline.h"
#include "connection.h"
#include "jbuffer.h"
#include "request.h"

#define forever while(1)

#define DEFAULT_BUF_SIZE 8
#define DEFAULT_THREADS 4

static BNDBUF *BUFFER;

static unsigned long int get_bufsize() {
    char* str_bufsize = (char*) cmdlineGetValueForKey("bufsize");
    char *t;
    unsigned long int bufsize;

    if ( str_bufsize && *str_bufsize ) {
        errno = 0;
        bufsize = strtoul( str_bufsize, &t, 10 );

        if ( t != NULL
             || ( bufsize == ULONG_MAX && errno == ERANGE ) ) /* outside unsigned long range? */
            bufsize = DEFAULT_BUF_SIZE;
    } else {
        bufsize = DEFAULT_BUF_SIZE;
    }

    return bufsize;
}

static unsigned long int get_threads() {
    char* str = (char*) cmdlineGetValueForKey("threads");
    char *t;
    unsigned long int threads;

    if ( str && *str ) {
        errno = 0;
        threads = strtoul( str, &t, 10 );

        if ( t != NULL
             || ( threads == ULONG_MAX && errno == ERANGE ) ) /* outside unsigned long range? */
            threads = DEFAULT_THREADS;
    } else {
        threads = DEFAULT_THREADS;
    }

    return threads;
}

static void *worker(void *parameters) {
    int sockrx, socktx;
    FILE *rx, *tx;

    forever {
        socktx = bbGet(BUFFER);

        if ( ( sockrx = dup( socktx ) ) == -1 ) {
            perror("dup");
            continue;
        }

        if ( fcntl( sockrx, F_SETFD, FD_CLOEXEC) == -1 ) {
            perror("fcntl");
            continue;
        }

        if ( ! ( rx = fdopen( sockrx, "r" ) ) ) {
            perror("fdopen");
            continue;
        }

        if ( ! ( tx = fdopen( socktx, "w" ) ) ) {
            perror("fdopen");
            continue;
        }

        handleRequest( rx, tx );

        if ( fclose(tx) )
            perror("fclose");

        if ( fclose(rx) )
            perror("fclose");

    }



    return NULL;
}

int initConnectionHandler(void) {
    unsigned long int bufsize, threads;
    pthread_t tid;
    struct sigaction action = {
        .sa_handler = SIG_IGN,
    };

    /* ignore SIGPIPEs */
    if ( sigaction( SIGPIPE, &action, NULL ) ) {
        perror("sigaction");
        exit(EXIT_FAILURE);
    }

    bufsize = get_bufsize();
    threads = get_threads();

    /* create bounded buffer */
    BUFFER = bbCreate(bufsize);

    /* spawn threads */
    int errno_bak = errno;
    for( unsigned long int i = 0; i < threads; i++ ) {
        if ( ( errno = pthread_create( &tid, NULL, worker, NULL ) ) ) {
            perror("pthread_create");
            bbDestroy(BUFFER);
            return -1;
        }
    }
    errno = errno_bak;

    if ( initRequestHandler() )
        return -1;

    return 0;
}

void handleConnection(int clientSock, int listenSock) {

    /* add socket descriptor to bounded buffer */
    bbPut(BUFFER, clientSock);

    return;
}
