#include "plist.h"
#include "shellutils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define BLOCK_SIGCHLD sigset_t mask, oldmask;   \
    sigemptyset( &mask );                       \
    sigaddset( &mask, SIGCHLD );                \
    sigprocmask( SIG_BLOCK, &mask, &oldmask )
#define UNBLOCK_SIGCHLD sigprocmask( SIG_SETMASK, &oldmask, NULL )


static void printProcEvent(pid_t pid, const char cmdLine[], int event) {
	fprintf(stderr, "[%d] \"%s\" ", pid, cmdLine);
	if (WIFEXITED(event) != 0)
		fprintf(stderr, "exited with status %d", WEXITSTATUS(event));
	else if (WIFSIGNALED(event) != 0)
		fprintf(stderr, "terminated by signal %d", WTERMSIG(event));
	else if (WIFSTOPPED(event) != 0)
		fprintf(stderr, "stopped by signal %d", WSTOPSIG(event));
	else if (WIFCONTINUED(event) != 0)
		fputs("continued", stderr);
	putc('\n', stderr);
}


static void die(const char message[]) {
	perror(message);
	exit(EXIT_FAILURE);
}

static void sigchld_handler(int signum) {
    int status, errno_bak;
    pid_t cld_pid;

    errno_bak = errno;

    if ( ( cld_pid = waitpid( 0 , &status, WNOHANG ) ) == -1 ) {
#ifdef DEBUG
        perror("waitpid");
#endif
        errno = errno_bak;
        return;
    }

    /* iterate over all possible dead children */
    while ( ( cld_pid = waitpid( 0, &status, WNOHANG ) ) ) {
        if ( cld_pid == -1 ) {
            perror("waitpid");
            errno = errno_bak;
            return;
        }

        plistNotifyEvent( cld_pid, status );
        /* no error checking required,
           if the process was not a background process it won't be in plist,
           hence plistNotifyEvent may return -1 (PID not in list) */
    }


    errno = errno_bak;

    return;
}



static void changeCwd(char *argv[]) {
	if (argv[1] == NULL || argv[2] != NULL) {
		fprintf(stderr, "Usage: %s <dir>\n", argv[0]);
		return;
	}
	if (chdir(argv[1]) != 0)
		perror(argv[0]);
}


static int printProcInfo(const ProcInfo *info) {
	fprintf(stderr, "[%d] \"%s\"\n", info->pid, info->cmdLine);
	return 0;
}

static int printBackgroundInfo(const ProcInfo *info, int event) {
    printProcEvent( info->pid, info->cmdLine, event );

    BLOCK_SIGCHLD;
    if ( plistRemove(info->pid) ) {
        fprintf( stderr, "Background process with PID %d not found in plist\n", info->pid );
    }
    UNBLOCK_SIGCHLD;
    return 0;
}


static void printJobs(char *argv[]) {
	if (argv[1] != NULL) {
		fprintf(stderr, "Usage: %s\n", argv[0]);
		return;
	}
    BLOCK_SIGCHLD;
	plistIterate(printProcInfo);
    UNBLOCK_SIGCHLD;
}


static void execute(ShCommand *cmd) {
    int in_fd = -1;
    int out_fd = -1;

	pid_t pid = fork();
	if (pid == -1)
		die("fork");

	if (pid == 0) { // Child process

        /* catch SIGINT if cmd->background is false */
        if ( cmd->background == false ) {
            sigset_t set;
            sigemptyset( &set );
            sigaddset( &set, SIGINT );
            sigprocmask( SIG_UNBLOCK, &set, NULL );
        }

        if ( cmd->inFile ) {
            if ( ( in_fd = open( cmd->inFile, O_RDONLY ) ) == -1 )
                die("open");

            if ( dup2( in_fd, STDIN_FILENO ) == -1 )
                die("dup2");
        }

        if ( cmd->outFile ) {
            if ( ( out_fd = creat( cmd->outFile, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH ) ) == -1 )
                die("creat");

            if ( dup2( out_fd, STDOUT_FILENO ) == -1 )
                die("dup2");
        }

        if ( close(in_fd) )
            perror("close");

        if ( close(out_fd) )
            perror("close");


		// Execute the desired program
		execvp(cmd->argv[0], cmd->argv);

        if ( in_fd != -1 )
            if ( close(in_fd) ) perror("close");

        if ( out_fd != -1 )
            if ( close(out_fd) ) perror("close");

		die(cmd->argv[0]);

	} else {		// Parent process

		if (cmd->background == true) { // Child runs in background
            BLOCK_SIGCHLD;
			const ProcInfo *info = plistAdd(pid, cmd->cmdLine, cmd->background);
            UNBLOCK_SIGCHLD;
			if (info == NULL)
				die("plistAdd");
			printProcInfo(info);
		} else { // Child runs in foreground
            /* block all signals */
            sigset_t mask, oldmask;
            sigfillset( &mask );
            sigprocmask( SIG_BLOCK, &mask, &oldmask );

            sigsuspend( &mask );

            UNBLOCK_SIGCHLD;
		}
	}
}

int kill_proc(const ProcInfo *pi) {
    if ( ! pi )
        return -1;

    if ( kill( pi->pid, SIGTERM ) == -1 )
        perror("kill");

    return 0;
}

int nuke_pid(pid_t pid) {

    if ( kill( (pid_t) pid, SIGTERM ) ) {
        perror("kill");
        return -1;
    }

    BLOCK_SIGCHLD;

#ifdef DEBUG
    printf("Sleeping for one second\n");
#endif

    unsigned s;
    if ( ( s = sleep(1) ) )
        fprintf( stderr, "Error: returned early from sleep, s: %u\n", s );

#ifdef DEBUG
    printf("Slept one second\n");
#endif

    UNBLOCK_SIGCHLD;

    return 0;
}

int nuke(const char* process) {
    unsigned long int pid;
    char *endptr = NULL;

    errno = 0;
    pid = strtoul( process, &endptr, 10 );

    if ( ! endptr
         || ( pid == ULONG_MAX && errno == ERANGE )
         || ( pid == 0 && errno == EINVAL ) ) {
        perror("strtoul");
        return -1;
    }

    return nuke_pid(pid);
}


int main(void) {
    /* set signal handler for SIGCHLD */
    struct sigaction sa;
    sa.sa_flags = SA_RESTART | SA_NOCLDSTOP;
    sa.sa_handler = sigchld_handler;
    sigemptyset(&sa.sa_mask);
    sigaction( SIGCHLD, &sa, NULL );

    /* ignore SIGINT */
    sa.sa_flags = SA_RESTART;
    sa.sa_handler = SIG_IGN;
    sigemptyset(&sa.sa_mask);
    sigaction( SIGINT, &sa, NULL );

    for (;;) {

        /* print dead background processes */
        int r;
        BLOCK_SIGCHLD;
        if ( ( r = plistHandleEvents( printBackgroundInfo ) ) ) {
            fprintf( stderr, "plistHandleEvents: %d\n", r );
        }
        UNBLOCK_SIGCHLD;

        // Show prompt
        shPrompt();

        // Read command line
        char cmdLine[MAX_CMDLINE_LEN];
        if (fgets(cmdLine, sizeof(cmdLine), stdin) == NULL) {
            if (ferror(stdin) != 0)
                die("fgets");
            break;
        }

        // Parse command line
        ShCommand *cmd = shParseCmdLine(cmdLine);
        if (cmd == NULL) {
            perror("shParseCmdLine");
            continue;
        }

        // Execute command
        if (cmd->parseError != NULL) {
            if (strlen(cmdLine) > 0)
                fprintf(stderr, "Syntax error: %s\n", cmd->parseError);
        } else if (strcmp("cd", cmd->argv[0]) == 0) {
            changeCwd(cmd->argv);
        } else if (strcmp("jobs", cmd->argv[0]) == 0) {
            printJobs(cmd->argv);
        } else if ( strcmp("nuke", cmd->argv[0] ) == 0 ) {
            if ( cmd->argv[1] == NULL ) {
                BLOCK_SIGCHLD;
                if( plistIterate(kill_proc) )
                    fprintf( stderr, "Error: did not walk through entire list\n" );
                UNBLOCK_SIGCHLD;
            } else if ( cmd->argv[2] == NULL ) {
                nuke( cmd->argv[1] );
            } else {
                fprintf( stderr, "Usage: nuke [pid]\n" );
            }

        } else {
            execute(cmd);
        }
    }

    putchar('\n');
    return EXIT_SUCCESS;
}
