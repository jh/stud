#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "request.h"
#include "cmdline.h"
#include "i4httools.h"

static const size_t MAX_LINE_LENGTH = 8192;

int initRequestHandler(void) {
    const char* wwwpath = cmdlineGetValueForKey("wwwpath");

    if ( ! wwwpath )
        return -1;

    /* switch to webroot */
    if ( chdir(wwwpath) )
        perror("chdir"),
            exit(EXIT_FAILURE);

    return 0;
}

void handleRequest( FILE *rx, FILE *tx ) {
    char LINE_BUFFER[MAX_LINE_LENGTH+1];
    char *method, *uri, *version;
    FILE *f;

    if ( ! fgets( LINE_BUFFER, MAX_LINE_LENGTH, rx ) ) {
        perror("fgets");
        return;
    }

#ifdef DEBUG
    printf("Read line: %s\n", LINE_BUFFER);
#endif

    method = strtok( LINE_BUFFER, " " );
    uri = strtok( NULL, " " );
    version = strtok( NULL, " " );

#ifdef DEBUG
    printf("Method: %s, URI: %s, Version: %s\n", method, uri, version);
#endif

    if ( ! method ) {
        httpBadRequest(tx);
        return;
    } else if ( strcmp ( "GET", method ) != 0 ) {
        /* actually 501 Not Implemented */
        httpBadRequest(tx);
        return;
    }

    if ( ! uri ) {
        httpNotFound( tx, uri );
        return;
    } else if ( checkPath( uri ) < 0 ) {
        httpForbidden( tx, uri );
        return;
    }

    if ( ! version ) {
        httpBadRequest(tx);
        return;
    } else if ( strncmp( "HTTP/1.0", version, 8 ) != 0
                && strncmp( "HTTP/1.1", version, 8 ) != 0 ) {
        /* actually 505 HTTP Version Not Supported */
        httpBadRequest(tx);

        return;
    }

    char *relpath = uri[0] == '/' ? &uri[1] : uri;

    struct stat status;
    if ( lstat( relpath, &status ) == -1 ) {
        perror("stat");
        httpNotFound( tx, uri );
        return;
    }

    if ( ! S_ISREG( status.st_mode )
        ||  ! ( f = fopen( relpath, "r" ) ) ) {
        perror("fopen");
        httpNotFound( tx, uri );
        return;
    }

    /* ok */
    httpOK(tx);

    int c;
    while ( errno = 0, ( c = fgetc(f) ) != EOF ) {
        if ( fputc( c, tx ) == EOF )
            perror("fputc");
    }
    if ( errno )
        perror("fgetc");

    if ( fclose(f) )
        perror("fclose");

    /* force writing of data to socket */
    if ( fflush(tx) )
        perror("fflush");

    return;
}
