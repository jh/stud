#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include "connection.h"
#include "request.h"

int initConnectionHandler(void) {
    struct sigaction action = {
        .sa_handler = SIG_IGN,
        .sa_flags = SA_NOCLDWAIT,
    };

    if ( sigaction( SIGCHLD, &action, NULL ) )
        perror("sigaction"),
            exit(EXIT_FAILURE);

    if ( initRequestHandler() )
        return -1;

    return 0;
}

void handleConnection(int clientSock, int listenSock) {
    pid_t p = fork();

    if ( p == -1 ) {
        perror("fork");
        /*
          no EXIT_FAILURE here because we might have
          hit our process limit _SC_CHILD_MAX, however
          clients may close their connections (and therefore
          free up processes), then the server can continue
          to operate
        */

        /*
          in general the connection -> fork model is
          very vulnerable to DDOS and Slowloris attacks
        */
    } else if ( p == 0 ) {
        /* child */

        int sockrx, socktx = clientSock;
        FILE *rx, *tx;

        if ( close(listenSock) )
            perror("close(listenSock)"); /* not fatal */

        if ( ( sockrx = dup( socktx ) ) == -1 )
            perror("dup"),
                exit(EXIT_FAILURE);

        if ( ! ( rx = fdopen( sockrx, "r" ) ) )
            perror("fdopen"),
                exit(EXIT_FAILURE);

        if ( ! ( tx = fdopen( socktx, "w" ) ) )
            perror("fdopen"),
                exit(EXIT_FAILURE);

        handleRequest( rx, tx );

        if ( fclose(tx) )
            perror("fclose");

        if ( fclose(rx) )
            perror("fclose");

        exit(EXIT_SUCCESS);

    } else {
        /* parent */

#ifdef DEBUG
        printf("Child PID: %d\n", p);
#endif

        if ( close(clientSock) )
            perror("close(clientSock)"); /* not fatal */
    }

    return;
}
