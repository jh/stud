#include <errno.h>
#include <stdlib.h>
#include <pthread.h>

#include "sem.h"

struct SEM {
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    volatile int value;
};

#define M &(sem->mutex)
#define C &(sem->cond)

SEM *semCreate(int initVal) {
    SEM *sem;
    int errno_sv = errno;
    if ( ! ( sem = malloc( sizeof(SEM) ) ) )
        return NULL;

    if ( ( errno = pthread_mutex_init( M, NULL ) ) ) {
        free(sem);
        return NULL;
    }

    if ( ( errno = pthread_cond_init( C, NULL ) ) ) {
        pthread_mutex_destroy( M );
        free(sem);
        return NULL;
    }

    sem->value = initVal;
    errno = errno_sv;
    return sem;
}

void semDestroy(SEM *sem) {
    if ( ! sem ) return;
    pthread_mutex_destroy( M );
    pthread_cond_destroy( C );
    free(sem);
}

void P(SEM *sem) {
    if ( ! sem ) return;
    pthread_mutex_lock( M );
    while ( sem->value <= 0 )
        pthread_cond_wait( C, M );
    (sem->value)--;
    pthread_mutex_unlock( M );
}

void V(SEM *sem) {
    if ( ! sem ) return;
    pthread_mutex_lock( M );
    (sem->value)++;
    pthread_cond_broadcast( C );
    pthread_mutex_unlock( M );
}
