#include <errno.h>
#include <stdlib.h>

#include "jbuffer.h"
#include "sem.h"

struct BNDBUF {
    volatile int *in;
    volatile int *out;
    size_t size;
    SEM *space;
    SEM *elements;
    int *buf;
};

BNDBUF *bbCreate(size_t size) {
    BNDBUF *bb;
    if ( ! size )
        return NULL;

    if ( ! ( bb = calloc( sizeof(BNDBUF), 1 ) ) )
        return NULL;

    bb->size = size;

    if ( ! ( bb->buf = malloc( sizeof(int) * size ) ) ) {
        bbDestroy(bb);
        return NULL;
    }

    bb->in = bb->buf;
    bb->out = bb->buf;

    if ( ! ( bb->elements = semCreate(0) ) ) {
        bbDestroy(bb);
        return NULL;
    }

    if ( ! ( bb->space = semCreate(size) ) ) {
        bbDestroy(bb);
        return NULL;
    }

    return bb;
}

void bbDestroy(BNDBUF *bb) {
    if ( ! bb ) return;
    semDestroy(bb->space);
    semDestroy(bb->elements);
    free(bb->buf);
    free(bb);
}

#define PP(b, p, s)  b + ( p - b + 1 ) % s

void bbPut(BNDBUF *bb, int value) {
    if ( ! bb ) return;

    P(bb->space);

    volatile int *old;
    do
        old = bb->in;
    while ( ! __sync_bool_compare_and_swap( &(bb->in), old, PP(bb->buf, bb->in, bb->size) ) );

    V(bb->elements);
    *(old) = value;
}

int bbGet(BNDBUF *bb){
    if ( ! bb ) return 0;

    P(bb->elements);

    volatile int *old;
    int value;
    do {
        old = bb->out;
        value = *(old);
    }
    while ( ! __sync_bool_compare_and_swap( &(bb->out), old, PP(bb->buf, bb->out, bb->size) ) );

    V(bb->space);

    return value;
}
