## SP2 WS16

Lösungen für Systemprogrammierung 2 aus dem Wintersemester 2016 / 2017.

## Aufgaben:
https://www4.cs.fau.de/Lehre/WS16/V_SP2/Uebung/aufgaben.shtml

- [aufgabe1 - snail](aufgabe1/)
- [aufgabe2 - sister](aufgabe2/)
- [aufgabe3 - rush](aufgabe3/)
- [aufgabe4 - jbuffer](aufgabe4/)
- [aufgabe5 - mother](aufgabe5/)

## Verweise
[Beej's Guide to Network Programming](https://beej.us/guide/bgnet/output/html/multipage/index.html) ist sehr relevant für das zweite Semester Systemprogrammierung und erhält auch viele nützliche Beispiele zum Thema Netzwerkprogrammierung in C.
