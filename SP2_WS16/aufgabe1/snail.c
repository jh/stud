/* snail - simple natty mail */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <limits.h>
#include <pwd.h>

#define HOST "faui03.cs.fau.de"
#define PORT "25"

#define PRINT_SYNTAX_ERROR fprintf( stderr, "Usage: %s [-s subject] <address>\n", argv[0] )

/* maximum line length supported by system */
static long MAX_LINE_LENGTH;
static long MAX_BUFFER_LENGTH;

struct mail {
    char* fullname;
    char* username;
    char* fqdn;
    char* to;
    char* subject;
    char** body;
    int body_lines;
};

static struct mail MAIL;

/* copies string from source into new buffer and returns its address */
static char* cpstr( const char* src ) {
    size_t l;
    char* dest;
    size_t i;

    if( ! src )
        return NULL;

    l = strlen(src)+1;
    if ( ! ( dest = malloc( l * sizeof(char) ) ) )
        return NULL;

    /* simple implementation of strcpy, see strncpy(3) */
    for( i = 0; i < l && src[i] != '\0'; i++ )
        dest[i] = src[i];

    for ( ; i < l; i++ )
        dest[i] = '\0';

    return dest;
}

/* reads multiple lines from stdin */
/* lines are not terminated by newline and are not dot-escaped */
/* returns total lines read or -1 on error */
static int read_input( char ***output ) {
    /* char *buffer = NULL; */
    char **lines = NULL;
    char **tmp = NULL;
    int s = 0;

    char* lineptr;
    size_t n;
    ssize_t r;

    while ( lineptr = NULL, n = 0, ( r = getline( &lineptr, &n, stdin ) ) != -1 ) {
        if ( lineptr[r] == '\r' || lineptr[r] == '\n' )
            lineptr[r] = '\0';

        if ( lineptr[r-1] == '\r' || lineptr[r-1] == '\n' )
            lineptr[r-1] = '\0';

        if ( ! ( tmp = realloc( lines, (s+1) * sizeof(char*) ) ) ) {
            perror("realloc");
            for ( int i = s; i > 0; i-- )
                free( lines[i] );
            free(lines);
            return -1;
        }

        lines = tmp;
        lines[s++] = lineptr;
    }

    free(lineptr);
    *output = lines;
    return s;
}

static int get_user_info( char** username, char** fullname ) {
    struct passwd *u;
    char* buf;

    if ( ! ( u = getpwuid( getuid() ) ) ) {
        perror("getpwuid");
        return 1;
    }

    if ( ! ( *username = cpstr( u->pw_name ) ) ) {
        perror("malloc");
        return 1;
    }

    if ( ! ( buf = cpstr( u->pw_gecos ) ) ) {
        perror("malloc");
        return 1;
    }

    *fullname = strtok( buf, "," );

    return 0;
}

static char* get_fqdn() {
    char h[HOST_NAME_MAX+1];
    struct addrinfo *result;
    int r = 0;
    char *fqdn = NULL;

    if ( gethostname( h, HOST_NAME_MAX ) == -1 ) {
        perror("gethostname");
        return NULL;
    }

    struct addrinfo hints = {
        .ai_socktype = SOCK_STREAM,
        .ai_family = AF_UNSPEC,
        .ai_flags = AI_CANONNAME,
    };

    if ( ( r = getaddrinfo( h, NULL, &hints, &result ) ) ) {
        fprintf( stderr, "getaddrinfo: %s\n", gai_strerror(r) );
        freeaddrinfo(result);
        return NULL;
    }

    if( ! ( fqdn = cpstr( result->ai_canonname ) ) )
        perror("malloc");

    freeaddrinfo(result);
    /* due to internal implementation of getaddrinfo/freeaddrinfo
       this entry is not freed */

    return fqdn;
}


int con( const char* host, const char* service ) {
    int r, sockfd;
    struct addrinfo *result, *i;
    struct addrinfo hints = {
        .ai_family = AF_INET6,
        .ai_socktype = SOCK_STREAM,
    };

    if ( ( r = getaddrinfo( host, service, &hints, &result ) ) ) {
        fprintf( stderr, "getaddrinfo: %s\n", gai_strerror(r) );
        return -1;
    }

    /* loop through all results and try to connect */
    for ( i = result; i; i = i->ai_next ) {
        if ( ( sockfd = socket( i->ai_family, i->ai_socktype, i->ai_protocol ) ) == -1 ) {
            perror("socket");
            if ( close(sockfd) ) perror("close");
            continue;
        }

        if ( connect( sockfd, i->ai_addr, i->ai_addrlen) == -1 ) {
            perror("connect");
            if ( close(sockfd) ) perror("close");
            continue;
        }

        break;
    }

    freeaddrinfo(result);

    /* no connection possible */
    if ( ! i ) {
        fprintf( stderr, "con: No connection available\n" );
        return -1;
    }

    return sockfd;
}

#ifdef DEBUG
#define printDebug printf( "_%s_\n", line )
#else
#define printDebug
#endif

#define recvline if ( line = NULL, n = 0, ( r = getline( &line, &n, rx ) ) == -1 ) { \
        perror("getline"); free(line); return 1; }
#define sendline( ... ) if ( fprintf( tx, __VA_ARGS__ ) < 0 )   \
    { fprintf ( stderr, "fprintf: Error\n" ); return 1; }       \
    if ( fflush(tx) ) { perror("fflush"); return 1; }

int sendmail( const int sockfd, struct mail* m ) {
    char *line;
    int sockrx, socktx = sockfd;
    FILE *rx, *tx;
    size_t n;
    ssize_t r;

    /* duplicate socket */
    if( ( sockrx = dup( socktx ) ) == -1 ) {
        perror("dup");
        return 1;
    }

    /* open sockets */
    if ( ! ( rx = fdopen( sockrx, "r" ) ) ) {
        perror("fdopen");
        return 1;
    }
    if ( ! ( tx = fdopen( socktx, "w" ) ) ) {
        perror("fdopen");
        return 1;
    }

    /* 220 ... */
    recvline;
    printDebug;
    if( strncmp( line, "220 ", 4 ) ) {
        fprintf( stderr, "Error: Expected SMTP 220, got: %s\n", line );
        free(line);
        return 1;
    }
    free(line);

    /* HELO ... */
    sendline( "HELO %s\r\n", m->fqdn );

    /* 250 ... */
    recvline;
    printDebug;
    if ( strncmp( line, "250 ", 4 ) ) {
        fprintf( stderr, "Error: Expected SMTP 250, got: %s\n", line );
        free(line);
        return 1;
    }
    free(line);

    /* MAIL FROM: <username@fqdn> */
    sendline( "MAIL FROM: <%s@%s>\r\n", m->username, m->fqdn );

    /* 250 ... */
    recvline;
    printDebug;
    if ( strncmp( line, "250 ", 4 ) ) {
        fprintf( stderr, "Error: Expected SMTP 250, got: %s\n", line );
        free(line);
        return 1;
    }
    free(line);

    /* RCPT TO: <address> */
    sendline( "RCPT TO: <%s>\r\n", m->to );

    /* 250 ... */
    recvline;
    printDebug;
    if ( strncmp( line, "250 ", 4 ) ) {
        fprintf( stderr, "Error: Expected SMTP 250, got: %s\n", line );
        free(line);
        return 1;
    }
    free(line);

    /* DATA */
    sendline( "DATA\r\n" );

    /* 354 ... */
    recvline;
    printDebug;
    if ( strncmp( line, "354 ", 4 ) ) {
        fprintf( stderr, "Error: Expected SMTP 354, got: %s\n", line );
        free(line);
        return 1;
    }
    free(line);

    /* Content */
    sendline( "From: %s <%s@%s>\r\n", m->fullname, m->username, m->fqdn );

    sendline( "To: <%s>\r\n", m->to );

    if ( m->subject )
        sendline( "Subject: %s\r\n", m->subject );

    for ( int i = 0; i < m->body_lines; i++ ) {
        /* escape dot at beginning of line if necessary*/
        sendline( m->body[i][0] == '.' ? ".%s\r\n" : "%s\r\n", m->body[i] );
    }

    /* terminating dot */
    sendline( ".\r\n" );

    /* 250 ... */
    recvline;
    printDebug;
    if ( strncmp( line, "250 ", 4 ) ) {
        fprintf( stderr, "Error: Expected SMTP 250, got: %s\n", line );
        free(line);
        return 1;
    }
    free(line);

    sendline( "QUIT\r\n" );

    recvline;
    printDebug;
    if ( strncmp( line, "221 ", 4 ) ) {
        fprintf( stderr, "Error: Expected SMTP 221, got: %s\n", line );
        free(line);
        return 1;
    }

    free(line);

    if ( fclose(tx) )
        perror("fclose");

    if ( fclose(rx) )
        perror("fclose");

    return 0;
}

#define freemail                                                    \
    free(MAIL.fullname);                                            \
    free(MAIL.username);                                            \
    free(MAIL.fqdn);                                                \
    free(MAIL.to);                                                  \
    free(MAIL.subject);                                             \
    for(int i = 0; i < MAIL.body_lines; i++) free(MAIL.body[i]);    \
    free(MAIL.body);


int main( int argc, char** argv ) {
    int sockfd;
    int r;

    /* aquire global system parameters */
    MAX_LINE_LENGTH = sysconf(_SC_LINE_MAX);
    if( MAX_LINE_LENGTH == -1 )
        perror("sysconf"), exit(EXIT_FAILURE);
    MAX_BUFFER_LENGTH = sizeof(char) * ( MAX_LINE_LENGTH + 2 );

    /* evaluate command line arguments (address & subject) */
    switch(argc) {
    case 0:
    case 1:
    case 3:
    default:
        PRINT_SYNTAX_ERROR;
        exit(EXIT_FAILURE);
        break; /* should never be reached */
    case 2:
        if( ! (MAIL.to = cpstr( argv[1] ) ) )
            perror("malloc"), exit(EXIT_FAILURE);
        break;
    case 4:
        if( strcmp( argv[1], "-s" ) == 0 ) {
            if ( ! ( MAIL.subject = cpstr( argv[2] ) ) ) {
                perror("malloc");
                exit(EXIT_FAILURE);
            }
            if ( ! ( MAIL.to = cpstr( argv[3] ) ) ) {
                perror("malloc");
                freemail;
                exit(EXIT_FAILURE);
            }
        } else {
            PRINT_SYNTAX_ERROR;
            exit(EXIT_FAILURE);
        }
    }

    /* read mail body from stdin */
    if ( ( MAIL.body_lines = read_input( &(MAIL.body) ) ) == -1 ) {
        fprintf( stderr, "Error reading input\n" );
        freemail;
        exit(EXIT_FAILURE);
    }

#ifdef DEBUG
    printf( "Body: %d lines\n", MAIL.body_lines );
    for( int i = 0; i < MAIL.body_lines; i++ ) { printf( "%s\n", MAIL.body[i] ); }
#endif

    /* get FQDN */
    if( ! ( MAIL.fqdn = get_fqdn() ) ) {
        fprintf( stderr, "Error retrieving FQDN\n" );
        freemail;
        exit(EXIT_FAILURE);
    }

    if ( get_user_info( &(MAIL.username), &(MAIL.fullname) ) ) {
        fprintf( stderr, "Error retrieving user info\n" );
        freemail;
        exit(EXIT_FAILURE);
    }

#ifdef DEBUG
    printf(
        "MAIL\n Fullname: _%s_\n Username: _%s_\n FQDN: _%s_\n To: _%s_\n Subject: _%s_\n",
        MAIL.fullname,
        MAIL.username,
        MAIL.fqdn,
        MAIL.to,
        MAIL.subject
        );
#endif

    /* connect to server */
    if ( ( sockfd = con( HOST, PORT ) ) < 0 ) {
        freemail;
        exit(EXIT_FAILURE);
    }

    /* work with socketfd */
    r = sendmail( sockfd, &MAIL );

    freemail;

    ! r ? exit(EXIT_SUCCESS) : exit(EXIT_FAILURE);
}
