# SP Cheatsheet


## Betriebssystem (Operating System)
 Eine Menge von Programmen, die:
 - Programme, Anwendungen oder BenutzerInnen assistieren sollen
 - die Ausführung von Programmen überwachen und steuern
 - den Rechner für eine Anwendungsklasse betreiben
 - eine abstrakte Maschine implementieren

Verwaltet die Betriebsmittel eines Rechensystems:
 - kontrolliert die Vergabe der (Software/Hardware) Ressourcen
 - verteilt diese ggf. gerecht an die mitbenutzenden Rechenprozesse

Definiert sich nicht über die Architektur, sondern über Funktionen



## Schichtenfolge in Rechensystemen

```text
    +---------------------------------------+
 5  | problemorientierte Programmiersprache |
    +---------------------------------------+
               | Kompilation (Übersetzung)
               v (z.B. gcc)
    +---------------------------------------+
 4  |      Assembliersprachenebene          |
    +---------------------------------------+
               | Assemblierung und Bindung
               v (z.B. as, ld)
    +---------------------------------------+
 3  |        Maschinenprogrammebene         |
    +---------------------------------------+
Software       | Teilinterpretation von
=============  | Systemaufrufen durch OS
Firm/Hardware  v (z.B. UNIX)
    +---------------------------------------+
 2  |          Befehlssatzebene             |
    +---------------------------------------+
               | Interpretation
               v (z.B. x86-64)
    +---------------------------------------+
 1  |         Mikroarchitekturebene         |
    +---------------------------------------+
               | Ausführung
               v (z.B. ARM)
    +---------------------------------------+
 0  |         digitale Logikebene           |
    +---------------------------------------+
```
Schichten der Ebenen 3-5 repräsentieren **virtuelle Maschinen**, die auf eine **reale Maschine** (Ebenen 2-0) abzubilden sind.



## Systemaufrufschnittstelle
"Grenzübergangsstelle" **Aufrufstumpf** erscheint als normaler *Funktionsaufruf*, bewirkt jedoch einen *Moduswechsel*.

Systemaufrufe sind *Prozedurenaufrufe* um *Prozessdomäne* (kontrolliert) zu überwinden.



## Speicherverwaltung
Arbeitsspeicher hat mehr Kapazität als der Hauptspeicher (Begrifflich sind Arbeits- und Hauptspeicher verschiedene Dinge!)
- Hauptspeicher ist Zwischenspeicher → Vordergrundspeicher
- ungenutzte Bestände im Massenspeicher → Hintergrundspeicher


### Seitennummerierung (paging)
Jede Adresse wird interpretiert als Tupel `Ap = (p, o)`, wobei

 Oktettnummer o = [0, 2^i − 1], mit 9 ≤ i ≤ 30

 Seitennummer p = [0, 2^n−i − 1], mit 32 ≤ n ≤ 64

Übliche Einkomponentenadresse -> eindimensionaler Adressraum (d.h. Oktetts oder Worte in einer Dimension aufgereiht)


### Segmentierung (segmentation)
Jede Adresse ist repräsentiert als Paar As = (s, a), wobei

 Segmentname s = [0, 2m − 1], mit 12 ≤ m ≤ 18

 Adresse a = [0, 2n − 1], mit 32 ≤ n ≤ 64

Zweikomponentenadresse -> zweidimensionaler Adressraum (d.h. Segmente in der ersten Segmentinhalte in der zweiten Dimension)


### Seitennummerierte Segmentiertung (paged segmentation)
Jedes Segment ist seitennummeriert ausgelegt, d.h. a in As entspricht Ap

Die **Ladestrategie** bestimmt, _wann_ ein Datum im Hauptspeicher liegt und wie es dort hingebracht wird:
- Einzelanforderung (*demand paging*) - wartet auf Zugriffsfehler
- Vorausladen (*anticipatory*) - versucht Zugriffsfehler vorzubeugen

Die **Ersetzungsstrategie** bestimmt, _welches_ Datum seinen Platz im Hautpspeicher freimachen muss
- Globale Verfahren (Alle residenten Seiten aller Prozesse): FIFO, LRU => *Seitenflattern* möglich
- Lokale Verfahren (Nur residente Seiten des Prozesses, der Zugriffsfehler verursacht hat): Freiseitenpufer, Arbeitsmenge => *hohe Unkosten* (overhead)


```
#include <stdlib.h>
void *malloc(size_t size);
void free(void *ptr);
void *calloc(size_t nmemb, size_t size);
void *realloc(void *ptr, size_t size);
```



## Adressraum


### Realer Adressraum
Durch Prozessor definierter Wertevorrat von Adressen, wobei nicht jede Adresse gültig sein muss.


### Logischer Adressraum
Durch Programm definierte Wertevorrat von Adressen (Teil des realen Adressraums) der einem Prozess zugeteilt wird. Jede Adresse ist gültig, der logische Adressraum enthält keine Lücken.


### Virtueller Adressraum
Der virtuelle Adressraum ist gleich dem logischen Adressraum, wobei nicht jede Adresse auf ein im Hauptspeicher liegendes Datum abbildet.
(Aktion → Zugriffsfehler → synchrone Unterbrechung (*trap*) → Einlagerung → Wiederholung der Aktion)


### Interne Fragmentierung
Verschnitt, der entsteht wenn reservierte Speicherblöcke nicht vollständig ausgenutzt werden.

=> **Verschwendung** (unvermeidbar)


### Externe Fragmentierung
Zerstückelung des Adressraums (kein Loch ist groß genug, Summe der Löcher jedoch schon)

=> **Verlust** (aufwändig vermeidbar, kann durch *Verschmelzung* verringert und durch *Kompaktifizierung* aufgelöst werden)



## Dateien öffnen


```c
FILE *fopen (const char* path, const char* mode)
FILE *fdopen(int fd, const char *mode); // [mode: r, w, a]
int fclose(FILE *stream);
```

Zeilenweises einlesen
```c
char *t;
while ( errno = 0, ( t = fgets( buffer, buffersize, stream/stdin ) ) ) {
    if ( buffer[buffersize-2] != '\0' && buffer[buffersize-2] != '\n' ) {
        // too many chars in line
        while ( fgetc(stream) != (int) '\n' ) {}
        buffer[buffersize-2] = '\0';
        continue;
    }
    // do something
}
if ( errno && ! t )
    perror("fgets");
```


## Datei-Infos

```c
int stat(const char *pathname, struct stat *buf); // follows symlinks
int lstat(const char *pathname, struct stat *buf);
```

```c
struct stat {
 dev_t     st_dev;         /* ID of device containing file */
 ino_t     st_ino;         /* inode number */
 mode_t    st_mode;        /* file type and mode */
 nlink_t   st_nlink;       /* number of hard links */
 uid_t     st_uid;         /* user ID of owner */
 gid_t     st_gid;         /* group ID of owner */
 dev_t     st_rdev;        /* device ID (if special file) */
 off_t     st_size;        /* total size, in bytes */
 blksize_t st_blksize;     /* blocksize for filesystem I/O */
 blkcnt_t  st_blocks;      /* number of 512B blocks allocated */
 struct timespec st_atim;  /* time of last access */
 struct timespec st_mtim;  /* time of last modification */
 struct timespec st_ctim;  /* time of last status change */
};
```

## Verzeichnis durchlaufen

```c
struct dirent **files;
int n = scandir( path, &files, filterfunction, alphasort );
if ( n < 0 ) {
    perror("scandir");
} else {
    while(n--) {
        printf("%s/%s", path, files[n]->d_name );
        free(files[n]);
    }
    free(files);
}
```

```c
DIR *opendir(const char *dirname);
struct dirent *readdir(DIR *dirp); // iterator
int closedir(DIR *dirp);
int chdir(const char *path);
```

```c
struct dirent {
ino_t d_ino; /* inode number */
off_t d_off; /* offset to the next dirent */
unsigned short d_reclen; /* length of this record */
unsigned char d_type; /* type of file; not supported
by all file system types */
char d_name[256]; /* filename */
};
```



## Prozesse


### Programm
Folge von Anweisungen (hinterlegt beispielsweise als ausführbare Datei auf dem Hintergrundspeicher)


### Prozess
Programm, das sich in Ausführung befindet, und seine Daten (Beachte: ein Programm kann sich mehrfach in Ausführung befinden)

Ein Prozess ist erst mal ein abstraktes Gebilde (= Funktionen und Datenstrukturen zur Verwaltung von Programmausführungen)

Im objektorientierten Sinn eine Klasse


### Prozessinstanz (Prozessinkarnation)
Eine physische Instanz des abstrakten Gebildes "Prozess"

Eine konkrete Ausführungsumgebung für ein Programm (Speicher, Rechte, Verwaltungsinformation)

Im objektorientierten Sinn die Instanz


### Prozesszustände
- **Erzeugt** (New): Prozess wurde erzeugt, besitzt aber noch nicht alle nötigen Betriebsmittel
- **Bereit** (Ready): Prozess besitzt alle nötigen Betriebsmittel und ist bereit zum Laufen
- **Laufend** (Running): Prozess wird vom realen Prozessor ausgeführt
- **Blockiert** (Blocked/Waiting): Prozess wartet auf ein Ereignis (z.B. Fertigstellung einer Ein- oder Ausgabeoperation, Zuteilung eines Betriebsmittels, Empfang einer Nachricht); zum Warten wird er blockiert
- **Beendet** (Terminated): Prozess ist beendet; einige Betriebsmittel sind aber noch nicht freigegeben oder Prozess muss aus anderen Gründen im System verbleiben


### Terminvorgaben
Externe (physikalische) Prozesse definieren, was genau bei einer nicht termingerecht geleisteten Berechnung zu geschehen hat:

**weich** (soft) auch „schwach“:
- das Ergebnis ist weiterhin von Nutzen, verliert jedoch mit jedem weiteren Zeitverzug des internen Prozesses zunehmend an Wert
- die Terminverletzung ist tolerierbar

**fest** (firm) auch „stark“:
- das Ergebnis ist wertlos, wird verworfen, der interne Prozess wird abgebrochen und erneut bereitgestellt
- die Terminverletzung ist tolerierbar

**hart** (hard) auch „strikt“:
- Verspätung der Ergebnislieferung kann zur „Katastrophe“ führen, dem internen Prozess wird eine Ausnahmesituation zugestellt
- Terminverletzung ist keinesfalls tolerierbar — aber möglich. . .


### Synchronisationsarten

1. Unterdrückend
   - verhindert die Prozessauslösung anderer Prozesse (betrifft konsumierbare Betriebsmittel)
   - aktueller Prozess wird dabei nicht verzögert

2. Blockierend
   - sperrt die Betriebsmittelvergabe an Prozesse (betrifft wiederverwendbare/konsumierbare Betriebsmittel)
   - aktueller Prozess wird möglicherweise suspendiert

3. Nichtblockierend
   - unterbindet Zustandsverstetigung durch Prozesse (betrifft wiederverwendbare Betriebsmittel: Speicher)
   - aktueller Prozess wird möglicherweise zurückgerollt

```c
pid_t getpid( void ); /* own pid */
pid_t getppid( void ); /* parent pid */
int execlp(const char* file, const char* arg0, ... , NULL);
int execvp(const char* file, const char* argv[] );
/* name of executable must be in 'file' AND 'arg0' */

pid_t wait(int *stat_loc);
pid_t waitpid(pid_t pid , int *stat_loc, int options);
[options: WCONTINUED, WNOHANG, WUNTRACED]

```

```c
pid_t p = fork();
if ( p == -1 ) {
    perror("fork"); // fork failed
} else if ( p == 0 ) { // child
    // work
    exit(EXIT_STATUS);
} else { // parent, p is PID of child
    // wait until child exits:
    do {
        if ( waitpid( p, &status, 0 ) == -1 ) {
            perror("waitpid");
            return -1;
        }
    } while ( !WIFEXITED(status) );
    WEXITSTATUS(status);
}
```

Signalbehandler:

```c
static void sigchld_handler(int signum) {
    int status, errno_bak = errno;
    pid_t cld_pid;

    /* iterate over all possible dead children */
    while ( ( cld_pid = waitpid( 0, &status, WNOHANG ) ) ) {
        if ( cld_pid == -1 ) {
            errno = errno_bak;
            return;
        }
        // do something
    }

    errno = errno_bak;
    return;
}
```

Signalbehandler installieren:
```c
struct sigaction sa;
sa.sa_flags = SA_RESTART | SA_NOCLDSTOP;
sa.sa_handler = sigchld_handler;
sigemptyset(&sa.sa_mask);
sigaction( SIGCHLD, &sa, NULL );
```

Signal ignorieren:
```c
struct sigaction sa;
sa.sa_flags = SA_RESTART;
sa.sa_handler = SIG_IGN;
sigemptyset(&sa.sa_mask);
sigaction( SIGINT, &sa, NULL );
```



## Unterbrechungen

**Trap**: synchrone (vorhersehbare) Programmunterbrechung

**Interrupt**: asynchrone (nicht-vorhersehbare) Programmunterbrechung



## Betriebsmittel

1. wiederverwendbar (dauerhaft, begrenzt)
  - teilbar (zeitlich)
  - unteilbar (zeitweise exklusiv)
  z.B. CPU, RAM, Variable, kritischer Abschnitt
2. konsumierbar (vorübergehend, unbegrenzt)
  z.B. IRQ, trap, Signal

Gleichzeitiger Zugriff auf unteilbare und Zugriffe auf konsumierbare Betriebsmittel erfordert **Synchronisation**



## Fäden (Threads)

### Federgewichtiger Prozess
Anwendungsfaden (*User-Thread*)

* Realisierung auf Anwendungsebene
* Erzeugung und Umschaltung von Threads extrem billig
* Systemkern sieht nur *einen* Kontrollfluss

-> keine parallen Abläufe auf Multiprozessorsystemen
-> blockiert ein Anwendungsfaden, sind alle anderen auch blockiert
-> Umschaltung zwischen den Fäden muss selbst erledigt werden


### Leichgewichtiger Prozess
Systemkernfaden (*Kernel-Thread*)

* Gruppe von Fäden nutzt gemeinsam die Betriebsmittel eines Prozesses
* jeder Faden ist dem Systemkern als Aktivitätstrager bekannt

-> Kosten für Erzeugung und Umschaltung erheblich geringer als bei *schwergewichtigen* Prozessen, aber teurer als bei *User-Threads*


```c
int pthread_create(pthread_t *thread, NULL ,void *(*start_routine)(void *), void *arg);
// save return value in errno and call perror
pthread_t pthread_self(); // own thread-id
void pthread_exit(void *retval); // leave thread
void pthread_join(pthread_t thread, void **retvalp); // waits until thread finished
int pthread_detach(pthread_t thread); // do not wait for thread to finish
```



## Semaphor & Mutex
**Mut**ual **ex**clusion (gegenseitiger Ausschluss)

```c
struct SEM {
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    volatile int value;
};

SEM *semCreate(int initVal) {
    SEM *sem;
    int errno_sv = errno;
    if ( ! ( sem = malloc( sizeof(SEM) ) ) )
        return NULL;

    if ( ( errno = pthread_mutex_init( &(sem->mutex), NULL ) ) ) {
        free(sem);
        return NULL;
    }

    if ( ( errno = pthread_cond_init( &(sem->cond), NULL ) ) ) {
        pthread_mutex_destroy( &(sem->mutex) );
        free(sem);
        return NULL;
    }

    sem->value = initVal;
    errno = errno_sv;
    return sem;
}

void semDestroy(SEM *sem) {
    pthread_mutex_destroy( &(sem->mutex) );
    pthread_cond_destroy( &(sem->cond) );
    free(sem);
}

void P(SEM *sem) {
    pthread_mutex_lock( &(sem->mutex) );
    while ( sem->value <= 0 )
        pthread_cond_wait( &(sem->cond), &(sem->mutex) );
    (sem->value)--;
    pthread_mutex_unlock( &(sem->mutex) );
}

void V(SEM *sem) {
    pthread_mutex_lock( &(sem->mutex) );
    (sem->value)++;
    pthread_cond_broadcast( &(sem->cond) );
    pthread_mutex_unlock( &(sem->mutex) );
}
```



## Netzwerk

Sockel öffnen:
```c
int listensock = socket( AF_INET6, SOCK_STREAM, 0 );
if ( listensock == -1 )
    perror("socket");
```

Sockel binden:
```c
struct sockaddr_in6 name = {
    .sin6_family = AF_INET6,
    .sin6_port = htons( port ),
    .sin6_addr = in6addr_any;
};
if ( bind( listensock, (struct sockaddr *) &name, sizeof(name) ) )
    perror("bind");
```

Auf Sockel lauschen:
```c
if ( listen( listensock, SOMAXCONN ) ) /* backlog is the maximum length to which the queue of pending connections for sockfd may grow */
    perror("listen");
```

Verbindungen annehmen:
```c
while(1) {
    int clientsock = accept( listensock, NULL, NULL );
    if ( clientsock == -1 ) {
        perror("accept");
        continue;
    }
    // handle Connection
}
```

Mit Hostname verbinden:
```c
int con( const char *host, const char *service) {
struct addrinfo *result;
struct addrinfo hints = {
    .ai_family = AF_INET6,
    .ai_socktype = SOCK_STREAM,
};
if ( ( r = getaddrinfo( host, service, &hints, &result) ) ) {
    fprintf( stderr, "getaddrinfo: %s\n", gai_sterror(r) );
}
int sockfd;
for ( struct addrinfo *i = result; i; i = i->ai_next ) {
    if ( ( sockfd = socket( i->ai_family, i->ai_protocol ) ) == -1 ) {
        perror("socket");
        close(sockfd);
        continue;
    }
    if ( connect( sockfd, i->ai_addr, i->ai_addrlen) == -1 ) {
        perror("connect");
        close(sockfd);
        continue;
    }
    break;
}

freeaddrinfo(result);

if ( ! i ) {
    // no connection available
}

return sockfd;
}
```
